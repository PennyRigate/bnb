{
    "id": "fef52a36-a1a9-45f1-9cb7-f58d8fbe3098",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_rock",
    "eventList": [
        {
            "id": "e2f01239-84c1-40f1-ae8a-526158ca7efa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fef52a36-a1a9-45f1-9cb7-f58d8fbe3098"
        },
        {
            "id": "b59c5e2f-8ba0-4351-bd68-f1268c7b74d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fef52a36-a1a9-45f1-9cb7-f58d8fbe3098"
        },
        {
            "id": "4595b5cb-1fca-4902-93ea-687befe1e84e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fef52a36-a1a9-45f1-9cb7-f58d8fbe3098"
        },
        {
            "id": "6dbed73a-88b9-4c14-860d-a7f7f3d52618",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f226f0ca-99f3-464e-988d-7da5bd3d810a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fef52a36-a1a9-45f1-9cb7-f58d8fbe3098"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fde97bea-7907-4d78-a4df-0286c9675baf",
    "visible": true
}