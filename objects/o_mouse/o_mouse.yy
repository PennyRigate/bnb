{
    "id": "a2a1283e-0c1b-446f-a932-51c026d21900",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_mouse",
    "eventList": [
        {
            "id": "773335ca-4c96-4c93-9d38-ba6d8d2df054",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a2a1283e-0c1b-446f-a932-51c026d21900"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87bf40a2-3dd9-4f9f-9d7c-9400db6727f1",
    "visible": true
}