window_set_min_width(768)
window_set_min_height(576)
//draw
draw_sprite_ext(sprite_index,doro,0,0,1,1,0,colself,1)
draw_sprite_ext(sprite_index,1+global.kempston,0,-8,1,1,0,colself,1)
draw_sprite_ext(sprite_index,3,0,-8,1,1,0,colself,1)
draw_sprite_ext(sprite_index,4+global.sound,0,-8,1,1,0,colself,1)
draw_sprite_ext(sprite_index,6,0,0,1,1,0,colself,1)
//code
if keyboard_check_pressed(ord("1")) then global.kempston = 0
if keyboard_check_pressed(ord("2")) then global.kempston = 1
if keyboard_check_pressed(ord("3")) then room_goto(r_bottles)
if keyboard_check_pressed(ord("4")) then {if global.sound = 1 then global.sound = 0 else global.sound = 1;if audio_is_playing(a_title) then audio_pause_sound(a_title) else audio_resume_sound(a_title)}
if keyboard_check_pressed(ord("5")) then 
{
ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
ini_write_real("OPTIONS","KEMPSTON",global.kempston)
ini_write_real("OPTIONS","SOUND",global.sound)
ini_close()
global.gbl = 0
audio_stop_sound(a_title)
room_goto_next()
}
draw_set_font(f_)
draw_sprite_ext(s_scorebord,global.mono,x,y,1,1,0,colself,1)
if global.mono = 0 then draw_set_colour(make_colour_rgb(0,215,0)) else draw_set_color(colself)
draw_text(47,-5,"000000")
if global.mono = 0 then draw_set_colour(make_colour_rgb(0,215,215)) else draw_set_color(colself)
draw_text(110,-5,"0")
if global.mono = 0 then draw_set_colour(make_colour_rgb(215,215,0)) else draw_set_color(colself)
draw_text(131,-5,"00")
if global.mono = 0 then draw_set_colour(make_colour_rgb(255,255,0)) else draw_set_color(colself)
draw_text(161,-5,"000")
if global.mono = 0 then draw_set_colour(make_colour_rgb(215,0,215)) else draw_set_color(colself)
if global.hi[1] > 99999 then additzero = ""
if global.hi[1] < 100000 then additzero = "0"
if global.hi[1] < 10000 then additzero = "00"
if global.hi[1] < 1000 then additzero = "000"
if global.hi[1] < 100 then additzero = "0000"
if global.hi[1] < 10 then additzero = "00000"
if global.hi[1] < 999999 then draw_text(207,-5,string(additzero) + string(global.hi[1])) else draw_text(207,-5,"LOTS")
