{
    "id": "97b05c33-dab6-482b-823b-fa23c8912ec8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tit",
    "eventList": [
        {
            "id": "7475d5ab-38be-42c5-b465-eb158b97849b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97b05c33-dab6-482b-823b-fa23c8912ec8"
        },
        {
            "id": "f706aa2d-7358-4eb7-9852-cdbb0def2a3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "97b05c33-dab6-482b-823b-fa23c8912ec8"
        },
        {
            "id": "0aa3425e-e92f-40a4-9fa3-212bb2500556",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "97b05c33-dab6-482b-823b-fa23c8912ec8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
    "visible": true
}