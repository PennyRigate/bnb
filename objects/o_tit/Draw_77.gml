//	VARS
crt = true
fullscreen = window_get_fullscreen()
display_width = display_get_width()
display_height = display_get_height()
window_width = window_get_width()
window_height = window_get_height()
view_width = camera_get_view_width(view_camera[0]);
view_height = camera_get_view_height(view_camera[0]);
view_x = camera_get_view_x(view_camera[0])
view_y = camera_get_view_y(view_camera[0])
game_width = 0
game_height = 0
//	MIN WINDOW SIZE
if crt = true {
window_set_min_width(256 * 4)
window_set_min_height(192 * 4)
}
//	SHOULD IT SCALE TO WINDOW (WINDOWED MODE) OR TO SCREEN? (FULLSCREEN MODE)
if fullscreen = 1 {
    game_width = display_width
    game_height = display_height
} else {
    game_width = window_width
    game_height = window_height
}
//	DETERMINE SCALE AMOUNT
scale = game_height div 192
//	DRAW SCALED
application_surface_draw_enable(false)
if crt = true shader_set(shd_crt)
draw_surface_ext(application_surface,(game_width/2) - ((256*scale)/2),(game_height/2) - ((192*scale)/2),scale,scale,0,c_white,1)
shader_reset()

