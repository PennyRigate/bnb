{
    "id": "16f56573-0f54-4c37-87fb-5629ece6ea5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_buzzer",
    "eventList": [
        {
            "id": "669dbbf7-5f6b-4b8f-be88-ecc1a2ab8f57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16f56573-0f54-4c37-87fb-5629ece6ea5e"
        },
        {
            "id": "e92500e5-43fa-4383-b3ef-1a45c7e44fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16f56573-0f54-4c37-87fb-5629ece6ea5e"
        },
        {
            "id": "32f8d0a7-ccc5-45e5-8ce0-15549c58dad3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "16f56573-0f54-4c37-87fb-5629ece6ea5e"
        },
        {
            "id": "96aca3a7-b9db-4952-b523-75af357c50a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2d3c2618-645f-43a6-9219-37fb0a1b6369",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "16f56573-0f54-4c37-87fb-5629ece6ea5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ffea6aa-55c5-4c05-a9c2-8b3097b8fe9f",
    "visible": true
}