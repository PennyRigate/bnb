{
    "id": "fc7e1f86-3ae2-4e04-a1d8-e2325933850f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dropspawn",
    "eventList": [
        {
            "id": "f1c99e07-4d11-4a47-a7e2-e6d9adbe34a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fc7e1f86-3ae2-4e04-a1d8-e2325933850f"
        },
        {
            "id": "8a58c4ff-b4dd-43e5-84e6-f379cc43c208",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc7e1f86-3ae2-4e04-a1d8-e2325933850f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7532695-a13c-4311-a11a-ce0ecf429686",
    "visible": true
}