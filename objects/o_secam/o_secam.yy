{
    "id": "56796fef-2f7c-4954-b41f-75040ac417e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_secam",
    "eventList": [
        {
            "id": "ae5e345f-e3d9-4a59-bccd-27135f5057e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56796fef-2f7c-4954-b41f-75040ac417e7"
        },
        {
            "id": "d7cd42ad-423c-4391-a020-856d9c8da7f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "56796fef-2f7c-4954-b41f-75040ac417e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c42acfeb-c52e-4140-8eeb-97b4d6cd286a",
    "visible": true
}