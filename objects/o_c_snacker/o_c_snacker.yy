{
    "id": "c0f1d102-0635-4a24-aaac-d346661416df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_snacker",
    "eventList": [
        {
            "id": "b2fde79f-c01b-4db8-8436-75db9388f8da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "453e796d-fd88-4261-b8db-7a25506b9e78",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0f1d102-0635-4a24-aaac-d346661416df"
        },
        {
            "id": "598f689b-f831-4062-9661-32b906c2d05b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c0f1d102-0635-4a24-aaac-d346661416df"
        },
        {
            "id": "e5dbc92f-837c-488e-95fd-f8370e7469d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0f1d102-0635-4a24-aaac-d346661416df"
        },
        {
            "id": "154f8aa5-685e-4c5a-a7be-5035c89665f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0f1d102-0635-4a24-aaac-d346661416df"
        },
        {
            "id": "e50f0cc0-4280-4d16-b0ee-84625ca9b2ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c0f1d102-0635-4a24-aaac-d346661416df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7cfb8e0-8c8a-4103-8db4-6096497f9d6e",
    "visible": true
}