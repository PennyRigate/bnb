{
    "id": "0870af2d-b0df-42f2-bc42-0cef37b46e4a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_solid",
    "eventList": [
        {
            "id": "9822ea7d-307d-412b-9257-59f3a4e1c6aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0870af2d-b0df-42f2-bc42-0cef37b46e4a"
        },
        {
            "id": "217efdc7-e962-4809-8fe8-c6cfc8ad0627",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0870af2d-b0df-42f2-bc42-0cef37b46e4a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
    "visible": true
}