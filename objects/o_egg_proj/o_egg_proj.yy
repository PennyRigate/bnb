{
    "id": "5a6bbec2-d31b-4cca-9be1-b6ae748b1beb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_egg_proj",
    "eventList": [
        {
            "id": "1b5dac7e-27d8-465f-a3bd-f40a7c591cfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5a6bbec2-d31b-4cca-9be1-b6ae748b1beb"
        },
        {
            "id": "0a87c9ee-1e07-423f-9d56-2c1c1d6c84fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a6bbec2-d31b-4cca-9be1-b6ae748b1beb"
        },
        {
            "id": "fb654b22-c88f-48c2-9cd1-48ab9134efee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a6bbec2-d31b-4cca-9be1-b6ae748b1beb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbecb6df-be24-4d33-94d9-aa6c4901cd00",
    "visible": true
}