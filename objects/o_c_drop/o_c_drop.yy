{
    "id": "b631149f-95d9-4114-aa11-0dfb9bd93b8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_drop",
    "eventList": [
        {
            "id": "38468ab7-6794-4fc0-b920-f9ea906680b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1f704f75-3289-4f67-912f-9902f779b487",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b631149f-95d9-4114-aa11-0dfb9bd93b8f"
        },
        {
            "id": "e65c79de-f3dd-4a62-8795-b68c3f3a4851",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b631149f-95d9-4114-aa11-0dfb9bd93b8f"
        },
        {
            "id": "73ec8a0c-f789-4950-a73e-8a31ab2f2069",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b631149f-95d9-4114-aa11-0dfb9bd93b8f"
        },
        {
            "id": "09da27a1-ffa0-4ed4-a18d-a1ec8a47117b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b631149f-95d9-4114-aa11-0dfb9bd93b8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
    "visible": true
}