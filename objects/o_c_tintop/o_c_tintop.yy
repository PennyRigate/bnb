{
    "id": "b1910468-bcab-4477-ba97-1ec788313f8d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_tintop",
    "eventList": [
        {
            "id": "dca57c81-24a7-4960-8645-7df5bd130437",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1910468-bcab-4477-ba97-1ec788313f8d"
        },
        {
            "id": "83b5a912-552e-4e37-b4c5-27ff2ee40290",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1910468-bcab-4477-ba97-1ec788313f8d"
        },
        {
            "id": "d328462c-a17d-4a57-a889-e5a117ffdcc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1910468-bcab-4477-ba97-1ec788313f8d"
        },
        {
            "id": "5c3d72ea-0206-4c39-95e2-e084fa8c6bdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8be45857-0c5f-48c1-b8c5-6e2c8b0b1053",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b1910468-bcab-4477-ba97-1ec788313f8d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0f85258b-1327-4b95-a8cc-f7f2b31ce57e",
    "visible": true
}