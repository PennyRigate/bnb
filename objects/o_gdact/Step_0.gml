if o_player.pause = 0 then y += 1.25 * dir
if o_player.pause = 0 then x += (1.25 * xdir) * movex
if place_meeting(x,y+dir,o_ewall) then {y -= dir;dir *= -1}
if place_meeting(x+xdir,y,o_ewall) then {x -= xdir;xdir *= -1}
if movex = 0 then if o_player.x < x then image_xscale = -1 else image_xscale = 1
if movex = 1 then image_xscale = xdir
if o_player.pause = 1 then image_speed = 0 else image_speed = 1