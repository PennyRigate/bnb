{
    "id": "2d3c2618-645f-43a6-9219-37fb0a1b6369",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_gdact",
    "eventList": [
        {
            "id": "44fd4089-4a76-45dd-9d39-e49afdfc773b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d3c2618-645f-43a6-9219-37fb0a1b6369"
        },
        {
            "id": "c611ada9-66c5-4c72-a1fd-7f76379bb75f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d3c2618-645f-43a6-9219-37fb0a1b6369"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e4b17f2e-2372-4331-8a87-9cfc92450c81",
    "visible": true
}