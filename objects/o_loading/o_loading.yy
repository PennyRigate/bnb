{
    "id": "253c1d2f-973c-4086-9ce9-a2820e6e88d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_loading",
    "eventList": [
        {
            "id": "14ca8d82-4dc2-4956-8098-5e65bcc760ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "253c1d2f-973c-4086-9ce9-a2820e6e88d3"
        },
        {
            "id": "7ed6ac2b-fa4a-43b1-b7b2-b933b35937a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "253c1d2f-973c-4086-9ce9-a2820e6e88d3"
        },
        {
            "id": "15db2c46-294b-4d21-b9ea-0f0a1c7cb0c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "253c1d2f-973c-4086-9ce9-a2820e6e88d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5bf13635-8a3b-44b0-926a-074d1dc29cfc",
    "visible": true
}