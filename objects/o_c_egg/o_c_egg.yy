{
    "id": "077bb61d-f462-4147-92dd-3b36ed47bc0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_egg",
    "eventList": [
        {
            "id": "40223fee-2fc1-405c-adb3-c11856471527",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "077bb61d-f462-4147-92dd-3b36ed47bc0b"
        },
        {
            "id": "40383378-6a86-4e97-9d49-c13cc813e0f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f28d5c2d-c665-4183-b2f7-cb758ad2f73c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "077bb61d-f462-4147-92dd-3b36ed47bc0b"
        },
        {
            "id": "a075b518-6e5a-4630-b1e0-0466a7f23b31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "077bb61d-f462-4147-92dd-3b36ed47bc0b"
        },
        {
            "id": "277ceee9-2f68-41f1-9d16-f66a788e3ce1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "077bb61d-f462-4147-92dd-3b36ed47bc0b"
        },
        {
            "id": "71291030-0220-4e14-9859-9d9ee573fc3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f226f0ca-99f3-464e-988d-7da5bd3d810a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "077bb61d-f462-4147-92dd-3b36ed47bc0b"
        },
        {
            "id": "a6496d93-b40d-4ee8-bc87-2d33a1621e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "54a0a6a1-13a2-4a40-ac2d-af833487ae05",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "077bb61d-f462-4147-92dd-3b36ed47bc0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
    "visible": true
}