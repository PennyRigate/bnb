window_set_min_width(768)
window_set_min_height(576)
//DRAW
draw_sprite_ext(sprite_index,0,0,0,1,1,0,scol,1)
draw_sprite_ext(sprite_index,1,iy,0,1,1,0,scol,1)
//ITIMER
itimer -= 1
if itimer > 25 then iy = ix else iy = 0
if itimer = 0 then {ix *= -1;itimer = 65}
if room != r_demoend then
{
//REBIND
if act = 1 then
{
draw_sprite_ext(sprite_index,2,0,0,1,1,0,scol,1)
draw_sprite_ext(sprite_index,3+act,0,0,1,1,0,scol,1)
if act = 1 then
{
	repeat(left)
	{
		draw_sprite_ext(sprite_index,5,0,i,1,1,0,scol,1)
		i -= 20
	}
	i = 0
}


if keyboard_check_pressed(vk_anykey) then 
{
if keyboard_check(vk_space) then {lchar = vk_space;dchar = "space"}
if keyboard_check(vk_left) then {lchar = vk_left;dchar = "left"}
if keyboard_check(vk_right) then {lchar = vk_right;dchar = "right"}
if keyboard_check(vk_up) then {lchar = vk_up;dchar = "up"}
if keyboard_check(vk_down) then {lchar = vk_down;dchar = "down"}
if keyboard_check(ord("1")) then {lchar = 49;dchar = "1"}
if keyboard_check(ord("2")) then {lchar = 50;dchar = "2"}
if keyboard_check(ord("3")) then {lchar = 51;dchar = "3"}
if keyboard_check(ord("4")) then {lchar = 52;dchar = "4"}
if keyboard_check(ord("5")) then {lchar = 53;dchar = "5"}
if keyboard_check(ord("6")) then {lchar = 54;dchar = "6"}
if keyboard_check(ord("7")) then {lchar = 55;dchar = "7"}
if keyboard_check(ord("8")) then {lchar = 56;dchar = "8"}
if keyboard_check(ord("9")) then {lchar = 57;dchar = "9"}
if keyboard_check(ord("0")) then {lchar = 48;dchar = "0"}
if keyboard_check(189) then {lchar = 189;dchar = "-"}
if keyboard_check(187) then {lchar = 187;dchar = "="}
if keyboard_check(192) then {lchar = 192;dchar = "`"}
if keyboard_check(8) then {lchar = 8;dchar = "bksp"}
if keyboard_check(9) then {lchar = 9;dchar = "tab"}
if keyboard_check(vk_return) then {lchar = vk_return;dchar = "return"}
if keyboard_check(vk_enter) then {lchar = vk_enter;dchar = "enter"}
if keyboard_check(20) then {lchar = 20;dchar = "caps"}
if keyboard_check(65) then {lchar = 65;dchar = "a"}
if keyboard_check(83) then {lchar = 83;dchar = "s"}
if keyboard_check(68) then {lchar = 68;dchar = "d"}
if keyboard_check(70) then {lchar = 70;dchar = "f"}
if keyboard_check(71) then {lchar = 71;dchar = "g"}
if keyboard_check(72) then {lchar = 72;dchar = "h"}
if keyboard_check(74) then {lchar = 74;dchar = "j"}
if keyboard_check(75) then {lchar = 75;dchar = "k"}
if keyboard_check(76) then {lchar = 76;dchar = "l"}
if keyboard_check(186) then {lchar = 186;dchar = ";"}
if keyboard_check(222) then {lchar = 222;dchar = "'"}
if keyboard_check(220) then {lchar = 220;dchar = "bslash"}
if keyboard_check(219) then {lchar = 219;dchar = "["}
if keyboard_check(221) then {lchar = 221;dchar = "]"}
if keyboard_check(ord("Q")) then {lchar = 81;dchar = "q"}
if keyboard_check(ord("W")) then {lchar = 87;dchar = "w"}
if keyboard_check(ord("E")) then {lchar = 69;dchar = "e"}
if keyboard_check(ord("R")) then {lchar = 82;dchar = "r"}
if keyboard_check(ord("T")) then {lchar = 84;dchar = "t"}
if keyboard_check(ord("Y")) then {lchar = 89;dchar = "y"}
if keyboard_check(ord("U")) then {lchar = 85;dchar = "u"}
if keyboard_check(ord("I")) then {lchar = 73;dchar = "i"}
if keyboard_check(ord("O")) then {lchar = 79;dchar = "o"}
if keyboard_check(ord("P")) then {lchar = 80;dchar = "p"}
if keyboard_check(ord("Z")) then {lchar = 90;dchar = "z"}
if keyboard_check(88) then {lchar = 88;dchar = "x"}
if keyboard_check(67) then {lchar = 67;dchar = "c"}
if keyboard_check(86) then {lchar = 86;dchar = "v"}
if keyboard_check(66) then {lchar = 66;dchar = "b"}
if keyboard_check(78) then {lchar = 78;dchar = "n"}
if keyboard_check(77) then {lchar = 77;dchar = "m"}
if keyboard_check(188) then {lchar = 188;dchar = ","}
if keyboard_check(190) then {lchar = 190;dchar = "."}
if keyboard_check(191) then {lchar = 191;dchar = "/"}
if keyboard_check(16) then {lchar = 16;dchar = "shift"}
if keyboard_check(91) then {lchar = 91;dchar = "wind"}
if keyboard_check(162) then {lchar = 162;dchar = "lctrl"}
if keyboard_check(163) then {lchar = 163;dchar = "rctrl"}
if keyboard_check(164) then {lchar = 164;dchar = "lalt"}
if keyboard_check(165) then {lchar = 165;dchar = "ralt"}
if keyboard_check(93) then {lchar = 93;dchar = "fn"}
if keyboard_check(78) then {lchar = 78;dchar = "n"}
if keyboard_check(145) then {lchar = 145;dchar = "scrlk"}
if keyboard_check(19) then {lchar = 19;dchar = "pause"}
if keyboard_check(45) then {lchar = 45;dchar = "ins"}
if keyboard_check(36) then {lchar = 36;dchar = "home"}
if keyboard_check(33) then {lchar = 33;dchar = "pgup"}
if keyboard_check(34) then {lchar = 34;dchar = "pgdn"}
if keyboard_check(46) then {lchar = 46;dchar = "del"}
if keyboard_check(35) then {lchar = 35;dchar = "end"}
if keyboard_check(27) then {lchar = 27;dchar = "esc"}
if keyboard_check(112) then {lchar = 112;dchar = "f1"}
if keyboard_check(113) then {lchar = 113;dchar = "f2"}
if keyboard_check(114) then {lchar = 114;dchar = "f3"}
if keyboard_check(115) then {lchar = 115;dchar = "f4"}
if keyboard_check(116) then {lchar = 116;dchar = "f5"}
if keyboard_check(117) then {lchar = 117;dchar = "f6"}
if keyboard_check(118) then {lchar = 118;dchar = "f7"}
if keyboard_check(119) then {lchar = 119;dchar = "f8"}
if keyboard_check(120) then {lchar = 120;dchar = "f9"}
if keyboard_check(121) then {lchar = 121;dchar = "f10"}
if keyboard_check(122) then {lchar = 122;dchar = "f11"}
if keyboard_check(123) then {lchar = 123;dchar = "f12"}
if keyboard_check(144) then {lchar = 144;dchar = "numl"}
if keyboard_check(111) then {lchar = 111;dchar = "num/"}
if keyboard_check(106) then {lchar = 106;dchar = "num*"}
if keyboard_check(109) then {lchar = 109;dchar = "num-"}
if keyboard_check(103) then {lchar = 103;dchar = "num7"}
if keyboard_check(104) then {lchar = 104;dchar = "num8"}
if keyboard_check(105) then {lchar = 105;dchar = "num9"}
if keyboard_check(107) then {lchar = 107;dchar = "num+"}
if keyboard_check(100) then {lchar = 100;dchar = "num4"}
if keyboard_check(101) then {lchar = 101;dchar = "num5"}
if keyboard_check(102) then {lchar = 102;dchar = "num6"}
if keyboard_check(97) then {lchar = 97;dchar = "num1"}
if keyboard_check(98) then {lchar = 98;dchar = "num2"}
if keyboard_check(99) then {lchar = 99;dchar = "num3"}
if keyboard_check(96) then {lchar = 96;dchar = "num0"}
if keyboard_check(110) then {lchar = 110;dchar = "numd"}
	  if left = 4 then {global.k_up = lchar;inp[1] = string("= ") + string_upper(dchar)}
	  if left = 3 then {global.k_down = lchar;inp[2] = string("= ") + string_upper(dchar)}
	  if left = 2 then {global.k_left = lchar;inp[3] = string("= ") + string_upper(dchar)}
	  if left = 1 then {global.k_right = lchar;inp[4] = string("= ") + string_upper(dchar)}
	  if left = 0 then {global.k_jump = lchar;inp[5] = string("= ") + string_upper(dchar)}
	  if left > -1 then left -= 1
}


if global.mono = 0 then draw_set_colour(make_colour_rgb(215,215,215)) else if global.mono = 1 then draw_set_color(scol)
draw_set_font(f_)
repeat(5-left)
{
	i += 21
draw_text(174,4+i,string(inp[i / 21]))
}
i = 0

draw_set_halign(fa_center)
if left = -1 then 
{
draw_text(75,67,"KEEP CHANGES? Y/N")
if keyboard_check_pressed(ord("Y")) then 
{
ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
ini_write_real("CONTROLS","LEFT",global.k_left)
ini_write_real("CONTROLS","RIGHT",global.k_right)
ini_write_real("CONTROLS","UP",global.k_up)
ini_write_real("CONTROLS","DOWN",global.k_down)
ini_write_real("CONTROLS","JUMP",global.k_jump)
ini_close()
room_goto(r_title)
}
if keyboard_check_pressed(ord("N")) then 
{
left = 4
inp[1] = ""
inp[2] = ""
inp[3] = ""
inp[4] = ""
inp[5] = ""
inp[6] = ""
}
}
draw_set_halign(fa_left)
}
else
{
draw_sprite_ext(sprite_index,2,0,0,1,1,0,scol,1)
draw_sprite_ext(sprite_index,3+act,0,0,1,1,0,scol,1)
	repeat(left)
	{
		draw_sprite(sprite_index,6,0,i)
		i -= 16
	}
	i = 0
	if calc = 0 then if global.infinilives = 0 then
	{
		treq = 15000
		if global.b_time < treq then b_time = round((round((treq - global.b_time) / 10)) / 3) else b_time = 0
		if global.notes != 50 or global.b_jig != 5 then b_time = round(b_time / 2)
		if b_time < 0 then b_time = 0
		if global.b_perfect = 1 then b_perfect = 250 else b_perfect = 0
		if global.notes = 50 then b_note = 200 else b_note = global.notes * 2
		if global.b_jig = 5 then b_jig = 350 else b_jig = global.b_jig * 25
		b_complete = 100
		global.scor += b_time + b_perfect + b_note + b_jig + b_complete
		global.levelscore += b_time + b_perfect + b_note + b_jig + b_complete
		if global.scor > global.hi[1] then 
		{
		ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
		ini_write_real("SCORE","1st",global.scor)
		ini_close()
		}
		global.live += round(global.levelscore / 10000) + 1
		calc = 1
	}
	wait -= 1.25
	if wait = 0 then {wait = 150;left -= 1}
if global.mono = 0 then draw_set_colour(make_colour_rgb(215,215,215)) else if global.mono = 1 then draw_set_color(scol)
	draw_set_font(f_)
	draw_set_halign(fa_center)
	if left = 5 then draw_text(78,60,"LETS TOTAL UP")
	if left = 5 then draw_text(78,68,"YOUR BONUSES")
	if left = 4 then draw_text(78,67,"CLEAR BONUS")
	if left = 3 then draw_text(78,67,"TIME BONUS")
	if left = 2 then draw_text(78,67,"PERFECT BONUS")
	if left = 1 then draw_text(78,67,"NOTE BONUS")
	if left = 0 then draw_text(78,67,"JIGGY BONUS")
	if left < 0 then draw_text(78,67,"TOTAL")
	draw_set_halign(fa_left)
	//draw calcs
	if keyboard_check_pressed(vk_anykey) or gamepad_button_check_pressed(0,gp_face1) then wait = 1.25
	if left = 4 then {if wait < 101 and wait > 50 then draw_text(177,24,"x1 ");if wait < 51 then draw_text(177,24,string("x1 +100"))}
	if left < 4 then draw_text(177,24,string("x1 +100"))
	if left = 3 then {if wait < 101 and wait > 50 then draw_text(177,56,string("x") + string(round(global.b_time / 500)));if wait < 51 then draw_text(177,56,string("x") + string(round(global.b_time / 500)) + string(" +") + string(b_time) )}
	if left < 3 then draw_text(177,56,string("x") + string(round(global.b_time / 500)) + string(" +") + string(b_time))
	if left = 2 then {if wait < 101 and wait > 50 then draw_text(177,72,string("x") + string(global.b_perfect));if wait < 51 then draw_text(177,72,string("x") + string(global.b_perfect) + string(" +") + string(b_perfect) )}
	if left < 2 then draw_text(177,72,string("x") + string(global.b_perfect) + string(" +") + string(b_perfect))
	if left = 1 then {if wait < 101 and wait > 50 then draw_text(177,88,string("x") + string(global.notes));if wait < 51 then draw_text(177,88,string("x") + string(global.notes) + string(" +") + string(b_note) )}
	if left < 1 then draw_text(177,88,string("x") + string(global.notes) + string(" +") + string(b_note))
	if left = 0 then {if wait < 101 and wait > 50 then draw_text(177,104,string("x") + string(global.b_jig));if wait < 51 then draw_text(177,104,string("x") + string(global.b_jig) + string(" +") + string(b_jig) )}
	if left < 0 then draw_text(177,104,string("x") + string(global.b_jig) + string(" +") + string(b_jig))
	if left = -1 then {if wait < 101 and wait > 50 then draw_text(161,120,"TOTAL ");if wait < 51 then draw_text(161,120,string("TOTAL ") + string(b_time + b_perfect + b_note + b_jig + b_complete))}
	if left < -1 then draw_text(161,120,string("TOTAL ") + string(b_time + b_perfect + b_note + b_jig + b_complete))
	if left = -2 then if wait < 51 then room_goto(global.next)
}
}
else
{
global.kempston = -1
if gamepad_is_connected(0) then {cunt = 0;global.kempston = 1}
if gamepad_is_connected(1) then {cunt = 1;global.kempston = 1}
if gamepad_is_connected(2) then {cunt = 2;global.kempston = 1}
if gamepad_is_connected(3) then {cunt = 3;global.kempston = 1}
	if keyboard_check_pressed(vk_anykey) then room_goto(r_spiralmountain)
	if global.kempston = 1 then
	{
	if gamepad_button_check_pressed(cunt,gp_padd) then room_goto(r_spiralmountain)
	if gamepad_button_check_pressed(cunt,gp_padl) then room_goto(r_spiralmountain)
	if gamepad_button_check_pressed(cunt,gp_padr) then room_goto(r_spiralmountain)
	if gamepad_button_check_pressed(cunt,gp_padu) then room_goto(r_spiralmountain)
	if gamepad_button_check_pressed(cunt,gp_face1) then room_goto(r_spiralmountain)
	if gamepad_button_check_pressed(cunt,gp_face3) then room_goto(r_spiralmountain)
}
	draw_set_color(scol)
	draw_set_font(f_)
	draw_set_halign(fa_center)
	draw_text(78,68,string("for game ") + string(global.loop + 1) + string("."))
	draw_text(78,68-8,"Press any key")
	if global.doro = 0 then draw_text(78,68-16,"Bear 'n' Burd!") else draw_text(78,68-16,"Zombie 'n' Burd!")
	draw_text(78,68-24,"Congratulations,")
}