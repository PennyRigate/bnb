pstart = xstart - (256 * (o_player.quadrant - 1))
if act = 0 then 
{
	if o_player.pause = 0 then  x += dir
if x > pstart + 32 or place_meeting(x+1,y,o_floor) then {dir = -1}
if x < pstart - 32 or place_meeting(x-1,y,o_floor) then {dir = 1}
}

if act = 1 then
{
	if o_player.pause = 0 then x += dir * 1
	if o_player.x < x then dir = -1 else dir = 1
	if !(o_player.x > pstart - 48 or o_player.x < pstart + 48) then {act = 0}
}


if o_player.x < pstart then if o_player.x > pstart - 48 then act = 1
if o_player.x > pstart then if o_player.x < pstart + 48 then act = 1
if o_player.y < y then act = 0
if x > pstart + 48 then act = 0
if x < pstart - 48 then act = 0

if o_player.y > y + 8 then act = 0

if o_player.pause = 1 then image_speed = 0 else image_speed = 1