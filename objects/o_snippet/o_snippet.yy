{
    "id": "71babd67-9f9c-438b-b4b4-3895f6f11b5d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_snippet",
    "eventList": [
        {
            "id": "ec55ef44-3041-43d7-9d66-d9a49a6ca6bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71babd67-9f9c-438b-b4b4-3895f6f11b5d"
        },
        {
            "id": "5a77b805-abf1-4723-8573-399d17ce0c0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71babd67-9f9c-438b-b4b4-3895f6f11b5d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "194dfb62-a8d7-43ee-861f-e0f2c9312b2d",
    "visible": true
}