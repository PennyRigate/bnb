{
    "id": "b93a8d16-4cb3-4947-90cf-e63996cbff60",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slider",
    "eventList": [
        {
            "id": "41317881-c33c-42b8-b083-16e9a0c4bbce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "b93a8d16-4cb3-4947-90cf-e63996cbff60"
        },
        {
            "id": "801c88b2-7d79-4766-8817-32141dcc499f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b93a8d16-4cb3-4947-90cf-e63996cbff60"
        },
        {
            "id": "a5d83eff-160b-455a-ab64-dac04dbb9991",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b93a8d16-4cb3-4947-90cf-e63996cbff60"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8e0d6089-37d2-484e-81f5-8d5e8b2fd9ea",
    "visible": true
}