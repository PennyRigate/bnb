if image_index >= 1 then 
{
if o_player.pause = 0 then x += 1.25 * image_xscale
y = ystart - 3
}
else y = ystart

if place_meeting(x+image_xscale,y,o_ewall) then {x -= image_xscale;image_xscale *= -1}
if not place_meeting(x,ystart+4,o_floor) then image_index = 1

if o_player.pause = 1 then image_speed = 0 else image_speed = 1