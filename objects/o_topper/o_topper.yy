{
    "id": "a99aca33-74b0-4752-9b33-6386ca6c8859",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_topper",
    "eventList": [
        {
            "id": "608bbb58-a78f-4765-94f1-3254837c7232",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a99aca33-74b0-4752-9b33-6386ca6c8859"
        },
        {
            "id": "4c7e2661-209a-446b-bb64-808b8e7f01a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a99aca33-74b0-4752-9b33-6386ca6c8859"
        },
        {
            "id": "52d92c71-fdc7-4f36-93c5-9816ab18edd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "be7c16cc-5092-4276-bca7-a812c19b355e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a99aca33-74b0-4752-9b33-6386ca6c8859"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e0caab4-e00c-4044-9f8e-2d88d4c8f0bf",
    "visible": true
}