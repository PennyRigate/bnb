{
    "id": "3b4feee4-ffaf-472a-9bb4-ea72bc7162bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_notedoor",
    "eventList": [
        {
            "id": "10531a43-43f1-40ed-b9a4-7d90ef8ac19c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3b4feee4-ffaf-472a-9bb4-ea72bc7162bc"
        },
        {
            "id": "d2868af7-4251-4f44-a164-8469b3947aaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3b4feee4-ffaf-472a-9bb4-ea72bc7162bc"
        },
        {
            "id": "34f346f3-723e-41cb-9ae7-4dcf157635ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3b4feee4-ffaf-472a-9bb4-ea72bc7162bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a56b056d-bc17-4b1e-a7d4-b5479e456425",
    "visible": true
}