{
    "id": "1f61bd02-edd0-4eea-86fe-2de0ec834403",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_lavarock",
    "eventList": [
        {
            "id": "a55d1fd6-e3d8-4127-b047-721b42436c56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f61bd02-edd0-4eea-86fe-2de0ec834403"
        },
        {
            "id": "13336ec3-72ff-4e7a-b578-eef671f6dec5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f61bd02-edd0-4eea-86fe-2de0ec834403"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af767dbd-4181-4ad8-8aad-defe4d402e56",
    "visible": true
}