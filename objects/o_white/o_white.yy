{
    "id": "8ec2646f-2689-4c42-9c4e-d801e5e7f447",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_white",
    "eventList": [
        {
            "id": "af021f47-b561-489e-9c66-408172983817",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ec2646f-2689-4c42-9c4e-d801e5e7f447"
        },
        {
            "id": "d8625fc0-ec69-4c3b-b265-350cc3568466",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ec2646f-2689-4c42-9c4e-d801e5e7f447"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4a5ddc7-87f9-4ee1-b046-35ef610447d2",
    "visible": true
}