{
    "id": "1f704f75-3289-4f67-912f-9902f779b487",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_drop",
    "eventList": [
        {
            "id": "1f00f615-4ba7-4a30-9e90-3773e7a1d32f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f704f75-3289-4f67-912f-9902f779b487"
        },
        {
            "id": "1535c288-7a09-40fc-b801-8b02854512c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f704f75-3289-4f67-912f-9902f779b487"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7532695-a13c-4311-a11a-ce0ecf429686",
    "visible": true
}