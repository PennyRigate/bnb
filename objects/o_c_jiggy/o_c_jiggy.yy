{
    "id": "04251641-4512-48b8-b9eb-5785498c4ea1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_jiggy",
    "eventList": [
        {
            "id": "cb3b5f02-bf31-4b09-95fa-28f7f8f9fcb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04251641-4512-48b8-b9eb-5785498c4ea1"
        },
        {
            "id": "5da12efc-7684-49d9-99d3-873e5264d749",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04251641-4512-48b8-b9eb-5785498c4ea1"
        },
        {
            "id": "904670e3-f2f7-440d-ab44-0982fd5b7317",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "04251641-4512-48b8-b9eb-5785498c4ea1"
        },
        {
            "id": "51f856fc-25fe-4f4b-a763-ba8abfe98cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a9c98e97-122e-43de-8b14-66f7f4737e03",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "04251641-4512-48b8-b9eb-5785498c4ea1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
    "visible": true
}