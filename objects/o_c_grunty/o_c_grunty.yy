{
    "id": "d0a240b6-027d-4e6e-ba23-2cb5d342b1a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_grunty",
    "eventList": [
        {
            "id": "9f027070-1bb0-43a3-8232-f639688cb456",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0a240b6-027d-4e6e-ba23-2cb5d342b1a3"
        },
        {
            "id": "03839160-f4a3-477e-8ff0-413802b1c6fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d0a240b6-027d-4e6e-ba23-2cb5d342b1a3"
        },
        {
            "id": "9c1f7b48-6727-4aba-bd95-988e4eccf8ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d0a240b6-027d-4e6e-ba23-2cb5d342b1a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c74a770-4e4f-4cd2-9489-2fa61e74fb80",
    "visible": true
}