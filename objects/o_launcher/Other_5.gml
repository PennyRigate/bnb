//LAUNCHER
global.red = r
global.green = g
global.blue = b
with o_checkbox {
if y = 97 then global.mono = visible
if y = 75 then global.infinilives = visible
if y = 31 then global.saving = visible
if y = 9 then global.loading = visible
if y = 53 then global.twob = visible
}
//MAIN MENU
ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
global.kempston = ini_read_real("OPTIONS","KEMPSTON",0)
global.sound = ini_read_real("OPTIONS","SOUND",1)
global.k_left = ini_read_real("CONTROLS","LEFT",90)
global.k_right = ini_read_real("CONTROLS","RIGHT",88)
global.k_up = ini_read_real("CONTROLS","UP",75)
global.k_down = ini_read_real("CONTROLS","DOWN",77)
global.k_jump = ini_read_real("CONTROLS","JUMP",76)
global.hi[1] = ini_read_real("SCORE","1st",0)
ini_close()
