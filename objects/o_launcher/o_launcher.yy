{
    "id": "49d1c5c3-7032-4aba-b703-7f54556909b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_launcher",
    "eventList": [
        {
            "id": "47cc22d2-6a83-44bf-b286-3962d5c90c30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "49d1c5c3-7032-4aba-b703-7f54556909b3"
        },
        {
            "id": "af4f0cb7-3783-45f4-947f-b7c53ce12fc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "49d1c5c3-7032-4aba-b703-7f54556909b3"
        },
        {
            "id": "a437e995-68a9-4a08-bad4-626a759c5d87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "49d1c5c3-7032-4aba-b703-7f54556909b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9f1976ed-a1c3-4286-9ea5-8233f504e493",
    "visible": true
}