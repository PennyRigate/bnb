{
    "id": "81005b74-c390-4abf-a3a4-c49db6756386",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_note",
    "eventList": [
        {
            "id": "bd377a1c-0b75-4ef9-bd30-1def9393d3ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81005b74-c390-4abf-a3a4-c49db6756386"
        },
        {
            "id": "9f451ee9-6695-4eff-a906-358a8d5cb87b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9be14ecc-79ca-4189-bfd6-16b596600e1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "81005b74-c390-4abf-a3a4-c49db6756386"
        },
        {
            "id": "f1ff620a-9f9e-4884-9eef-d08123d4c519",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "81005b74-c390-4abf-a3a4-c49db6756386"
        },
        {
            "id": "8f56f929-92c4-4335-a00e-3c5391971dd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "81005b74-c390-4abf-a3a4-c49db6756386"
        },
        {
            "id": "804c35c3-2eee-4dcb-a5a0-bb919fa9d516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7587def-8e66-49ca-80e0-4df7e1c6e884",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "81005b74-c390-4abf-a3a4-c49db6756386"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
    "visible": true
}