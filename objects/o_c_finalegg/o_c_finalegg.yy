{
    "id": "85d189df-bf82-47d3-a810-803284e7de38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_finalegg",
    "eventList": [
        {
            "id": "9e91157d-35bf-464a-84c7-17a504e097e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "85d189df-bf82-47d3-a810-803284e7de38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22dcc40e-2496-457f-8a89-77ea0acaa8d0",
    "visible": true
}