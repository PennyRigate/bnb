{
    "id": "f226f0ca-99f3-464e-988d-7da5bd3d810a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_water",
    "eventList": [
        {
            "id": "9d77d4f7-3ee9-4346-aff6-3fe7ac1706ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f226f0ca-99f3-464e-988d-7da5bd3d810a"
        },
        {
            "id": "266bf4ed-5ff1-4246-a2c9-30c0ee956885",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f226f0ca-99f3-464e-988d-7da5bd3d810a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b993b73-73b9-4ae4-8dce-671df19ccc32",
    "visible": true
}