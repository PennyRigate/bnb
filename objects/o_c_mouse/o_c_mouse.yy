{
    "id": "ba265c10-4272-4e3c-a5e1-0ba208140dcd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_mouse",
    "eventList": [
        {
            "id": "e9aaa145-6c9a-4a40-ad23-4a8f1fc490a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba265c10-4272-4e3c-a5e1-0ba208140dcd"
        },
        {
            "id": "ee794c2f-f391-49f3-9c6c-b18d4a06e701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ba265c10-4272-4e3c-a5e1-0ba208140dcd"
        },
        {
            "id": "cc725af6-11fa-49a0-b251-9af15a0692e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ba265c10-4272-4e3c-a5e1-0ba208140dcd"
        },
        {
            "id": "cdeb2b87-786a-4ed2-9b02-2af005473a1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a6bbec2-d31b-4cca-9be1-b6ae748b1beb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ba265c10-4272-4e3c-a5e1-0ba208140dcd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
    "visible": true
}