{
    "id": "abad6b81-a916-49a6-9e13-802e26d7ace5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_lavarock",
    "eventList": [
        {
            "id": "9241ad2f-83de-435e-b1d9-b68a63a4baeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "abad6b81-a916-49a6-9e13-802e26d7ace5"
        },
        {
            "id": "b44c7460-a006-4c5d-a09a-72d118a30176",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "abad6b81-a916-49a6-9e13-802e26d7ace5"
        },
        {
            "id": "5f3f371d-5191-48b8-9609-d9e1d8ee1f6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "abad6b81-a916-49a6-9e13-802e26d7ace5"
        },
        {
            "id": "f292585a-2259-4c6a-89a5-f5d00d6260ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1f61bd02-edd0-4eea-86fe-2de0ec834403",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "abad6b81-a916-49a6-9e13-802e26d7ace5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22dcc40e-2496-457f-8a89-77ea0acaa8d0",
    "visible": true
}