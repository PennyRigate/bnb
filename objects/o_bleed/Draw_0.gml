if o_player.d_viewcolours = 0 then
{	
	
if !surface_exists(surf)
    {
    var _cw = camera_get_view_width(view_camera[0]);
    var _ch = camera_get_view_height(view_camera[0]);
    surf = surface_create(_cw, _ch);
    surface_set_target(surf);
    draw_set_colour(c_black);
    draw_set_alpha(0);
    draw_rectangle(0, 0, 256, 192, false);
    surface_reset_target();
    }
	else
{
if (surface_exists(surf)) {
var _cw = camera_get_view_width(view_camera[0]);
var _ch = camera_get_view_height(view_camera[0]);
//var _cx = camera_get_view_x(view_camera[0]);
//var _cy = camera_get_view_y(view_camera[0]);
//var _sw = sprite_width / 2;
//var _sh = sprite_height / 2;
surface_set_target(surf);
draw_set_color(c_black);
draw_set_alpha(1);
draw_rectangle(0, 0, 256, 192, 0);
gpu_set_blendmode(bm_subtract);
global.monocol = make_colour_rgb(global.red,global.green,global.blue)
draw_set_color(global.monocol);
var lay_id = layer_get_id("env");
var map_id = layer_tilemap_get_id(lay_id);
draw_tilemap(map_id, 0 - (256 * (o_player.quadrant - 1)), 0);
with (o_light)
    {
switch(object_index)
{
case o_floor:
    zx_draw()
    break;
case o_ladder:
    zx_draw()
    break;
case o_player:
    zx_draw()
    break;
case o_egg:
    zx_draw()
    break;
case o_racingmouse:
   zx_draw()
    break;
case o_note:
    zx_draw()
    break;
case o_jig:
    zx_draw()
    break;
case o_topper:
    zx_draw()
    break;
case o_egg_proj:
   draw_sprite_ext(s_egg_proj,0, round(x), round(y), image_xscale, image_yscale, image_angle, global.monocol, 1);
    break;
case o_gdact:
    zx_draw()
    break;
case o_notedoor:
    if dontdrawme = 0 then draw_sprite_ext(s_notedoor,image_index, round(x) , round(y) , image_xscale, 1, 0, global.monocol, 1);
	draw_set_font(o_bleed.f_smal)
	if image_xscale = 1 then reqloc = 12 else reqloc = -10
	if image_xscale = 1 then draw_set_halign(fa_left) else draw_set_halign(fa_right)
	if reqyes = 1 then draw_text(x+(reqloc),y,string("A") + string(req))
	draw_set_font(f_)
	draw_set_halign(fa_left)
	break;
case o_jinjo:
    zx_draw()
    break;
case o_corn:
    zx_draw()
    break;
case o_turniwood:
    draw_sprite_ext(sprite_index,image_index, round(x), round(ystart), image_xscale, image_yscale, image_angle, global.monocol, 1);
    break;
case o_rock:
    zx_draw()
    break;
case o_jigdoor:
    zx_draw()
	draw_set_font(o_bleed.f_smal)
	draw_set_halign(fa_center)
	if open = 0 then draw_text(x+8,y-8,string("B") + string(o_player.initjig + req))
	draw_set_font(f_)
	draw_set_halign(fa_left)
	break;
case o_snippet:
    zx_draw()
    break;
case o_water:
    zx_draw()
    break;
case o_snacker:
    draw_sprite_ext(sprite_index,image_index, round(x), round(y), image_xscale, -dir, image_angle, global.monocol,visible);
    break;
	case o_lavarock:
    zx_draw()
    break;
	case o_hothand:
    zx_draw()
    break;
	case o_nonsolid:
    zx_draw()
    break;
	case o_secam:
    zx_draw()
	if o_player.alert[(y - 4) / 8] = 1 then draw_sprite_ext(s_alert,0,x,y-5,dir,1,0,global.monocol,1)
    break;
	case o_tintop:
    zx_draw()
    break;
	case o_klang:
    zx_draw()
    break;
	case o_drop:
    zx_draw()
    break;
	case o_grunty:
    zx_draw()
    break;
	case o_sns:
    zx_draw()
    break;
}
    }
	
draw_sprite_ext(s_scorebord,global.mono,x,y,1,1,0,global.monocol,1)
if global.scor > 99999 then additzero = ""
if global.scor < 100000 then additzero = "0"
if global.scor < 10000 then additzero = "00"
if global.scor < 1000 then additzero = "000"
if global.scor < 100 then additzero = "0000"
if global.scor < 10 then additzero = "00000"
if global.scor < 999999 then draw_text(47,-5,string(additzero) + string(global.scor)) else draw_text(47,-5,"LOTS")
draw_text(110,-5,global.eggs)
if global.notes < 100 then additzero = ""
if global.notes < 10 then additzero = "0"
draw_text(131,-5,string(additzero) + string(global.notes))
if global.jiggies < 1000 then additzero = ""
if global.jiggies < 100 then additzero = "0"
if global.jiggies < 10 then additzero = "00"
if global.jiggies < 1000 then draw_text(161,-5,string(additzero) + string(global.jiggies)) else draw_text(161,-5,"LOTS")
if global.scor < global.hi[1] then 
{
if global.hi[1] > 99999 then additzero = ""
if global.hi[1] < 100000 then additzero = "0"
if global.hi[1] < 10000 then additzero = "00"
if global.hi[1] < 1000 then additzero = "000"
if global.hi[1] < 100 then additzero = "0000"
if global.hi[1] < 10 then additzero = "00000"
if global.scor < 999999 then draw_text(207,-5,string(additzero) + string(global.hi[1])) else draw_text(207,-5,"LOTS")
} 
else {
if global.scor > 99999 then additzero = ""
if global.scor < 100000 then additzero = "0"
if global.scor < 10000 then additzero = "00"
if global.scor < 1000 then additzero = "000"
if global.scor < 100 then additzero = "0000"
if global.scor < 10 then additzero = "00000"
if global.scor < 999999 then draw_text(207,-5,string(additzero) + string(global.scor)) else draw_text(207,-5,"LOTS")
}

if room = r_spiralmountain then draw_sprite_ext(s_levelbord,0,0,184,1,1,0,global.monocol,1)
if room = r_treasuretrovecove then draw_sprite_ext(s_levelbord,1,0,184,1,1,0,global.monocol,1)
if room = r_hailfire then draw_sprite_ext(s_levelbord,2,0,184,1,1,0,global.monocol,1)
if room = r_gindustries then draw_sprite_ext(s_levelbord,3,0,184,1,1,0,global.monocol,1)
if global.live > -1 then {if global.doro = 0 then draw_sprite_ext(s_lives,global.live,4,184,1,1,0,global.monocol,1) else draw_sprite_ext(s_lives_doro,global.live,4,184,1,1,0,global.monocol,1)}
if global.jinjo[4] = 1 then draw_sprite_ext(s_jinjos,0,0,184,1,1,0,global.monocol,1)
if global.jinjo[3] = 1 then draw_sprite_ext(s_jinjos,1,0,184,1,1,0,global.monocol,1)
if global.jinjo[2] = 1 then draw_sprite_ext(s_jinjos,2,0,184,1,1,0,global.monocol,1)
if global.jinjo[1] = 1 then draw_sprite_ext(s_jinjos,3,0,184,1,1,0,global.monocol,1)
if global.jinjo[0] = 1 then draw_sprite_ext(s_jinjos,4,0,184,1,1,0,global.monocol,1)

//
gpu_set_blendmode(bm_normal);
draw_set_alpha(1);
surface_reset_target();
draw_surface(surf, 0, 0);
}}



draw_set_font(f_)
draw_set_colour(c_white)


}

//debug
if image_index % 10 = 0 then fps_slow = fps_real
draw_set_color(c_orange)
if o_player.d_ebug = 1 then draw_text(4,16,string("Q ") + string(o_player.quadrant))
if o_player.d_ebug = 1 then draw_text(4,32,string("FPS ") + string(fps))
if o_player.d_ebug = 1 then draw_text(4,48,string("INST ") + string(instance_count))
if o_player.d_ebug = 1 then if keyboard_check_pressed(ord("T")) then inst ++
if o_player.d_ebug = 1 then if keyboard_check(ord("J")) then with o_light {draw_text(x,y,object_index)}
if o_player.d_ebug = 1 then if instance_exists(inst) then draw_text(4,64,string("? ") + string(keyboard_lastkey))
if o_player.d_ebug = 1 then if mouse_check_button(mb_left) then instance_create_depth(mouse_x,mouse_y,0,1)
if o_player.room_life < 2 then 
{
draw_set_color(c_black);
draw_set_alpha(1);
draw_rectangle(0, 0, 256, 192, 0);
}

if o_player.ls = 1 then {
draw_set_color(c_black);
draw_set_alpha(1);
draw_rectangle(0, 0, 256, 192, 0);
}