col = instance_create_depth(x,y,depth,o_c_egg)
col.sprite_index = s_col_meg
r = irandom_range(1,10)
if room = r_spiralmountain then {image_index = 0;if global.loop < 1 then instance_destroy(self,true)}
if room = r_treasuretrovecove then {image_index = 1;if global.loop != 1 or global.jiggies < 25 then instance_destroy(self,true)}
if room = r_hailfire then {image_index = 2;if r != 2 then instance_destroy(self,true)}
if room = r_gindustries then {image_index = 3;if global.loop != 0 or global.perfect = 0 then instance_destroy(self,true)}
col.image_index = image_index
instance_destroy(self,false)