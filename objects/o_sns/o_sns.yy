{
    "id": "54a0a6a1-13a2-4a40-ac2d-af833487ae05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sns",
    "eventList": [
        {
            "id": "14a6a97b-14e0-408c-b9ae-16966f8d9e6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54a0a6a1-13a2-4a40-ac2d-af833487ae05"
        },
        {
            "id": "e844c213-f397-4a15-a4f8-e7f591d83b72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54a0a6a1-13a2-4a40-ac2d-af833487ae05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
    "visible": false
}