{
    "id": "6bec89c4-e9a5-4708-8ce5-8d386fa48aaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hothand",
    "eventList": [
        {
            "id": "4924a060-59a6-4050-8a23-f752cb1181a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6bec89c4-e9a5-4708-8ce5-8d386fa48aaf"
        },
        {
            "id": "a3dd50b7-068c-4c7d-af3e-0621980ddcea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6bec89c4-e9a5-4708-8ce5-8d386fa48aaf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4877827a-8f84-48b4-9f40-88586d426475",
    "visible": true
}