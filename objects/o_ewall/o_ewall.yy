{
    "id": "e73bf218-69b2-4c8f-bc2a-7afbf7a64b4c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ewall",
    "eventList": [
        {
            "id": "e5ad320a-71c0-41b4-8a22-8d1c69a0c684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e73bf218-69b2-4c8f-bc2a-7afbf7a64b4c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7b42cda-81f7-41aa-8b0d-a5d1a20378bd",
    "visible": false
}