window_set_min_width(768)
window_set_min_height(576)
class = 6
global.gbl = 0
ini_open(string(environment_get_variable("APPDATA") + "/playground/gs.zed"))
ini_write_real("GS","played",1)
ini_close()
ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
global.hi[1] = ini_read_real("SCORE","1st",0)
//LAUNCHER
global.mono = ini_read_real("EXTRA","MONOCHROME",0)
global.red = ini_read_real("EXTRA","R",255)
global.green = ini_read_real("EXTRA","G",255)
global.blue = ini_read_real("EXTRA","B",255)
global.doro = ini_read_real("EXTRA","DOROTHY",0)
global.infinilives = ini_read_real("EXTRA","INFINILIVES",0)
global.saving = ini_read_real("EXTRA","SAVING",1)
global.loading = ini_read_real("EXTRA","LOADING",0)
global.twob = ini_read_real("EXTRA","TWOBUTTON",1)
window_set_fullscreen(ini_read_real("EXTRA","FULLSCR",0))
//MAIN MENU
global.kempston = ini_read_real("OPTIONS","KEMPSTON",0)
global.sound = ini_read_real("OPTIONS","SOUND",1)
global.k_left = ini_read_real("CONTROLS","LEFT",90)
global.k_right = ini_read_real("CONTROLS","RIGHT",88)
global.k_up = ini_read_real("CONTROLS","UP",75)
global.k_down = ini_read_real("CONTROLS","DOWN",77)
global.k_jump = ini_read_real("CONTROLS","JUMP",76)
ini_close()
if global.saving = 0 then 
{
	global.hi[1] = 0
	global.kempston = 0
	global.sound = 1
	global.k_left = 90
	global.k_right =  88
	global.k_up = 75
	global.k_down = 77
	global.k_jump = 76
}

room_goto_next()
