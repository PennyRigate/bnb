{
    "id": "453e796d-fd88-4261-b8db-7a25506b9e78",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_snacker",
    "eventList": [
        {
            "id": "e111754a-2761-4241-b141-94486bccdd09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "453e796d-fd88-4261-b8db-7a25506b9e78"
        },
        {
            "id": "61015957-72a3-4ae7-b99d-11891524133d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "453e796d-fd88-4261-b8db-7a25506b9e78"
        },
        {
            "id": "c6f4833b-09e5-4571-a000-0fe8b7bb9d6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f226f0ca-99f3-464e-988d-7da5bd3d810a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "453e796d-fd88-4261-b8db-7a25506b9e78"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bbc9713-33f2-4d8e-9a4f-a4f7e6fb0136",
    "visible": true
}