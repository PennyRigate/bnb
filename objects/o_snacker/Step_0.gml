if o_player.pause = 0 then y += 1.5 * dir
if place_meeting(x,y+dir,o_ewall) or y > obj.y + 8 + tim then {y -= dir;dir *= -1}
if !place_meeting(x,y+dir,o_water) then jumptime = -1
if place_meeting(x,y,o_water) then if jumptime = -1 then jumptime = jumptimeinit
if o_player.pause = 0 then if jumptime > 0 then jumptime -= 1
if y > obj.y then 
{
if place_meeting(x,y+6,obj) then {sprite_index = s_snacker_small;visible = 1} else visible = 0 
} else sprite_index = s_snacker

if jig = 1 then if instance_exists(o_snacjig) then
{
if visible = 1 then {o_snacjig.y = y + 16;o_snacjig.x = x-4} 
if o_snacjig.y >= ystart then o_snacjig.y = 268
col.sprite_index = s_col_snacker_jig
}
else {col.sprite_index = s_col_snacker;jig = 0}
if visible = 0 then class = 99 else class = 2
col.visible = visible
if o_player.pause = 1 then image_speed = 0 else image_speed = 1