{
    "id": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_light",
    "eventList": [
        {
            "id": "8e933ea5-e8af-4b1c-96ab-58fd6ae0428f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "235aafd2-f367-4ae3-b951-ac2c0c280307"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f08c605c-3006-47da-b801-1a325a048c06",
    "visible": true
}