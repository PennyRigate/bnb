{
    "id": "f28d5c2d-c665-4183-b2f7-cb758ad2f73c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_egg",
    "eventList": [
        {
            "id": "216541ba-47a7-4873-909f-7ebd8928b2cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f28d5c2d-c665-4183-b2f7-cb758ad2f73c"
        },
        {
            "id": "7511a4b6-2395-481a-b9eb-0223d3be6e2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f28d5c2d-c665-4183-b2f7-cb758ad2f73c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b44e3532-1dad-4ceb-8653-0607eb068b3f",
    "visible": true
}