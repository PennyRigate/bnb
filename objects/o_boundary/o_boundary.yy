{
    "id": "3e39aa47-aeab-476f-9f86-12fe19439f0a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_boundary",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "fc7012c9-0e70-4fff-b4ee-00e5a59b8a29",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "28b1dbc9-13ff-4b80-9409-65a2faa0bb82",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 0
        },
        {
            "id": "0242cbf9-b2c4-4073-879e-38aff7291856",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 192
        },
        {
            "id": "a75b646d-34c8-4894-8837-eaa24751384d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 192
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7c3a2be5-7332-4cfd-92b5-b8570d32fa42",
    "visible": true
}