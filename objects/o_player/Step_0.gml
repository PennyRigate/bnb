if climbing = 0 then
{
//movement
if pause = 0 then y += vsp

if pause = 0 then {if jumps < 2 or not grounded = 1  then vsp += 0.25}
if pause = 0 then {if flaptimer = 0 then {sprite_index = global.jumpspr;image_index = 0}}
if pause = 0 then {if keyboard_check(global.k_left) then if (grounded = 1 or canmoveinair = 1) then {if pause = 0 then x -= 1; image_xscale = -1}}
if pause = 0 then {if keyboard_check(global.k_right) then if (grounded = 1 or canmoveinair = 1) then {if pause = 0 then x += 1; image_xscale = 1}}
if not keyboard_check(global.k_right) if not keyboard_check(global.k_left) then if grounded = 1 then
{
sprite_index = global.idlespr
}
if keyboard_check(global.k_right) or keyboard_check(global.k_left) then if grounded = 1 then sprite_index = global.walkspr
if pause = 0 then {if grounded = 1 then if keyboard_check_pressed(global.k_jump) then {y -= 1;vsp = -3;jumps = 1;sprite_index = global.jumpspr;image_index = 0;zx_beep(a_jump);canmoveinair = 1}}
if keyboard_check_released(global.k_jump) then jumps -= 1
if pause = 0 then {if keyboard_check_pressed(global.k_jump) then if jumps = 0 then {vsp = -2; jumps = 0; sprite_index = global.flyspr;image_index = 0;flaptimer = 30;zx_beep(a_bree);canmoveinair = 1}}
if flaptimer = 20 then image_index = 1
if pause = 0 then if keyboard_check_pressed(global.k_down) then {if global.eggs > 0 then {global.eggs -= 1;zx_beep(a_egg_shoot);egg = instance_create_depth(x+(8*image_xscale),y,depth,o_egg_proj);}}
if vsp <= -3 then canmoveinair = 1

}


//sys
if sprite_index = global.jumpspr and grounded = 0 then image_speed = 0 else image_speed = 1
flaptimer -= 1
if vsp > 4 then vsp = 4
if vsp < -4 then vsp = -4
//hcollision
if place_meeting(x+image_xscale,y,o_floor) then x -= image_xscale

if image_xscale = 1 then facekey = global.k_right else facekey = global.k_left

//vcollision
if place_meeting(x,y+vsp,o_floor)
{
    while (!place_meeting(x,y+sign(vsp),o_floor)) y+=sign(vsp);
    vsp = 0
}


//climbing
	if place_meeting(x,y,o_ladder) then {climbing = 1;if climbing = 0 then y -= 1} else climbing = 0
if climbing = 1 then
{
	if place_meeting(x,y-1,o_floor) then y += 1
	vsp = 0
	canmoveinair = 1
	if place_meeting(x,y,o_h) then x -= image_xscale
sprite_index = global.climbspr
if (!keyboard_check(global.k_up) and !keyboard_check(global.k_down)) or (keyboard_check(global.k_up) and keyboard_check(global.k_down)) then image_speed = 0 
if pause = 0 then {if keyboard_check(global.k_up) then {y -= 0.65;if !keyboard_check(global.k_down) image_speed = 1}}
if pause = 0 then {if keyboard_check(global.k_down) then if not place_meeting(x,y+0.65,o_floor) then {vsp = 0;y += 0.65;if !keyboard_check(global.k_up) then image_speed = 1}}
if pause = 0 then {if ((keyboard_check_pressed(global.k_jump) and !keyboard_check(facekey)) or (!place_meeting(x+image_xscale,y-4,o_ladder and keyboard_check(global.k_up)) and grounded = 0)) then {x-=image_xscale * 3;y -= 1;vsp = -3;if not keyboard_check(global.k_jump) then jumps = 0 else jumps = 1;sprite_index = global.jumpspr;image_index = 0}}


if pause = 0 then if keyboard_check(global.k_up) then if not audio_is_playing(a_climb) then {audio_stop_all();if global.sound = 1 then audio_play_sound(a_climb,1,1)}
if pause = 0 then if keyboard_check(global.k_down) then if not audio_is_playing(a_bmilc) then {audio_stop_all();if global.sound = 1 then audio_play_sound(a_bmilc,1,1)}
if !keyboard_check(global.k_up) then audio_stop_sound(a_climb)
if !keyboard_check(global.k_down) then audio_stop_sound(a_bmilc)
if keyboard_check(global.k_down) then audio_stop_sound(a_climb)
if keyboard_check(global.k_up) then audio_stop_sound(a_bmilc)
}

if climbing = 0 then audio_stop_sound(a_climb)
if climbing = 0 then audio_stop_sound(a_bmilc)

//Debug
mask_index = m_banjo
if keyboard_check_pressed(ord("4")) then keyboard_string = ""
if keyboard_string = "15" then {d_ebug = 1;if pause != 0 then pause = 3}
if d_ebug = 1 then
{
if keyboard_check(ord("I")) then d_viewcolours = 1 else d_viewcolours = 0
if keyboard_check(ord("R")) then game_restart()
if keyboard_check(vk_add) then global.scor += 1
if keyboard_check(ord("Y")) then room_goto(r_obligatorytestroom)
}

//scrolling
if x != 255 and x != 1 then
{
if instance_exists(o_boundary) then
{
if o_boundary.x < 144 then quadrant = 1 else quadrant = 3
}
else quadrant = 2
}

if spawnjinjo = 1 then
{
	if quadrant = 1 then {instance_create_depth(xstart,ystart,depth,o_jig);spawnjinjo = 0}
}
ls = 0
if x = 256 then 
{
	x += 2
	ls = 1
	with o_light
	{
		x -= 256
		if class = 3 then instance_destroy(self,false)
	}
}

if x = 0 then 
{
	x -= 2
	ls = 1
	with o_light
	{
		x += 256
		if class = 3 then instance_destroy(self,false)
	}
}

//culling
if global.mono = 1 then
{
with o_light
{
	if class = 4 then visible = 0
}
}

if (x < 255 and x > 1) then
	{
		instance_deactivate_region(0,0,256,1080,false,true)
	}
else {instance_activate_all()}

if d_ebug = 1 then if keyboard_check(ord("8")) then x = 255
if d_ebug = 1 then if keyboard_check_pressed(ord("P")) then room_goto_next()

if global.kempston = 1 or global.kempston = -1 then
{
global.kempston = -1
if gamepad_is_connected(0) then {cunt = 0;global.kempston = 1}
if gamepad_is_connected(1) then {cunt = 1;global.kempston = 1}
if gamepad_is_connected(2) then {cunt = 2;global.kempston = 1}
if gamepad_is_connected(3) then {cunt = 3;global.kempston = 1}
if global.kempston = 1 and cunt > -1 then
{
	//NO KEYBOARD ALLOWED
	if gamepad_button_check_pressed(cunt,gp_face1) then global.k_jump = global.g_jump else global.k_jump = noone
	if gamepad_button_check(cunt,gp_padr) then global.k_right = global.g_right else global.k_right = noone
	if gamepad_button_check(cunt,gp_padl) then global.k_left = global.g_left else global.k_left = noone
	if gamepad_button_check(cunt,gp_padu) then global.k_up = global.g_up else global.k_up = noone
	if gamepad_button_check(cunt,eggkey) then global.k_down = global.g_down else global.k_down = noone
	//CONVERSION
	if gamepad_button_check_pressed(cunt,gp_face1) then keyboard_key_press(global.g_jump)
	if gamepad_button_check_released(cunt,gp_face1) then {keyboard_key_release(global.g_jump);jumps -= 1}
	if gamepad_button_check_pressed(cunt,gp_padr) then keyboard_key_press(global.g_right)
	if !gamepad_button_check(cunt,gp_padr) then keyboard_key_release(global.g_right)
	if gamepad_button_check_pressed(cunt,gp_padl) then keyboard_key_press(global.g_left)
	if !gamepad_button_check(cunt,gp_padl) then keyboard_key_release(global.g_left)
	if gamepad_button_check_pressed(cunt,gp_padu) then keyboard_key_press(global.g_up)
	if !gamepad_button_check(cunt,gp_padu) then keyboard_key_release(global.g_up)
	if gamepad_button_check_pressed(cunt,eggkey) then keyboard_key_press(global.g_down)
	if !gamepad_button_check(cunt,eggkey) then keyboard_key_release(global.g_down)
	if twob = 0 then eggkey = gp_padd else
	{
		if climbing = 1 then eggkey = gp_padd else eggkey = gp_face3
	}
	
}}
//
zx_pause()
global.b_time += 1
if room = r_hailfire 
{
if lavarockjig = 0 then {if !instance_exists(o_lavarock) then {if pause = 0 then lrtimer ++}} else {if pause = 0 then lrtimer += 0.75}
if lrtimer >  450 then if !instance_exists(o_lavarock) then if pause = 0 then {if x != xstart and x > 24 and x < 232 then instance_create_depth(x,0,0,o_lavarock);lrtimer = 0}
}
//die
if global.live < 0 then 
	{
		if global.scor > global.hi[1] then 
		{
		ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
		ini_write_real("SCORE","1st",global.scor)
		ini_close()
		}
		if room != r_gover then room_goto(r_gover)
	}
//restart
if room = r_gover then {if keyboard_check_pressed(ord("Y")) then {global.gbl = 0;room_goto(r_spiralmountain)};if keyboard_check_pressed(ord("N")) then {global.gbl = 0;room_goto(r_title)}}

if room_life = 1 then 
{
with o_light
{

	if class = 2 or class = 1 or class = 3 then
	{
	p_x = x
	p_y = y
	p_r_is = image_speed
	p_xsc = image_xscale
	p_si = sprite_index
	if object_index = o_secam then p_timer = timer
	}
	if class = 1 then
	{
		p_vsp = vsp
		p_ft = flaptimer
		p_jmps = jumps
		p_ii = image_index
	}
	if object_index = o_turniwood then p_ii = image_index
	if object_index = o_jinjo then p_r_is = image_speed
}
}
//
if room != r_gover and room != r_bottles then if inv = 0 then
{
if y > 200 then
{
	if instance_exists(o_lavarock) then o_lavarock.stop = 1
	x = xstart
	if !(room = r_treasuretrovecove and quadrant = 3) then y = ystart else y = 16
	vsp = 0
	global.b_perfect = 0
	
	global.live -= 1 + global.loop
	zx_beep(a_hurt)
}}

//spawn
if room = r_treasuretrovecove then {if quadrant = 3 then yspawn = 72 else yspawn = ystart}
if room = r_hailfire then {if quadrant = 2 then yspawn = 136 else yspawn = ystart}
if room = r_gindustries then {if quadrant = 2 then yspawn = 72 else yspawn = ystart}
if global.live > 5 then global.live = 5
//


if place_meeting(x,y+1,o_floor) then {grounded = 1;jumps = 2;canmoveinair = 0}
if !place_meeting(x,y+1,o_floor) then grounded = 0
if place_meeting(x,y+1,o_turniwood) then y = round(y)
if global.b_perfect = 0 then global.perfect = 0

//
if !keyboard_check(global.g_right) and !keyboard_check(global.k_right) then if x = 255 then x ++
if !keyboard_check(global.g_left) and !keyboard_check(global.k_left) then if x = 1 then x --
//Medals
if room = r_spiralmountain and global.b_jig = 5 and global.notes = 50 then global.medal[1] = 1
if room = r_treasuretrovecove and global.b_jig = 5 and global.notes = 50 then global.medal[2] = 1
if room = r_hailfire and global.b_jig = 5 and global.notes = 50 then global.medal[3] = 1
if room = r_gindustries and global.b_jig = 5 and global.notes = 50 then global.medal[4] = 1

if room = r_hailfire && y < 4 {if x > 240 x = 240}