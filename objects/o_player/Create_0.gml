randomize()
si = sprite_index
ii = 0
inv = 0
yspawn = ystart
alert = 0
lavarockjig = 0
collis = 1
lrtimer = 0
depth = -11
fi = 11
monoinit = global.mono
hurtevent = 0
eggkey = gp_padd
twob = 1
global.levelscore = 0
spawnjinjo = 0
quadrant = 1
grounded = 0
room_speed = 50
vsp = 0
climbing = 0
jumps = 0
flaptimer = -1
facekey = global.k_right
d_viewcolours = 0
d_ebug = 0
pause = 0
global.jinjo[0] = 0
global.jinjo[1] = 0
global.jinjo[2] = 0
global.jinjo[3] = 0
global.jinjo[4] = 0

if global.gbl = 0 then
{
global.medal[5] = 1
global.loop = 0
global.live = 2
global.scor = 0
global.eggs = 0
global.jiggies = 0
global.perfect = 1
ini_open(string(environment_get_variable("APPDATA") + "/playground/scores.zed"))
global.g_left = ini_read_real("CONTROLS","LEFT",90)
global.g_right = ini_read_real("CONTROLS","RIGHT",88)
global.g_up = ini_read_real("CONTROLS","UP",75)
global.g_down = ini_read_real("CONTROLS","DOWN",77)
global.g_jump = ini_read_real("CONTROLS","JUMP",76)
ini_close()

global.gbl = 1
}
if global.doro = 0 then
{
	global.idlespr = s_banjo_idle
	global.walkspr = s_banjo
	global.jumpspr = s_banjo
	global.climbspr = s_climb
	global.flyspr = s_flipflap
} 
else
{
	global.idlespr = s_doro_idle
	global.walkspr = s_doro
	global.jumpspr = s_doro_jump
	global.climbspr = s_doro_climb
	global.flyspr = s_doro_fly
}

mask_index = m_banjo
class = 1
room_life = 0
canmoveinair = 0

global.b_time = 0
global.b_perfect = 1
global.notes = 0
global.b_jig = 0
initjig = global.jiggies
global.eggs = 0
ls = 0



