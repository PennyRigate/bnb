{
    "id": "be7c16cc-5092-4276-bca7-a812c19b355e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "944cc1c0-ba99-4eae-b6a5-2192e6b0d227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "705b87c9-df87-48d4-96a1-f1d422c9a717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "e7b18050-7957-4635-aad4-c32831a44ee0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f28d5c2d-c665-4183-b2f7-cb758ad2f73c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "9303d4c9-5562-4f45-a7f8-abfecb3b5878",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "6d27ba00-f266-402f-a074-2044c35dc976",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9be14ecc-79ca-4189-bfd6-16b596600e1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "9c0f5dfd-8e1e-45c3-ab72-9bbc75a4338b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a9c98e97-122e-43de-8b14-66f7f4737e03",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "3e3a357d-8898-4ad5-bfc7-30568980ed42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "978c6858-f96d-479d-8a60-ba6a8c0fe551",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "35ffce19-ec03-444f-9ac5-126f3d114d9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "24a9e369-0076-454a-85d2-03dc2fd51a3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a7587def-8e66-49ca-80e0-4df7e1c6e884",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "b2dfcb15-acfe-47c0-8d59-e8de7a0398b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "54a0a6a1-13a2-4a40-ac2d-af833487ae05",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        },
        {
            "id": "175c5c3c-129c-45ff-9175-7d00b41dd00f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "be7c16cc-5092-4276-bca7-a812c19b355e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2aa5491b-2abe-4c68-92a1-c6481b6a5033",
    "visible": true
}