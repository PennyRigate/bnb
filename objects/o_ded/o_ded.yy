{
    "id": "dba7e9bc-d10a-4f3b-aefc-137ed40a949e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ded",
    "eventList": [
        {
            "id": "7f82b54c-47e9-4057-8c61-54aa533fcad7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dba7e9bc-d10a-4f3b-aefc-137ed40a949e"
        },
        {
            "id": "0f851439-51e0-43d6-8cc2-ce7b8af28370",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dba7e9bc-d10a-4f3b-aefc-137ed40a949e"
        },
        {
            "id": "bc74259b-41da-4e0c-8846-1ee102bc97e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "dba7e9bc-d10a-4f3b-aefc-137ed40a949e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1459038-39fd-474a-b4e8-c22dce01f859",
    "visible": true
}