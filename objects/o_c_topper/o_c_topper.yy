{
    "id": "077455b2-6ef8-4e68-91d2-d1196a6fa2b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_topper",
    "eventList": [
        {
            "id": "479b23c4-0928-40c8-a866-5c8e289f1953",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "077455b2-6ef8-4e68-91d2-d1196a6fa2b6"
        },
        {
            "id": "6c333c7b-5ed6-4f9e-8ead-904db442b4c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "077455b2-6ef8-4e68-91d2-d1196a6fa2b6"
        },
        {
            "id": "d90f6285-9204-4592-8198-e187c979ac24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "077455b2-6ef8-4e68-91d2-d1196a6fa2b6"
        },
        {
            "id": "5bfc182a-d05e-41f9-9a20-e72be42bfd0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a99aca33-74b0-4752-9b33-6386ca6c8859",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "077455b2-6ef8-4e68-91d2-d1196a6fa2b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b3502b1-93b0-4518-818f-deb999908435",
    "visible": true
}