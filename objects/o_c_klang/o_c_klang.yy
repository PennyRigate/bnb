{
    "id": "82aa6ed4-a731-4035-9348-791f13cf0002",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_klang",
    "eventList": [
        {
            "id": "8ce7c01e-4931-4c33-96d9-99f26335d761",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82aa6ed4-a731-4035-9348-791f13cf0002"
        },
        {
            "id": "df79085b-76fc-47a1-adeb-a9f2afae0278",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "82aa6ed4-a731-4035-9348-791f13cf0002"
        },
        {
            "id": "20e286ce-cc43-4e8b-ad8c-7ec7523c5001",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "82aa6ed4-a731-4035-9348-791f13cf0002"
        },
        {
            "id": "815c638e-d50f-4e4e-a884-b75e94460d88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "04a02bb4-a395-4aea-afea-030783077755",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "82aa6ed4-a731-4035-9348-791f13cf0002"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "632e04a1-6ee9-4bde-b6d3-a5b2ddee366d",
    "visible": true
}