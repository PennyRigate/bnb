{
    "id": "2e7f5ec6-a408-423f-87ea-8dee40f4b1a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_racingmouse",
    "eventList": [
        {
            "id": "de7d8115-0323-4f61-8630-a21a91e2869b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e7f5ec6-a408-423f-87ea-8dee40f4b1a0"
        },
        {
            "id": "a54878e1-6b77-40db-8bf2-b0759ca76ca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e7f5ec6-a408-423f-87ea-8dee40f4b1a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3eeb7a2-be82-4c46-befa-e3754cc1c79b",
    "visible": true
}