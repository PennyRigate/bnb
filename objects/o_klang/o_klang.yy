{
    "id": "04a02bb4-a395-4aea-afea-030783077755",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_klang",
    "eventList": [
        {
            "id": "6ea3d7d2-87d2-47fb-9e88-7f6afd36f46f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04a02bb4-a395-4aea-afea-030783077755"
        },
        {
            "id": "f0c38110-cf66-4942-94ff-8aea62e5c82a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04a02bb4-a395-4aea-afea-030783077755"
        },
        {
            "id": "05664bf6-be0d-4bc3-a3e9-f9a5779092ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e73bf218-69b2-4c8f-bc2a-7afbf7a64b4c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "04a02bb4-a395-4aea-afea-030783077755"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "477dab43-d99d-4ee6-ba85-e17db969c63f",
    "visible": true
}