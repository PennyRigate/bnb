{
    "id": "9b90c588-538d-452d-b791-3ff596e8475b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_jigdoor",
    "eventList": [
        {
            "id": "30953f2a-fca0-41d0-8300-2386841815d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b90c588-538d-452d-b791-3ff596e8475b"
        },
        {
            "id": "0088da3f-6c91-4cd3-9f0a-d637a16640e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9b90c588-538d-452d-b791-3ff596e8475b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2823d28-2f52-44d8-816d-a972c7b42c7f",
    "visible": true
}