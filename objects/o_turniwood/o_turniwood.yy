{
    "id": "70fdab11-ea41-49e3-8363-0ce8984a39a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_turniwood",
    "eventList": [
        {
            "id": "79f7dbd3-c324-4150-befd-59847921ca7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70fdab11-ea41-49e3-8363-0ce8984a39a4"
        },
        {
            "id": "2ab2ff8f-ab3d-4ac1-b98c-c6be200b592a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70fdab11-ea41-49e3-8363-0ce8984a39a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
    "visible": true
}