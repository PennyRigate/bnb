{
    "id": "8be45857-0c5f-48c1-b8c5-6e2c8b0b1053",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tintop",
    "eventList": [
        {
            "id": "1d0b0a29-336a-4934-9ca9-e98898a1bb43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8be45857-0c5f-48c1-b8c5-6e2c8b0b1053"
        },
        {
            "id": "5ebb6ad6-a1da-4a51-a2fb-a521f87a5a5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8be45857-0c5f-48c1-b8c5-6e2c8b0b1053"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1514996-194d-4f72-ba75-9cc1069464a0",
    "visible": true
}