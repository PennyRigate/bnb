{
    "id": "97a26562-178b-4b2d-a927-6d96358c07f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_checkbox",
    "eventList": [
        {
            "id": "dda3cd15-0e7e-4f32-8402-6fa0300036e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "97a26562-178b-4b2d-a927-6d96358c07f2"
        },
        {
            "id": "004772ae-0518-47d5-bf13-a96e04351f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97a26562-178b-4b2d-a927-6d96358c07f2"
        },
        {
            "id": "3ab8dfe8-b0c3-4d9d-85eb-e4f0f3500ac4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "97a26562-178b-4b2d-a927-6d96358c07f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
    "visible": true
}