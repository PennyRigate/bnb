{
    "id": "4f645c1b-9f7e-4721-a442-095992376358",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_grunty",
    "eventList": [
        {
            "id": "2df83381-16bf-4cde-9b62-00694351f6ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f645c1b-9f7e-4721-a442-095992376358"
        },
        {
            "id": "73fb2905-866f-4644-888f-eaa5d2c778a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f645c1b-9f7e-4721-a442-095992376358"
        },
        {
            "id": "5841fcfe-8ee0-4a17-9f81-b4ed82c769e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a6bbec2-d31b-4cca-9be1-b6ae748b1beb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4f645c1b-9f7e-4721-a442-095992376358"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0608a7b-8675-474d-9049-cbdce3e33ee9",
    "visible": true
}