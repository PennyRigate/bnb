{
    "id": "35ffce19-ec03-444f-9ac5-126f3d114d9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ladder",
    "eventList": [
        {
            "id": "dae3013d-a18b-417b-a7af-7ebd543ed847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "35ffce19-ec03-444f-9ac5-126f3d114d9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
    "visible": true
}