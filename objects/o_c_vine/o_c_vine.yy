{
    "id": "66800801-3bce-44f3-9230-093abd7c560f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_vine",
    "eventList": [
        {
            "id": "52f0ae19-6ad3-4a89-b2cc-338177377333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "66800801-3bce-44f3-9230-093abd7c560f"
        },
        {
            "id": "8189e9d1-e2e7-43f1-91b9-24e628bdfcb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "66800801-3bce-44f3-9230-093abd7c560f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
    "visible": true
}