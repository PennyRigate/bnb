{
    "id": "e4743fd0-60ae-497b-a41b-8a41959a0f4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_h",
    "eventList": [
        {
            "id": "7885f57f-003e-49af-9802-c2893d7fda2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e4743fd0-60ae-497b-a41b-8a41959a0f4d"
        },
        {
            "id": "908d6309-409b-474d-82a4-81fb26d518f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e4743fd0-60ae-497b-a41b-8a41959a0f4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7747a132-b0bb-4f16-94c4-311d8bacd198",
    "visible": true
}