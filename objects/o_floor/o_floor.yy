{
    "id": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_floor",
    "eventList": [
        {
            "id": "e96650aa-2305-4877-889d-94f061bf5c24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a"
        },
        {
            "id": "5fd87723-6d94-458b-a5a6-5ae0c06ea52a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cc4c9336-d02e-4d18-80fd-f8c6c99e257a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
    "visible": true
}