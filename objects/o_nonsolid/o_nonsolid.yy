{
    "id": "f878563b-bc94-4783-8198-1933a648babd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_nonsolid",
    "eventList": [
        {
            "id": "fea19942-2c05-4e12-95b7-22ea78cd91e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f878563b-bc94-4783-8198-1933a648babd"
        },
        {
            "id": "d9921bb2-56dc-4d5f-94ca-fadd6496eac5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f878563b-bc94-4783-8198-1933a648babd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "235aafd2-f367-4ae3-b951-ac2c0c280307",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
    "visible": true
}