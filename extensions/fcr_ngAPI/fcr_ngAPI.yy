{
    "id": "cc76fda8-36fd-4594-aa86-d4ef5e7838a7",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "fcr_ngAPI",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2020-57-13 06:11:17",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "db742327-349c-4a37-81a1-eea294d00d1a",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 32,
            "filename": "ngAPIextension.js",
            "final": "",
            "functions": [
                {
                    "id": "1e63644e-c977-4521-80d6-e3f9370d310f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Initiates the NG API. (arg0=app_id, arg1=encryption key, arg2=host block mode, arg3=console enable\/disable)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngInit",
                    "returnType": 2
                },
                {
                    "id": "b7abd8e8-d3e8-4979-b3a0-ab5e0ba02524",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Returns whether NG is being played by a registered user or not.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngUser",
                    "returnType": 2
                },
                {
                    "id": "3463ba40-e57f-465c-8de9-77be759a42a4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Loads scoreboards and\/or medals. (arg0=boolean to load scores, arg1=medals)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngLoad",
                    "returnType": 2
                },
                {
                    "id": "3f1667b3-0db2-4b0e-8a48-dff160e2b5c3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Unlocks the medal by name (argument0). Returns true if success or false otherwise.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngMedalUnlock",
                    "returnType": 2
                },
                {
                    "id": "1657756a-6081-4026-8abe-85f34fbd8242",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Posts score to board. Returns true if success or false if otherwise. (arg0=board_name, arg1=score)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngScorePost",
                    "returnType": 2
                },
                {
                    "id": "b7c15e89-562a-4325-9bdb-529b0dad7078",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Gets the debug mode.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngGetDebug",
                    "returnType": 2
                },
                {
                    "id": "f7ca056c-fc97-4ec8-aae3-774e550a67e9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Sets the debug mode.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngSetDebug",
                    "returnType": 2
                },
                {
                    "id": "ca26fedc-a039-41c1-9869-b5bbd952bb96",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Gets the currently active user. Returns \"\" if guest.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngUserName",
                    "returnType": 1
                },
                {
                    "id": "8fb4c6b9-02de-4b6f-83b1-709f528f45de",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Returns true if the API is active or false if otherwise.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngAPI",
                    "returnType": 2
                },
                {
                    "id": "561c723a-1c4e-4a03-bd1d-f26e0b07c644",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Returns the current session.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngSession",
                    "returnType": 1
                },
                {
                    "id": "b32c5ad8-e02e-427d-be3b-23d4862566b9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "Returns the last login error.",
                    "hidden": false,
                    "kind": 11,
                    "name": "ngLoginError",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                "1e63644e-c977-4521-80d6-e3f9370d310f",
                "b7abd8e8-d3e8-4979-b3a0-ab5e0ba02524",
                "3463ba40-e57f-465c-8de9-77be759a42a4",
                "3f1667b3-0db2-4b0e-8a48-dff160e2b5c3",
                "1657756a-6081-4026-8abe-85f34fbd8242",
                "b7c15e89-562a-4325-9bdb-529b0dad7078",
                "f7ca056c-fc97-4ec8-aae3-774e550a67e9",
                "ca26fedc-a039-41c1-9869-b5bbd952bb96",
                "8fb4c6b9-02de-4b6f-83b1-709f528f45de",
                "561c723a-1c4e-4a03-bd1d-f26e0b07c644",
                "b32c5ad8-e02e-427d-be3b-23d4862566b9"
            ],
            "origname": "extensions\\ngAPIextension.js",
            "uncompress": false
        },
        {
            "id": "89962573-5f43-46b2-8cc7-48fd108723a1",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "newgroundsio.js",
            "final": "",
            "functions": [
                
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\newgroundsio.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "version": "1.0.3"
}