{
    "id": "9658799c-122c-4d71-bc32-8e4ae25264f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_banjo_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bf42ec0-b9c4-48b1-8609-d5fc19a2ebae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9658799c-122c-4d71-bc32-8e4ae25264f3",
            "compositeImage": {
                "id": "3f9c6e34-c710-4ee5-b1a7-3c22cc4f5cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf42ec0-b9c4-48b1-8609-d5fc19a2ebae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa87c2e-bd3e-4131-bedc-674320bf952b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf42ec0-b9c4-48b1-8609-d5fc19a2ebae",
                    "LayerId": "ae9ecd99-bbd9-451e-9f00-b67a58ff62a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ae9ecd99-bbd9-451e-9f00-b67a58ff62a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9658799c-122c-4d71-bc32-8e4ae25264f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}