{
    "id": "a4a5ddc7-87f9-4ee1-b046-35ef610447d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_colbak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b9cf227-361c-41a4-a8f4-ac3ca10c420b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a5ddc7-87f9-4ee1-b046-35ef610447d2",
            "compositeImage": {
                "id": "82dc69c2-c9a2-475b-8e0c-f6add5a23116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b9cf227-361c-41a4-a8f4-ac3ca10c420b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cca8903-c642-4be2-b6db-e108df3ece40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b9cf227-361c-41a4-a8f4-ac3ca10c420b",
                    "LayerId": "21059af5-9387-45d5-8cf3-df3689e62af5"
                }
            ]
        },
        {
            "id": "ff8ba4e8-d4d9-4e4c-9b1f-8ab3a217fe3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a5ddc7-87f9-4ee1-b046-35ef610447d2",
            "compositeImage": {
                "id": "20a72428-a182-40d8-879e-72e64f0a3e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8ba4e8-d4d9-4e4c-9b1f-8ab3a217fe3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6d1e859-ce91-4acd-8432-add9f18268b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8ba4e8-d4d9-4e4c-9b1f-8ab3a217fe3a",
                    "LayerId": "21059af5-9387-45d5-8cf3-df3689e62af5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "21059af5-9387-45d5-8cf3-df3689e62af5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4a5ddc7-87f9-4ee1-b046-35ef610447d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}