{
    "id": "d78f6c19-d10b-461a-bbf1-45540e764fcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_hothand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72ed85da-7854-4238-b226-0d413334dabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d78f6c19-d10b-461a-bbf1-45540e764fcc",
            "compositeImage": {
                "id": "87611fa3-d62d-4235-a267-f7c8355cbf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ed85da-7854-4238-b226-0d413334dabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d8fa20-0202-4b26-b264-b786f85969dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ed85da-7854-4238-b226-0d413334dabd",
                    "LayerId": "1a7f92d1-e825-4cb2-82e4-dafa7aae11d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1a7f92d1-e825-4cb2-82e4-dafa7aae11d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d78f6c19-d10b-461a-bbf1-45540e764fcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 24
}