{
    "id": "cf563069-92ac-49f9-a19a-c905de8298a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hothand_short",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e357a752-01c4-462c-84f6-7077feb8eac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf563069-92ac-49f9-a19a-c905de8298a0",
            "compositeImage": {
                "id": "2226f4f8-51ec-488d-9626-cf1c3e5e290b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e357a752-01c4-462c-84f6-7077feb8eac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6ce6fa-a35a-4048-be70-c23b881187dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e357a752-01c4-462c-84f6-7077feb8eac1",
                    "LayerId": "665577a0-0c5d-4370-b9d2-9d4041f312f5"
                }
            ]
        },
        {
            "id": "b19b6a29-dadd-4a2a-bc5e-56a02e0848ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf563069-92ac-49f9-a19a-c905de8298a0",
            "compositeImage": {
                "id": "3dc620e1-e544-4417-9676-335d13f2b21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b19b6a29-dadd-4a2a-bc5e-56a02e0848ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adc42481-adfd-4a53-b9a6-0b740149c748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b19b6a29-dadd-4a2a-bc5e-56a02e0848ae",
                    "LayerId": "665577a0-0c5d-4370-b9d2-9d4041f312f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "665577a0-0c5d-4370-b9d2-9d4041f312f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf563069-92ac-49f9-a19a-c905de8298a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}