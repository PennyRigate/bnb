{
    "id": "4edbbf32-cf9b-4462-8373-02a13bba9b53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_testboi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba86fd2d-bc78-4a35-b551-08fd956b1b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4edbbf32-cf9b-4462-8373-02a13bba9b53",
            "compositeImage": {
                "id": "b864b314-0c5b-4af2-a144-5f51d45e05cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba86fd2d-bc78-4a35-b551-08fd956b1b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601f3330-255d-4da6-bb0b-dd7aec130b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba86fd2d-bc78-4a35-b551-08fd956b1b99",
                    "LayerId": "5d1f1cd8-cbd3-4a52-9a70-2f1ed09b102a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5d1f1cd8-cbd3-4a52-9a70-2f1ed09b102a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4edbbf32-cf9b-4462-8373-02a13bba9b53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}