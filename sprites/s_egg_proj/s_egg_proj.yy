{
    "id": "cbecb6df-be24-4d33-94d9-aa6c4901cd00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_egg_proj",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50f1aeb2-0ae8-4fe8-a615-55146cff66e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbecb6df-be24-4d33-94d9-aa6c4901cd00",
            "compositeImage": {
                "id": "9a99d2d0-c1fa-4db2-963b-8857fcc32615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f1aeb2-0ae8-4fe8-a615-55146cff66e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62516b74-747b-4e20-84b2-2cf63b48e3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f1aeb2-0ae8-4fe8-a615-55146cff66e2",
                    "LayerId": "fb1cd89a-b8c8-4156-aa30-0ecbc53cecd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fb1cd89a-b8c8-4156-aa30-0ecbc53cecd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbecb6df-be24-4d33-94d9-aa6c4901cd00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 2,
    "yorig": 1
}