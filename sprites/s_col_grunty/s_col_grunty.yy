{
    "id": "8c74a770-4e4f-4cd2-9489-2fa61e74fb80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_grunty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a884b01-1b29-43f9-a77a-0e22c7d2f28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c74a770-4e4f-4cd2-9489-2fa61e74fb80",
            "compositeImage": {
                "id": "d6ff5837-298c-4809-bafa-82a2688be314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a884b01-1b29-43f9-a77a-0e22c7d2f28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65902b74-94bb-447d-9b2a-968d61d91135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a884b01-1b29-43f9-a77a-0e22c7d2f28d",
                    "LayerId": "7ca37b44-9be9-437a-9511-b3a3aa076016"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7ca37b44-9be9-437a-9511-b3a3aa076016",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c74a770-4e4f-4cd2-9489-2fa61e74fb80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 16
}