{
    "id": "bf033294-a880-4dae-b967-d6d6140f044c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_note",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cf79c5a-374e-48f7-b67a-19f539c4252c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf033294-a880-4dae-b967-d6d6140f044c",
            "compositeImage": {
                "id": "4d89606a-c192-43f9-b74e-0e18b4083272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf79c5a-374e-48f7-b67a-19f539c4252c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eed09be-45fb-4874-bd22-0285c9673658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf79c5a-374e-48f7-b67a-19f539c4252c",
                    "LayerId": "8de02deb-60f7-42f6-86d9-4af0ff02513a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "8de02deb-60f7-42f6-86d9-4af0ff02513a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf033294-a880-4dae-b967-d6d6140f044c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}