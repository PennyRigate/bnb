{
    "id": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_levelbord",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 56,
    "bbox_right": 199,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67cd798b-5c1d-4daf-b871-f35dc7caaf51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "compositeImage": {
                "id": "cdd7828e-96b3-47fc-ba5e-3b0bcf525f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67cd798b-5c1d-4daf-b871-f35dc7caaf51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f31f832-eac8-48f7-a4fb-f06cfc72e520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67cd798b-5c1d-4daf-b871-f35dc7caaf51",
                    "LayerId": "0e797d80-e896-4453-b9d2-f6f09df3e638"
                }
            ]
        },
        {
            "id": "fa05ca00-e7d0-47ee-b85e-3f7c78e57bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "compositeImage": {
                "id": "329a1f54-9411-48b4-8897-d4426f6c9ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa05ca00-e7d0-47ee-b85e-3f7c78e57bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f274a00-0070-4a9f-868b-1b47c832a170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa05ca00-e7d0-47ee-b85e-3f7c78e57bb7",
                    "LayerId": "0e797d80-e896-4453-b9d2-f6f09df3e638"
                }
            ]
        },
        {
            "id": "5a91fa7f-4aaa-458c-8142-07adb6b613fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "compositeImage": {
                "id": "4c887bcd-cd53-45a3-a322-2d60f65fc291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a91fa7f-4aaa-458c-8142-07adb6b613fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f15b3ed-f3d3-4b25-97e3-0c94ae789b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a91fa7f-4aaa-458c-8142-07adb6b613fa",
                    "LayerId": "0e797d80-e896-4453-b9d2-f6f09df3e638"
                }
            ]
        },
        {
            "id": "e1512d15-ab6b-4090-b1d7-841f8912555c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "compositeImage": {
                "id": "0144f4e2-0e41-445b-ba58-8b604f139b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1512d15-ab6b-4090-b1d7-841f8912555c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d275031-d7cf-4be0-8935-8f7e8761c392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1512d15-ab6b-4090-b1d7-841f8912555c",
                    "LayerId": "0e797d80-e896-4453-b9d2-f6f09df3e638"
                }
            ]
        },
        {
            "id": "e7b1a202-114f-4d9f-8b38-bd27728879a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "compositeImage": {
                "id": "615aeb55-ce00-4e83-a37f-f15b5cc87926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b1a202-114f-4d9f-8b38-bd27728879a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3507fe82-c928-4378-a3f1-acfc7ff1ae84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b1a202-114f-4d9f-8b38-bd27728879a5",
                    "LayerId": "0e797d80-e896-4453-b9d2-f6f09df3e638"
                }
            ]
        },
        {
            "id": "8d9f3c93-8462-4935-a964-ad087da49a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "compositeImage": {
                "id": "a090de04-df54-43b6-b837-576a32b1cd0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9f3c93-8462-4935-a964-ad087da49a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "524cefc5-2731-4538-bd9b-70052fa8d02b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9f3c93-8462-4935-a964-ad087da49a1c",
                    "LayerId": "0e797d80-e896-4453-b9d2-f6f09df3e638"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0e797d80-e896-4453-b9d2-f6f09df3e638",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fe94515-f220-48f6-b761-f448dbdcc0aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}