{
    "id": "f56297da-2ab4-49bd-b146-3a5dbdbf7c43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_snippet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0699c35-5f3c-43d2-99d2-31b1c6da3b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f56297da-2ab4-49bd-b146-3a5dbdbf7c43",
            "compositeImage": {
                "id": "93ddeb0d-4d1e-401d-886f-3f9058406c87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0699c35-5f3c-43d2-99d2-31b1c6da3b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "597efc9a-d499-4d53-b876-13c5fac63a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0699c35-5f3c-43d2-99d2-31b1c6da3b64",
                    "LayerId": "195cdfe1-84de-45a6-ace4-443c9396649d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "195cdfe1-84de-45a6-ace4-443c9396649d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f56297da-2ab4-49bd-b146-3a5dbdbf7c43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}