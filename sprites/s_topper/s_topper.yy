{
    "id": "1e0caab4-e00c-4044-9f8e-2d88d4c8f0bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_topper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "569bda3c-7c67-4f10-80bd-39cbf4f62f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e0caab4-e00c-4044-9f8e-2d88d4c8f0bf",
            "compositeImage": {
                "id": "c7e5bc21-ddfd-41ab-af95-c3dab3cf7cc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569bda3c-7c67-4f10-80bd-39cbf4f62f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "891264d2-3163-44ae-b527-c4a63d4c6724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569bda3c-7c67-4f10-80bd-39cbf4f62f5b",
                    "LayerId": "408ae7e9-6697-4a8e-88f8-e4647e5d025e"
                }
            ]
        },
        {
            "id": "82762b5a-22e0-4b81-a458-4205422813b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e0caab4-e00c-4044-9f8e-2d88d4c8f0bf",
            "compositeImage": {
                "id": "86ff8a10-3b4d-4663-b094-6cc6c187898b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82762b5a-22e0-4b81-a458-4205422813b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3ee56f8-4c1a-4c8b-bce0-61c3edd7a5c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82762b5a-22e0-4b81-a458-4205422813b1",
                    "LayerId": "408ae7e9-6697-4a8e-88f8-e4647e5d025e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "408ae7e9-6697-4a8e-88f8-e4647e5d025e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e0caab4-e00c-4044-9f8e-2d88d4c8f0bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2.8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 8
}