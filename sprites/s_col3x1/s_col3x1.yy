{
    "id": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col3x1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c04ae334-8297-4330-8fe7-15efcc6dbb8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "677a3a4a-7dfa-4672-b8ee-2091f8e39b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04ae334-8297-4330-8fe7-15efcc6dbb8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96519dd5-be0d-45f8-865e-fba1cbffcc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04ae334-8297-4330-8fe7-15efcc6dbb8a",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "cc0539d9-7426-4869-9e8d-564a42de3065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "20daa05c-2e8b-43b5-8800-2210ab9e1c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0539d9-7426-4869-9e8d-564a42de3065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62680cb-312e-48de-96ba-a8dec70d96aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0539d9-7426-4869-9e8d-564a42de3065",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "dea17f8a-ff0d-4e85-9afa-976b9e8232ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "cfb91cfe-c23b-4eba-a394-17e97c651f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea17f8a-ff0d-4e85-9afa-976b9e8232ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74f86a4f-1e14-4f8c-a001-6bdd20f08716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea17f8a-ff0d-4e85-9afa-976b9e8232ac",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "d12534b5-eda1-4ebb-a8ea-58e10871b3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "3c1b2048-3e41-48f4-8e44-277d1b86d849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d12534b5-eda1-4ebb-a8ea-58e10871b3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6e494fc-e768-4854-a9ab-1828ea2f539b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12534b5-eda1-4ebb-a8ea-58e10871b3f1",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "929996fc-b5a8-4d36-9ab9-76de152802ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "4a0396f6-90ae-4fa9-8376-9061568748f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929996fc-b5a8-4d36-9ab9-76de152802ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63591b57-7676-463b-ac8d-d7c5b38a91ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929996fc-b5a8-4d36-9ab9-76de152802ca",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "ac22039b-1d81-46bf-b385-6d02a698d697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "12b93cf6-34f0-4b81-9a46-3c70063a8f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac22039b-1d81-46bf-b385-6d02a698d697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261cf178-fbde-41b5-8429-b06ce216b236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac22039b-1d81-46bf-b385-6d02a698d697",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "a2da63db-4d5b-4248-adcf-12e818bda367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "7d583d19-0edf-4ae7-bc82-c44f88329ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2da63db-4d5b-4248-adcf-12e818bda367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "851c06b5-f12b-4833-a390-247d41f2d531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2da63db-4d5b-4248-adcf-12e818bda367",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        },
        {
            "id": "062399da-9de6-4fc9-9bb8-e4a3e9e9d121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "compositeImage": {
                "id": "0421ba2b-d7a6-403f-a09e-a916c5ae2696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062399da-9de6-4fc9-9bb8-e4a3e9e9d121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6c6e54-f6ae-4ae9-ab57-77301b2f77ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062399da-9de6-4fc9-9bb8-e4a3e9e9d121",
                    "LayerId": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "7a0a9ce2-0f3e-4a87-8147-60a0d4a404db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44c61fad-8be3-4ccc-a3c0-ad41b2f1981b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 0
}