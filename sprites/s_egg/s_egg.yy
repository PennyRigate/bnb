{
    "id": "b44e3532-1dad-4ceb-8653-0607eb068b3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_egg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14854f50-6da5-43b9-b137-a6f174b8b67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b44e3532-1dad-4ceb-8653-0607eb068b3f",
            "compositeImage": {
                "id": "0877288c-4b9b-4995-bd49-1a923ce7feba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14854f50-6da5-43b9-b137-a6f174b8b67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "081cc344-2329-4082-9046-95a3590a8725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14854f50-6da5-43b9-b137-a6f174b8b67e",
                    "LayerId": "c0ffa0cb-0bcb-4dc2-b6dd-c112e515cee6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c0ffa0cb-0bcb-4dc2-b6dd-c112e515cee6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b44e3532-1dad-4ceb-8653-0607eb068b3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}