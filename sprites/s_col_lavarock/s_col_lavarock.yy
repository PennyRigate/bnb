{
    "id": "22dcc40e-2496-457f-8a89-77ea0acaa8d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_lavarock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99de3596-f9d7-4760-a7af-01cd5d006352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22dcc40e-2496-457f-8a89-77ea0acaa8d0",
            "compositeImage": {
                "id": "58d054c3-1960-45a5-a101-37fabc1ee092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99de3596-f9d7-4760-a7af-01cd5d006352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3dbe6d3-e5ae-4e01-94ef-8a774d0f6a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99de3596-f9d7-4760-a7af-01cd5d006352",
                    "LayerId": "7c98683f-ad82-4e3d-b495-dc4af9794cf8"
                }
            ]
        },
        {
            "id": "33ae3c7b-1872-489a-be6d-e0628ee2f2cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22dcc40e-2496-457f-8a89-77ea0acaa8d0",
            "compositeImage": {
                "id": "83799c42-7cd1-4759-a196-9cf6caa12fbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ae3c7b-1872-489a-be6d-e0628ee2f2cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d21f0548-3aa4-4f81-af0b-bf16ae7c1782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ae3c7b-1872-489a-be6d-e0628ee2f2cd",
                    "LayerId": "7c98683f-ad82-4e3d-b495-dc4af9794cf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7c98683f-ad82-4e3d-b495-dc4af9794cf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22dcc40e-2496-457f-8a89-77ea0acaa8d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}