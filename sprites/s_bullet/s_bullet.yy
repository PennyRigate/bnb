{
    "id": "a6c10e7c-98ee-4dee-bbda-ce215cfcb46a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fb5b915-6c15-417b-8232-05b58e5c9b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c10e7c-98ee-4dee-bbda-ce215cfcb46a",
            "compositeImage": {
                "id": "203ef56e-0a29-46eb-b962-278027d1794f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb5b915-6c15-417b-8232-05b58e5c9b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad5b43b-9a48-4e15-beb7-8e499ee0d76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb5b915-6c15-417b-8232-05b58e5c9b25",
                    "LayerId": "bc7bf356-7cf5-4380-9fe0-fba35a873679"
                }
            ]
        },
        {
            "id": "2b5e679d-00f4-4592-b78f-d4483c6b009b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c10e7c-98ee-4dee-bbda-ce215cfcb46a",
            "compositeImage": {
                "id": "1f675ee1-044e-4369-9dc2-6825e998bcd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5e679d-00f4-4592-b78f-d4483c6b009b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73be6db2-259a-44cc-b265-f37ccbfe3905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5e679d-00f4-4592-b78f-d4483c6b009b",
                    "LayerId": "bc7bf356-7cf5-4380-9fe0-fba35a873679"
                }
            ]
        },
        {
            "id": "74d9360a-d67f-4613-908a-f85bfa2ec854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c10e7c-98ee-4dee-bbda-ce215cfcb46a",
            "compositeImage": {
                "id": "5581dd1e-bee1-4410-9a49-705868d460fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d9360a-d67f-4613-908a-f85bfa2ec854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57932bd8-0ffc-4bb4-a9ca-7289256240ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d9360a-d67f-4613-908a-f85bfa2ec854",
                    "LayerId": "bc7bf356-7cf5-4380-9fe0-fba35a873679"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bc7bf356-7cf5-4380-9fe0-fba35a873679",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6c10e7c-98ee-4dee-bbda-ce215cfcb46a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}