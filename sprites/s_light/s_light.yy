{
    "id": "f08c605c-3006-47da-b801-1a325a048c06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fe79410-b96a-4d71-a702-4b60ebd83709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f08c605c-3006-47da-b801-1a325a048c06",
            "compositeImage": {
                "id": "cfbf3de5-165b-4161-ab2a-c2a07148464c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe79410-b96a-4d71-a702-4b60ebd83709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69db3fe-aa34-4b77-a983-a9642be031eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe79410-b96a-4d71-a702-4b60ebd83709",
                    "LayerId": "647bbab1-090b-4a14-9714-6a2348217e8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "647bbab1-090b-4a14-9714-6a2348217e8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f08c605c-3006-47da-b801-1a325a048c06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}