{
    "id": "3b85958f-bb6c-4ff6-9c84-f19f16af71cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_cube",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "330ce917-fb8b-4219-9c43-1846e110b255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b85958f-bb6c-4ff6-9c84-f19f16af71cc",
            "compositeImage": {
                "id": "b110ac02-4715-4f2c-ae9d-89267de4d076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330ce917-fb8b-4219-9c43-1846e110b255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6a67d51-5b72-4eca-880d-05d61daecd79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330ce917-fb8b-4219-9c43-1846e110b255",
                    "LayerId": "8eb57d36-9d41-4826-90bb-69d3ce1544e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "8eb57d36-9d41-4826-90bb-69d3ce1544e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b85958f-bb6c-4ff6-9c84-f19f16af71cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 24
}