{
    "id": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_drop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63e21529-eefa-41af-a505-3b34cfc3555b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "compositeImage": {
                "id": "f0e3c8ab-b550-41bc-9c23-5ab64cb09359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63e21529-eefa-41af-a505-3b34cfc3555b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd5e843-3393-42d0-9ba9-c2340adb31b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63e21529-eefa-41af-a505-3b34cfc3555b",
                    "LayerId": "e3a19473-099f-448f-905a-97dab8fe05ac"
                }
            ]
        },
        {
            "id": "5f5c6900-c5bc-43d2-9047-7f577fd1c6a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "compositeImage": {
                "id": "05ade9fd-04ea-43f3-b58a-bbd944181012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f5c6900-c5bc-43d2-9047-7f577fd1c6a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "764ff43e-ab7a-44c5-8451-c6afdaf62909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f5c6900-c5bc-43d2-9047-7f577fd1c6a7",
                    "LayerId": "e3a19473-099f-448f-905a-97dab8fe05ac"
                }
            ]
        },
        {
            "id": "415538f9-28db-41d6-8115-2109763beabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "compositeImage": {
                "id": "d5550d11-c520-4d69-8391-6cc36665fce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "415538f9-28db-41d6-8115-2109763beabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc24580-81d2-4ccf-bf15-145ee3685bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "415538f9-28db-41d6-8115-2109763beabd",
                    "LayerId": "e3a19473-099f-448f-905a-97dab8fe05ac"
                }
            ]
        },
        {
            "id": "e17b0703-aa5f-468e-9d88-9eab44bf6f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "compositeImage": {
                "id": "0c4bdcab-a063-49e1-b950-07f27774ca14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e17b0703-aa5f-468e-9d88-9eab44bf6f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83f65552-6403-4f35-9a32-47801c95ba58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e17b0703-aa5f-468e-9d88-9eab44bf6f25",
                    "LayerId": "e3a19473-099f-448f-905a-97dab8fe05ac"
                }
            ]
        },
        {
            "id": "393805d5-5e0d-4e3a-b9cb-4f64d108d7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "compositeImage": {
                "id": "a05b713a-8928-4201-9c0a-d34bae05c7ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393805d5-5e0d-4e3a-b9cb-4f64d108d7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c7aa6c-97ed-41bd-a623-b57b99336b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393805d5-5e0d-4e3a-b9cb-4f64d108d7e6",
                    "LayerId": "e3a19473-099f-448f-905a-97dab8fe05ac"
                }
            ]
        },
        {
            "id": "ad884733-b442-4024-af3c-43ab481b4a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "compositeImage": {
                "id": "31ae3e17-7d1c-4d01-843a-d8df05ee79c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad884733-b442-4024-af3c-43ab481b4a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498ab1fb-a45f-44cc-b49a-6ea01c4dc2a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad884733-b442-4024-af3c-43ab481b4a3b",
                    "LayerId": "e3a19473-099f-448f-905a-97dab8fe05ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e3a19473-099f-448f-905a-97dab8fe05ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b122a81-6fc1-4b9e-a8d5-4c9adab686c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}