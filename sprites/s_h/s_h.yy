{
    "id": "7747a132-b0bb-4f16-94c4-311d8bacd198",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 2,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc7a48be-9981-4f6f-b7d7-f1c75d26a4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7747a132-b0bb-4f16-94c4-311d8bacd198",
            "compositeImage": {
                "id": "962dd766-c253-4ace-adf8-20f65c841c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc7a48be-9981-4f6f-b7d7-f1c75d26a4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c60a4b5-639a-479e-ba57-4ac0bae5feff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc7a48be-9981-4f6f-b7d7-f1c75d26a4d3",
                    "LayerId": "ac127589-4561-4f87-880e-55b101f70610"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ac127589-4561-4f87-880e-55b101f70610",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7747a132-b0bb-4f16-94c4-311d8bacd198",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}