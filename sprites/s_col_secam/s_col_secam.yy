{
    "id": "a70fa292-aa10-4f2c-bd87-9c3229d5e6d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_secam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e0e2fab-97db-49a4-afab-0b019574d936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70fa292-aa10-4f2c-bd87-9c3229d5e6d9",
            "compositeImage": {
                "id": "50f58969-a719-452d-b0e5-f04538437e63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0e2fab-97db-49a4-afab-0b019574d936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45e5a47-94c1-40a8-9e37-7bd82a7e8282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0e2fab-97db-49a4-afab-0b019574d936",
                    "LayerId": "79458e68-5562-4883-8106-4c5021d4ffbe"
                }
            ]
        },
        {
            "id": "96fb3120-7a59-49bf-b958-f831de6bbd09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70fa292-aa10-4f2c-bd87-9c3229d5e6d9",
            "compositeImage": {
                "id": "7d19f52c-815a-4ec5-bd04-aaf815b372c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96fb3120-7a59-49bf-b958-f831de6bbd09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0abafd3c-131d-4b88-a1ad-f46ad5ccc787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96fb3120-7a59-49bf-b958-f831de6bbd09",
                    "LayerId": "79458e68-5562-4883-8106-4c5021d4ffbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79458e68-5562-4883-8106-4c5021d4ffbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a70fa292-aa10-4f2c-bd87-9c3229d5e6d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 8
}