{
    "id": "5d18cf71-c528-4be1-b17c-16747fd945e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite93",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca3ff93d-3ce7-4e14-8441-f3ec6d6aa90d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d18cf71-c528-4be1-b17c-16747fd945e0",
            "compositeImage": {
                "id": "18525249-d5a1-4328-8769-f5f32c7536b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca3ff93d-3ce7-4e14-8441-f3ec6d6aa90d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef5ecbe-f994-4c2a-b3ca-f5e97d25b8e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca3ff93d-3ce7-4e14-8441-f3ec6d6aa90d",
                    "LayerId": "c37932b7-8367-4a31-9c08-f1729ea3a9d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c37932b7-8367-4a31-9c08-f1729ea3a9d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d18cf71-c528-4be1-b17c-16747fd945e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}