{
    "id": "fa1fc8fe-ea25-458f-8d63-bdffb6994973",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07522c92-fbe4-4e6e-92ed-b1583b24c9d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1fc8fe-ea25-458f-8d63-bdffb6994973",
            "compositeImage": {
                "id": "75e7cbdf-db2c-4c99-94e0-f9f45a29a2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07522c92-fbe4-4e6e-92ed-b1583b24c9d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f481a3-fd91-4e85-b1ad-b0b90425e612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07522c92-fbe4-4e6e-92ed-b1583b24c9d1",
                    "LayerId": "4a760e03-e86b-49b8-b875-f9b012f19482"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4a760e03-e86b-49b8-b875-f9b012f19482",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa1fc8fe-ea25-458f-8d63-bdffb6994973",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}