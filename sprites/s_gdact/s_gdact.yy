{
    "id": "e4b17f2e-2372-4331-8a87-9cfc92450c81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gdact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0badc69-f8bd-4cf9-8232-73082051bfb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b17f2e-2372-4331-8a87-9cfc92450c81",
            "compositeImage": {
                "id": "5baa0dd2-c0dd-407a-82d8-8650f317ddb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0badc69-f8bd-4cf9-8232-73082051bfb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d91f48-f84a-47d2-a056-c63ecd48079a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0badc69-f8bd-4cf9-8232-73082051bfb8",
                    "LayerId": "f7ea33da-2838-4aa2-a929-e8afd87175d1"
                }
            ]
        },
        {
            "id": "c6c8a3e8-fc84-4ddb-8950-57c736259571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b17f2e-2372-4331-8a87-9cfc92450c81",
            "compositeImage": {
                "id": "ee22599c-0e38-4201-a3ff-f4d98e187226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c8a3e8-fc84-4ddb-8950-57c736259571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1e2827-ff91-49e2-b72b-8df1fa7c624a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c8a3e8-fc84-4ddb-8950-57c736259571",
                    "LayerId": "f7ea33da-2838-4aa2-a929-e8afd87175d1"
                }
            ]
        },
        {
            "id": "0ff50164-ad36-4bb6-87e2-9fd1e65358cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b17f2e-2372-4331-8a87-9cfc92450c81",
            "compositeImage": {
                "id": "14839fee-82ac-480b-999b-46246f02b955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff50164-ad36-4bb6-87e2-9fd1e65358cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3b1923-5373-4c2a-9298-43b6c6e36eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff50164-ad36-4bb6-87e2-9fd1e65358cc",
                    "LayerId": "f7ea33da-2838-4aa2-a929-e8afd87175d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f7ea33da-2838-4aa2-a929-e8afd87175d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4b17f2e-2372-4331-8a87-9cfc92450c81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 4
}