{
    "id": "af767dbd-4181-4ad8-8aad-defe4d402e56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lavarock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60f5d829-377c-4a13-ac6b-a7273ebac7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af767dbd-4181-4ad8-8aad-defe4d402e56",
            "compositeImage": {
                "id": "0c89c038-e7bb-4872-8fc0-1e8ed9a7d7bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60f5d829-377c-4a13-ac6b-a7273ebac7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54db2788-bd7e-4f68-bef5-c8f7a34affb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60f5d829-377c-4a13-ac6b-a7273ebac7e8",
                    "LayerId": "4b8c5ad8-2682-4bed-9d21-5a27096f2339"
                }
            ]
        },
        {
            "id": "bc33f3f3-7b2b-4e1a-9503-d8c0ed4fb6f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af767dbd-4181-4ad8-8aad-defe4d402e56",
            "compositeImage": {
                "id": "45188913-1246-4ee1-b6eb-f9df7f6a0644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc33f3f3-7b2b-4e1a-9503-d8c0ed4fb6f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc495690-7536-484c-b32f-488f0502972e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc33f3f3-7b2b-4e1a-9503-d8c0ed4fb6f0",
                    "LayerId": "4b8c5ad8-2682-4bed-9d21-5a27096f2339"
                }
            ]
        },
        {
            "id": "b957aa43-8db4-4b39-a15f-a3fae4ebdddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af767dbd-4181-4ad8-8aad-defe4d402e56",
            "compositeImage": {
                "id": "a8845f53-1264-4e52-8251-0fe38bd55472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b957aa43-8db4-4b39-a15f-a3fae4ebdddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04feb3f3-16b6-47fd-becc-7a6e6aaccb70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b957aa43-8db4-4b39-a15f-a3fae4ebdddd",
                    "LayerId": "4b8c5ad8-2682-4bed-9d21-5a27096f2339"
                }
            ]
        },
        {
            "id": "bcf556dc-39bf-4b7e-91cd-0318264db977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af767dbd-4181-4ad8-8aad-defe4d402e56",
            "compositeImage": {
                "id": "20e62a16-8a4f-4ea2-9f0c-b828ba8cc330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf556dc-39bf-4b7e-91cd-0318264db977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c6f154-ddfc-4d35-960c-7eacd662f9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf556dc-39bf-4b7e-91cd-0318264db977",
                    "LayerId": "4b8c5ad8-2682-4bed-9d21-5a27096f2339"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4b8c5ad8-2682-4bed-9d21-5a27096f2339",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af767dbd-4181-4ad8-8aad-defe4d402e56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}