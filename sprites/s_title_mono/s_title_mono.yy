{
    "id": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_title_mono",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acabd181-e7f1-4981-bab1-03e11f9d6f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "7e369438-78ab-4eaa-954f-2eb17b1bc6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acabd181-e7f1-4981-bab1-03e11f9d6f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "795c7d62-68d7-4d0f-bd68-7e878dfc4d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acabd181-e7f1-4981-bab1-03e11f9d6f79",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "7b4b64b6-c9ea-4ed2-9de1-8b5514142c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "ed2d072e-df70-4cc0-a137-2966b27046ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b4b64b6-c9ea-4ed2-9de1-8b5514142c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70385066-7b49-474f-bfd9-30ccdadfbdce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b4b64b6-c9ea-4ed2-9de1-8b5514142c56",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "3701b69f-704c-4655-a147-2884006e3388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "9a23b791-ca8f-4574-8c73-059ffc6fe2ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3701b69f-704c-4655-a147-2884006e3388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db41e931-4a05-4628-8a6c-7ee6e19cbfb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3701b69f-704c-4655-a147-2884006e3388",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "a4001d76-1202-4d7e-944e-36ed1179e4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "44ec4e31-e690-4f5e-9ac3-4c26f95f2229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4001d76-1202-4d7e-944e-36ed1179e4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd32d99-8ef1-4b62-936f-8ccb2e865dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4001d76-1202-4d7e-944e-36ed1179e4b0",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "4b95fbe5-4c91-48d9-83ce-36b7e602e783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "edd99bc0-f50c-4e2f-8d3c-9c44413e4804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b95fbe5-4c91-48d9-83ce-36b7e602e783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b606c8-1e27-4dae-a112-20ce5ebe5cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b95fbe5-4c91-48d9-83ce-36b7e602e783",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "97b543b9-deee-4a24-9f1a-8a209ca7385e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "96690def-c92c-4e5e-b065-48aed76b1e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b543b9-deee-4a24-9f1a-8a209ca7385e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea49bd6-f43f-4f25-ae59-bc581057ae89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b543b9-deee-4a24-9f1a-8a209ca7385e",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "76995277-f0b5-44a6-89f6-895c6b6f2146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "65410127-83b9-4666-a5cc-357435924844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76995277-f0b5-44a6-89f6-895c6b6f2146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25d9c97-16ab-4f76-9408-2729840062b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76995277-f0b5-44a6-89f6-895c6b6f2146",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        },
        {
            "id": "f448867c-3969-4292-a83b-e2cdb5e684b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "compositeImage": {
                "id": "864e43da-248a-41c2-a5bf-754505299326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f448867c-3969-4292-a83b-e2cdb5e684b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0f413f-c4fd-47f2-adac-a601b956cb72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f448867c-3969-4292-a83b-e2cdb5e684b6",
                    "LayerId": "28523f64-e1fc-458b-8fef-dc87d1517f52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "28523f64-e1fc-458b-8fef-dc87d1517f52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e26c29f0-0a5a-4b7f-9fb5-47ea2b3df6ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}