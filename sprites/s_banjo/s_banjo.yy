{
    "id": "2aa5491b-2abe-4c68-92a1-c6481b6a5033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_banjo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3d41081-6af4-4c37-a7cc-70c600cb6fe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2aa5491b-2abe-4c68-92a1-c6481b6a5033",
            "compositeImage": {
                "id": "a3983bbf-9cfe-4c0f-a006-2dc60feb857f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d41081-6af4-4c37-a7cc-70c600cb6fe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "315cebc8-2cc8-4fe2-addb-0d215a1037c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d41081-6af4-4c37-a7cc-70c600cb6fe7",
                    "LayerId": "13f71bb5-913b-4363-99b3-093305e22cb1"
                }
            ]
        },
        {
            "id": "02368837-8e21-4914-b316-54ccc9ef2554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2aa5491b-2abe-4c68-92a1-c6481b6a5033",
            "compositeImage": {
                "id": "15fbead0-cec2-4422-88c3-7f26e40becc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02368837-8e21-4914-b316-54ccc9ef2554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36ea89da-c139-4d8e-bb58-e27a0a5a6bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02368837-8e21-4914-b316-54ccc9ef2554",
                    "LayerId": "13f71bb5-913b-4363-99b3-093305e22cb1"
                }
            ]
        },
        {
            "id": "fb1ba619-cbf6-45ff-a301-901f19e41d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2aa5491b-2abe-4c68-92a1-c6481b6a5033",
            "compositeImage": {
                "id": "92f881bd-1209-4aae-9ec9-1e9456f81f7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb1ba619-cbf6-45ff-a301-901f19e41d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51b550c5-9cb9-42ca-95c8-2a23822f6c84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb1ba619-cbf6-45ff-a301-901f19e41d27",
                    "LayerId": "13f71bb5-913b-4363-99b3-093305e22cb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13f71bb5-913b-4363-99b3-093305e22cb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2aa5491b-2abe-4c68-92a1-c6481b6a5033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}