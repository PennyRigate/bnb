{
    "id": "b83959e7-c8cf-4716-9555-cafd013764ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gruntproj",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a80068da-082b-416b-a07e-fc38ef11b59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83959e7-c8cf-4716-9555-cafd013764ac",
            "compositeImage": {
                "id": "63c907a3-bdb7-4221-bbc6-532219b0f5f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80068da-082b-416b-a07e-fc38ef11b59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b118e349-83d5-46f2-8601-d4612108d96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80068da-082b-416b-a07e-fc38ef11b59b",
                    "LayerId": "11e23605-1dce-48cf-bf43-f3c13b6f4452"
                }
            ]
        },
        {
            "id": "87c692e2-bd72-49ce-8f54-3aa456f10724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83959e7-c8cf-4716-9555-cafd013764ac",
            "compositeImage": {
                "id": "99d9aaba-5191-42fe-92c6-e995b63e3c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c692e2-bd72-49ce-8f54-3aa456f10724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f6ba40-7f45-442d-aa3c-1c920af2e919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c692e2-bd72-49ce-8f54-3aa456f10724",
                    "LayerId": "11e23605-1dce-48cf-bf43-f3c13b6f4452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "11e23605-1dce-48cf-bf43-f3c13b6f4452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b83959e7-c8cf-4716-9555-cafd013764ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}