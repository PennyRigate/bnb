{
    "id": "ddaadbd4-2186-4d79-8912-8628a2021b4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_loading_mono",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03b38591-9278-49e5-8cf1-50fdf1d5006f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddaadbd4-2186-4d79-8912-8628a2021b4d",
            "compositeImage": {
                "id": "bf396c46-a025-48b8-a622-d7cae50be3fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b38591-9278-49e5-8cf1-50fdf1d5006f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3aaeb9-c458-4149-a045-4afc35d0112a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b38591-9278-49e5-8cf1-50fdf1d5006f",
                    "LayerId": "7ae14aaf-8da6-481f-a9f7-65792a9c27a3"
                }
            ]
        },
        {
            "id": "a4be7a5b-fdbd-4958-b509-12ae0057793e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddaadbd4-2186-4d79-8912-8628a2021b4d",
            "compositeImage": {
                "id": "9786544a-7ba3-4d59-a651-16a6415c00fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4be7a5b-fdbd-4958-b509-12ae0057793e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "481344c2-f5f1-4245-a138-799d227b9bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4be7a5b-fdbd-4958-b509-12ae0057793e",
                    "LayerId": "7ae14aaf-8da6-481f-a9f7-65792a9c27a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "7ae14aaf-8da6-481f-a9f7-65792a9c27a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddaadbd4-2186-4d79-8912-8628a2021b4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}