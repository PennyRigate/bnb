{
    "id": "6f34602d-8eac-4fd0-a73a-7b2945ff6a7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "h_corner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 3,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4c93451-dc54-4f3e-a999-941331d9a3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f34602d-8eac-4fd0-a73a-7b2945ff6a7a",
            "compositeImage": {
                "id": "56f2dac5-621b-4823-a771-c7af39a60c85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c93451-dc54-4f3e-a999-941331d9a3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5439f801-5add-4edb-80ae-436d7bf9b662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c93451-dc54-4f3e-a999-941331d9a3f2",
                    "LayerId": "f6030045-13a4-463f-9488-bcbc8110e065"
                }
            ]
        },
        {
            "id": "a0964efe-5ebd-4ace-97b8-1ea33197e46d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f34602d-8eac-4fd0-a73a-7b2945ff6a7a",
            "compositeImage": {
                "id": "7dbc2f33-edae-40e8-a8ae-d4b119e53061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0964efe-5ebd-4ace-97b8-1ea33197e46d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3517b6bb-3612-4b63-839f-a3b7d8ca6531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0964efe-5ebd-4ace-97b8-1ea33197e46d",
                    "LayerId": "f6030045-13a4-463f-9488-bcbc8110e065"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f6030045-13a4-463f-9488-bcbc8110e065",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f34602d-8eac-4fd0-a73a-7b2945ff6a7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}