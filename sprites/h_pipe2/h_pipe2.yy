{
    "id": "b2ccdd07-8de8-4a30-82bd-d1f69258b751",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "h_pipe2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7ed802e-7eb8-4398-9d21-0fe9bd76629b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2ccdd07-8de8-4a30-82bd-d1f69258b751",
            "compositeImage": {
                "id": "41d76e37-859d-4bb0-b971-3074ad9434c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ed802e-7eb8-4398-9d21-0fe9bd76629b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c4cc95a-42e8-49f4-bb31-5a4891829950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ed802e-7eb8-4398-9d21-0fe9bd76629b",
                    "LayerId": "561bf515-4a4f-490b-9be1-10982f7608fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "561bf515-4a4f-490b-9be1-10982f7608fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2ccdd07-8de8-4a30-82bd-d1f69258b751",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}