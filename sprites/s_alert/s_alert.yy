{
    "id": "0e2785f0-33e3-48c3-a598-158a77149d0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_alert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 4,
    "bbox_right": 4,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bd407b1-44f2-4e73-b5dd-2cdad42363cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2785f0-33e3-48c3-a598-158a77149d0a",
            "compositeImage": {
                "id": "400ce099-8bd8-49a4-b9ae-eab40a2223cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd407b1-44f2-4e73-b5dd-2cdad42363cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f97a3e-f76d-425c-93cb-4b7e3044b664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd407b1-44f2-4e73-b5dd-2cdad42363cd",
                    "LayerId": "b553a359-c465-41d8-a0a3-895116f3a400"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b553a359-c465-41d8-a0a3-895116f3a400",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e2785f0-33e3-48c3-a598-158a77149d0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}