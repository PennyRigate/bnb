{
    "id": "a7b42cda-81f7-41aa-8b0d-a5d1a20378bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ewall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f255a8b-0a8d-4855-b4de-67b62ceb59d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b42cda-81f7-41aa-8b0d-a5d1a20378bd",
            "compositeImage": {
                "id": "315cd38b-5cba-421a-9adc-62392a551767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f255a8b-0a8d-4855-b4de-67b62ceb59d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7a4e07-f684-4193-b5fc-d594e0672037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f255a8b-0a8d-4855-b4de-67b62ceb59d2",
                    "LayerId": "b46a2881-1d14-4818-8483-a0194ec08644"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b46a2881-1d14-4818-8483-a0194ec08644",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7b42cda-81f7-41aa-8b0d-a5d1a20378bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}