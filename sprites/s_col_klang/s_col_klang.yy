{
    "id": "632e04a1-6ee9-4bde-b6d3-a5b2ddee366d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_klang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "272f6242-c002-4c05-98bb-5e632da6d792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "632e04a1-6ee9-4bde-b6d3-a5b2ddee366d",
            "compositeImage": {
                "id": "da925085-a939-4e02-a05f-06046a7fb196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "272f6242-c002-4c05-98bb-5e632da6d792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f2986d-6ade-4236-bfb1-ed452877b9d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "272f6242-c002-4c05-98bb-5e632da6d792",
                    "LayerId": "4bc24483-e228-4cdb-8c2a-1ea96ec01d9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4bc24483-e228-4cdb-8c2a-1ea96ec01d9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "632e04a1-6ee9-4bde-b6d3-a5b2ddee366d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 0
}