{
    "id": "67fdf8aa-8586-415e-bdd0-6fc0d750bce9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "h_8x8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87d9ee76-5972-41db-a238-295dc91c1f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67fdf8aa-8586-415e-bdd0-6fc0d750bce9",
            "compositeImage": {
                "id": "89630a11-11e0-4d4e-a73b-63cba37d5f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d9ee76-5972-41db-a238-295dc91c1f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c6cd38-5a37-4a78-92e1-e3f88c13ba42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d9ee76-5972-41db-a238-295dc91c1f3e",
                    "LayerId": "03e833ec-ae0b-41e2-aca1-e43674eb888c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "03e833ec-ae0b-41e2-aca1-e43674eb888c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67fdf8aa-8586-415e-bdd0-6fc0d750bce9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}