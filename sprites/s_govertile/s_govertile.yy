{
    "id": "f1459038-39fd-474a-b4e8-c22dce01f859",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_govertile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74e9dad5-16f8-4755-bfd9-98424f795a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1459038-39fd-474a-b4e8-c22dce01f859",
            "compositeImage": {
                "id": "2886aeb9-3457-4eb6-bcbc-82e86d0c139e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e9dad5-16f8-4755-bfd9-98424f795a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b85d6ce7-fe44-475a-881c-6b032fbaf116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e9dad5-16f8-4755-bfd9-98424f795a61",
                    "LayerId": "040ad462-2a05-4915-9334-d4b512f6e0e7"
                }
            ]
        },
        {
            "id": "ebedf4e7-a66c-4d19-81de-8b0dd95d23b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1459038-39fd-474a-b4e8-c22dce01f859",
            "compositeImage": {
                "id": "4e48eded-e545-4194-9cf0-5fd04f3d7d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebedf4e7-a66c-4d19-81de-8b0dd95d23b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "704944d1-cdd9-4ca8-9d1a-8ad47620ebfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebedf4e7-a66c-4d19-81de-8b0dd95d23b3",
                    "LayerId": "040ad462-2a05-4915-9334-d4b512f6e0e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "040ad462-2a05-4915-9334-d4b512f6e0e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1459038-39fd-474a-b4e8-c22dce01f859",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}