{
    "id": "9e49e2a5-caf4-4fe2-bb19-89f5bf4157e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hothand_short1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1465b248-3a93-4f25-bfe8-14bf01e1b531",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e49e2a5-caf4-4fe2-bb19-89f5bf4157e8",
            "compositeImage": {
                "id": "9b2b86a3-6a7e-43d9-b754-427cf0458ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1465b248-3a93-4f25-bfe8-14bf01e1b531",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "684dea45-008b-448f-ad9d-b0ab945d4240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1465b248-3a93-4f25-bfe8-14bf01e1b531",
                    "LayerId": "af0f840b-8494-4b9e-9e0a-b329f89a3ea4"
                }
            ]
        },
        {
            "id": "3478e657-8c68-456a-8daf-4cfea48f044c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e49e2a5-caf4-4fe2-bb19-89f5bf4157e8",
            "compositeImage": {
                "id": "5e3b8c03-8e8c-4d54-9b5e-f8ba40cd7f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3478e657-8c68-456a-8daf-4cfea48f044c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45140215-d498-4f6e-9b86-a121ad45e64e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3478e657-8c68-456a-8daf-4cfea48f044c",
                    "LayerId": "af0f840b-8494-4b9e-9e0a-b329f89a3ea4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "af0f840b-8494-4b9e-9e0a-b329f89a3ea4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e49e2a5-caf4-4fe2-bb19-89f5bf4157e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}