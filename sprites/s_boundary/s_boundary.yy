{
    "id": "7c3a2be5-7332-4cfd-92b5-b8570d32fa42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boundary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94b143fd-d661-4c2a-99e7-48410149c357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c3a2be5-7332-4cfd-92b5-b8570d32fa42",
            "compositeImage": {
                "id": "106881ba-859c-4e45-a0f0-6ceeddeb2807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b143fd-d661-4c2a-99e7-48410149c357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b20936-0d0e-48d0-9bf2-bac3b31213a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b143fd-d661-4c2a-99e7-48410149c357",
                    "LayerId": "9e3229a0-4c10-411a-a583-ecd7c7c4841e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "9e3229a0-4c10-411a-a583-ecd7c7c4841e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c3a2be5-7332-4cfd-92b5-b8570d32fa42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}