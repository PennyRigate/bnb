{
    "id": "8f068285-c9b4-4cff-abb1-1b3a18d0bb79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "m_banjo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "405fc1c9-0cca-4fca-82af-7287fb172f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f068285-c9b4-4cff-abb1-1b3a18d0bb79",
            "compositeImage": {
                "id": "8bcf992e-75a7-45aa-a9db-cb720680ffda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "405fc1c9-0cca-4fca-82af-7287fb172f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548af4e2-7a6b-485f-9e76-b174855eb0df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "405fc1c9-0cca-4fca-82af-7287fb172f7e",
                    "LayerId": "fa99395f-a30b-4fa3-bea0-c0dbb4505b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fa99395f-a30b-4fa3-bea0-c0dbb4505b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f068285-c9b4-4cff-abb1-1b3a18d0bb79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}