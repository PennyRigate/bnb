{
    "id": "6bee7686-e56c-48b1-b348-f4d6e4d2293c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4293024-a26b-4a83-829b-14ce90dd820c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bee7686-e56c-48b1-b348-f4d6e4d2293c",
            "compositeImage": {
                "id": "fd890dc1-24ac-4aa8-9893-c8aa0fba980f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4293024-a26b-4a83-829b-14ce90dd820c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62a7402-92ed-4e6b-96d2-f82e2eee8b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4293024-a26b-4a83-829b-14ce90dd820c",
                    "LayerId": "bfe0d137-f12a-4ae5-8f66-52c67abf000d"
                }
            ]
        },
        {
            "id": "d4484ed4-8e5e-43ac-8fbf-a9afb21e12e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bee7686-e56c-48b1-b348-f4d6e4d2293c",
            "compositeImage": {
                "id": "ba8c18fe-d0ab-4a26-ad4f-6b6fa3a2da78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4484ed4-8e5e-43ac-8fbf-a9afb21e12e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca59cc6f-e4f9-4725-b69b-8669d99eb78d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4484ed4-8e5e-43ac-8fbf-a9afb21e12e5",
                    "LayerId": "bfe0d137-f12a-4ae5-8f66-52c67abf000d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bfe0d137-f12a-4ae5-8f66-52c67abf000d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bee7686-e56c-48b1-b348-f4d6e4d2293c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}