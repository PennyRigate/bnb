{
    "id": "0f85258b-1327-4b95-a8cc-f7f2b31ce57e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_yumyum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a10642e2-6e4e-4e4a-8d97-007c7292a8df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f85258b-1327-4b95-a8cc-f7f2b31ce57e",
            "compositeImage": {
                "id": "a307f056-da74-42d4-88bb-d4932b511e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a10642e2-6e4e-4e4a-8d97-007c7292a8df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f9f74bc-1229-4ce3-aceb-37ccfc5d2616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a10642e2-6e4e-4e4a-8d97-007c7292a8df",
                    "LayerId": "4d169207-7a10-4786-9b22-eb69fda754c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4d169207-7a10-4786-9b22-eb69fda754c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f85258b-1327-4b95-a8cc-f7f2b31ce57e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 24
}