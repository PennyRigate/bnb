{
    "id": "c7cfb8e0-8c8a-4103-8db4-6096497f9d6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_snacker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc5b6024-13de-42e0-9e33-69c40d4bdd3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7cfb8e0-8c8a-4103-8db4-6096497f9d6e",
            "compositeImage": {
                "id": "47c98baf-ecdc-4a39-b772-241716a8c5db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc5b6024-13de-42e0-9e33-69c40d4bdd3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d29d879-552b-4c8f-93ac-c42277c6b929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc5b6024-13de-42e0-9e33-69c40d4bdd3d",
                    "LayerId": "b94ac3a6-fba8-4512-89da-d3e90a68265d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b94ac3a6-fba8-4512-89da-d3e90a68265d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7cfb8e0-8c8a-4103-8db4-6096497f9d6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 16
}