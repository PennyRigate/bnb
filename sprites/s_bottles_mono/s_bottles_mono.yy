{
    "id": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bottles_mono",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f8b0ca2-ae91-4ca5-ab95-5d2ffa102647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "e9cdc534-d149-4d95-b4eb-15ce98c5be57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8b0ca2-ae91-4ca5-ab95-5d2ffa102647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8073b62e-cb3d-4c21-9a20-cb84de8dcf36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8b0ca2-ae91-4ca5-ab95-5d2ffa102647",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        },
        {
            "id": "5932b5bc-3617-43ba-9ea5-513542390638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "f5e5d73a-8862-4ba3-92ed-34bda9af8158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5932b5bc-3617-43ba-9ea5-513542390638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc95fbbf-f21d-4c5f-b8ff-24ab9581efc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5932b5bc-3617-43ba-9ea5-513542390638",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        },
        {
            "id": "053c198a-8010-4272-905d-78ee41760e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "08ec6649-01d5-4519-a653-f21e8ea92275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053c198a-8010-4272-905d-78ee41760e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8f2758-9f22-4021-97c1-fab31e476ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053c198a-8010-4272-905d-78ee41760e50",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        },
        {
            "id": "be8ac3f2-7946-46e8-8b81-609586471250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "b0693779-9a70-46c1-8152-15cf1a310ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8ac3f2-7946-46e8-8b81-609586471250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cac977b-4ccc-4731-af16-2a546e2f7064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8ac3f2-7946-46e8-8b81-609586471250",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        },
        {
            "id": "8794ac73-0784-4d8d-a96e-dd5ab1ef152d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "b91a6f11-68d6-48fb-a4b7-82794657e09a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8794ac73-0784-4d8d-a96e-dd5ab1ef152d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "875c3fc8-e0eb-4e77-b377-63221b628009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8794ac73-0784-4d8d-a96e-dd5ab1ef152d",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        },
        {
            "id": "1c855568-5297-470b-8ec5-2fdb1b3858a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "11b5f122-9b4e-444b-a2b7-5f3ded308618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c855568-5297-470b-8ec5-2fdb1b3858a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1bc5f7-ec43-4706-adbd-d0a053bbd67c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c855568-5297-470b-8ec5-2fdb1b3858a5",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        },
        {
            "id": "3486a7ef-8ab6-4b05-9c55-4f5cd6c795e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "compositeImage": {
                "id": "9db72442-501b-402b-8b1f-cbce9b63c3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3486a7ef-8ab6-4b05-9c55-4f5cd6c795e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1f9f56-f0ff-4f2e-a651-68ea8358d3b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3486a7ef-8ab6-4b05-9c55-4f5cd6c795e2",
                    "LayerId": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "29ecfa35-627b-4d9f-8cf9-3e6c4eb3d9aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20bfe817-ad52-4866-8d5f-e5ebac52a0bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}