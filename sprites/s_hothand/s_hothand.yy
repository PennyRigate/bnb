{
    "id": "4877827a-8f84-48b4-9f40-88586d426475",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hothand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2c34d9c-d621-49dd-9407-ef91327f7cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4877827a-8f84-48b4-9f40-88586d426475",
            "compositeImage": {
                "id": "da631821-8e18-4d21-b248-c4b168fb4d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c34d9c-d621-49dd-9407-ef91327f7cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be18e633-2296-4416-8692-f788600cafd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c34d9c-d621-49dd-9407-ef91327f7cbd",
                    "LayerId": "70b77390-9b34-42d6-9e54-947be36a294a"
                }
            ]
        },
        {
            "id": "3533cdc1-8fe1-4895-8abb-8039aaebdb51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4877827a-8f84-48b4-9f40-88586d426475",
            "compositeImage": {
                "id": "20dd8cd4-cda8-42cf-8040-42be6f9f802e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3533cdc1-8fe1-4895-8abb-8039aaebdb51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfdfaa3-e4d6-4237-8f83-091c9a8a374b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3533cdc1-8fe1-4895-8abb-8039aaebdb51",
                    "LayerId": "70b77390-9b34-42d6-9e54-947be36a294a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "70b77390-9b34-42d6-9e54-947be36a294a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4877827a-8f84-48b4-9f40-88586d426475",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}