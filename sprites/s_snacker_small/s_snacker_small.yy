{
    "id": "bee548a3-7ff1-4cbe-b3d1-642f7c452084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_snacker_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db769731-4bbd-4807-bb9b-23b73753bb97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bee548a3-7ff1-4cbe-b3d1-642f7c452084",
            "compositeImage": {
                "id": "2803172d-396e-4639-b93f-a2e8cc8c6706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db769731-4bbd-4807-bb9b-23b73753bb97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4964ea8b-1ec0-44d8-a7dd-43374aa34510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db769731-4bbd-4807-bb9b-23b73753bb97",
                    "LayerId": "9c95e66a-80fc-47c5-9ddc-2f7367f75562"
                }
            ]
        },
        {
            "id": "f9379bbe-d2cd-473e-87f2-4cbaad0e334e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bee548a3-7ff1-4cbe-b3d1-642f7c452084",
            "compositeImage": {
                "id": "42194df5-5e17-4178-900e-d741c7ef4d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9379bbe-d2cd-473e-87f2-4cbaad0e334e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "210cdd7d-ae82-4645-ac62-b173690c652b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9379bbe-d2cd-473e-87f2-4cbaad0e334e",
                    "LayerId": "9c95e66a-80fc-47c5-9ddc-2f7367f75562"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9c95e66a-80fc-47c5-9ddc-2f7367f75562",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bee548a3-7ff1-4cbe-b3d1-642f7c452084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 0
}