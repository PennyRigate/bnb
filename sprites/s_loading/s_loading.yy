{
    "id": "5bf13635-8a3b-44b0-926a-074d1dc29cfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_loading",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "537aca6f-5465-4e28-b390-bf4a7a391fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bf13635-8a3b-44b0-926a-074d1dc29cfc",
            "compositeImage": {
                "id": "671a877c-9e80-44a6-b58d-d8bdf1c1cf1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537aca6f-5465-4e28-b390-bf4a7a391fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909cebc2-7d41-497a-adb9-1c627faa0c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537aca6f-5465-4e28-b390-bf4a7a391fce",
                    "LayerId": "77a16a4c-175f-4f6a-96a6-a98707823e2d"
                }
            ]
        },
        {
            "id": "5562a3c3-92af-4fc2-9123-48ce94e82991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bf13635-8a3b-44b0-926a-074d1dc29cfc",
            "compositeImage": {
                "id": "924b6a03-5b63-4623-a819-36c965f6dab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5562a3c3-92af-4fc2-9123-48ce94e82991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab4b45e-dd7f-4716-bd01-4b7b84116c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5562a3c3-92af-4fc2-9123-48ce94e82991",
                    "LayerId": "77a16a4c-175f-4f6a-96a6-a98707823e2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "77a16a4c-175f-4f6a-96a6-a98707823e2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bf13635-8a3b-44b0-926a-074d1dc29cfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}