{
    "id": "0cf692c5-3933-42f3-a1b1-427fbc986771",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_flipflap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8460a6c9-6229-4f80-bd1c-fb8f2276c838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cf692c5-3933-42f3-a1b1-427fbc986771",
            "compositeImage": {
                "id": "d8789b70-db81-4373-9570-9a0520bb0093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8460a6c9-6229-4f80-bd1c-fb8f2276c838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca93d03-7e55-4baa-a307-9be2acf49161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8460a6c9-6229-4f80-bd1c-fb8f2276c838",
                    "LayerId": "3bdc1525-22bf-4032-ba6c-934fa5e6e979"
                }
            ]
        },
        {
            "id": "a4c4ceff-d8e3-4653-ad52-25f75ac65949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cf692c5-3933-42f3-a1b1-427fbc986771",
            "compositeImage": {
                "id": "61d55ef2-7e84-4030-af2b-5fabfa41c457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c4ceff-d8e3-4653-ad52-25f75ac65949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66ef08ed-11bc-40ad-abe2-4526c9101c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c4ceff-d8e3-4653-ad52-25f75ac65949",
                    "LayerId": "3bdc1525-22bf-4032-ba6c-934fa5e6e979"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3bdc1525-22bf-4032-ba6c-934fa5e6e979",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cf692c5-3933-42f3-a1b1-427fbc986771",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}