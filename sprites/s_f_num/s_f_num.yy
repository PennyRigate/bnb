{
    "id": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_f_num",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fb4645c-7c45-43d3-b96c-57d86ed68298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "a75119d9-5320-40da-b23d-5cfa7c92e7cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb4645c-7c45-43d3-b96c-57d86ed68298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394b56fd-63b2-4ad0-8f70-3799874818b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb4645c-7c45-43d3-b96c-57d86ed68298",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "625e91c4-2a3c-4130-9b58-a72159494745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "4c700741-53e2-4682-953b-b24a14e8472f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "625e91c4-2a3c-4130-9b58-a72159494745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b3646b-39f4-40b9-9776-3bfd4a38989b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "625e91c4-2a3c-4130-9b58-a72159494745",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "35d94971-d012-4947-93f7-00dc050ab52f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "85a9f8e7-a4b4-4ed3-b985-d355d689c6b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d94971-d012-4947-93f7-00dc050ab52f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6ef622b-af33-480f-8d76-57e96da1939c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d94971-d012-4947-93f7-00dc050ab52f",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "116fef5c-b5ff-44c2-8c1c-bd2b55502342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "1eec8cd5-9eeb-450f-9e1e-9a76b6c1f0ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116fef5c-b5ff-44c2-8c1c-bd2b55502342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2b8ad0-f04e-420f-adf2-676bdfdb8971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116fef5c-b5ff-44c2-8c1c-bd2b55502342",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "ec4e0523-f266-49d8-ad32-c44f81e01950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "73ffb611-e087-4158-b804-0148ac4b7d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec4e0523-f266-49d8-ad32-c44f81e01950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced9a369-31c2-4965-8be4-7ea5b14ee03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec4e0523-f266-49d8-ad32-c44f81e01950",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "ed7ede14-e259-4e24-95b2-2b57b93aca3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "24d635cc-c6e4-4828-844f-cd4144b270d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed7ede14-e259-4e24-95b2-2b57b93aca3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93bd5296-a499-437c-ad53-3cb219200e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed7ede14-e259-4e24-95b2-2b57b93aca3a",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "4a332af0-b2a1-4ff4-a8d0-320c3032d931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "43eebd8a-efa4-4bd8-b056-4226d80831ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a332af0-b2a1-4ff4-a8d0-320c3032d931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9427392-3551-47e8-86a4-435d59ddf093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a332af0-b2a1-4ff4-a8d0-320c3032d931",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "be840581-46d5-4b6a-8b2e-f7a394d9f7da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "1bb7a509-eed4-4793-bc19-af5682fcf042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be840581-46d5-4b6a-8b2e-f7a394d9f7da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5524cd-4c9a-4e15-bf58-35215537a771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be840581-46d5-4b6a-8b2e-f7a394d9f7da",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "c013b052-8e6f-4c52-bc79-66470fce8ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "ec2004f3-18bf-414c-b3fb-a0d852107310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c013b052-8e6f-4c52-bc79-66470fce8ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a6d830-f36b-4ab2-8831-fd237f427cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c013b052-8e6f-4c52-bc79-66470fce8ab7",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "91616357-d977-4c16-9829-dd00358a6f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "e7f6030d-3f35-4613-b770-d4bbc972cc38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91616357-d977-4c16-9829-dd00358a6f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bfb2179-d07a-43c8-8eff-c532c4ae124a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91616357-d977-4c16-9829-dd00358a6f38",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "71434609-5178-4ace-8cef-2e87681f5e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "6fa3852c-1c5f-4600-90dd-0c53f1cb16e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71434609-5178-4ace-8cef-2e87681f5e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ff52db-b35a-4385-85fd-7d17094d7e7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71434609-5178-4ace-8cef-2e87681f5e8b",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        },
        {
            "id": "3865d703-bbef-4324-aedf-b0660de7608c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "compositeImage": {
                "id": "21f89732-a615-43d7-9d70-025670d890d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3865d703-bbef-4324-aedf-b0660de7608c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb874961-b78c-472d-98d2-b12b48a59441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3865d703-bbef-4324-aedf-b0660de7608c",
                    "LayerId": "8bf97cfa-3eeb-447e-888c-e7fd78177f70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "8bf97cfa-3eeb-447e-888c-e7fd78177f70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b84e6dac-ff72-4dd8-8038-c78c8d3c896a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}