{
    "id": "dde0f87b-9d02-41cf-b1a3-a298303e1be0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_jinjo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ef6ecb8-08b2-4ba3-b80c-b607263e3ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde0f87b-9d02-41cf-b1a3-a298303e1be0",
            "compositeImage": {
                "id": "033a9d5c-8b21-4302-b33c-a98f902912fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ef6ecb8-08b2-4ba3-b80c-b607263e3ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec0ae798-ca65-4250-9bb4-88ba0d87d662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ef6ecb8-08b2-4ba3-b80c-b607263e3ee8",
                    "LayerId": "c16e0dd9-611b-4328-9564-f1e03bc63a08"
                }
            ]
        },
        {
            "id": "c048857f-2db6-480d-9b53-f931632b380f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde0f87b-9d02-41cf-b1a3-a298303e1be0",
            "compositeImage": {
                "id": "676bb994-710a-4685-a77a-31b739751097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c048857f-2db6-480d-9b53-f931632b380f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adb10864-5c6d-4eb6-b1e6-5b18873ca724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c048857f-2db6-480d-9b53-f931632b380f",
                    "LayerId": "c16e0dd9-611b-4328-9564-f1e03bc63a08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c16e0dd9-611b-4328-9564-f1e03bc63a08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dde0f87b-9d02-41cf-b1a3-a298303e1be0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}