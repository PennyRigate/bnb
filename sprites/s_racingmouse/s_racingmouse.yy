{
    "id": "b3eeb7a2-be82-4c46-befa-e3754cc1c79b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_racingmouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a8bd158-d293-4b54-85fd-2279c494173b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3eeb7a2-be82-4c46-befa-e3754cc1c79b",
            "compositeImage": {
                "id": "d98d0ab6-d6d4-455f-b6c1-7311ba730686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8bd158-d293-4b54-85fd-2279c494173b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c522df8a-2cf8-456a-a2f6-21a11d2bc5b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8bd158-d293-4b54-85fd-2279c494173b",
                    "LayerId": "f234e4cd-df6a-448b-b1db-b9428f9e5fea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f234e4cd-df6a-448b-b1db-b9428f9e5fea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3eeb7a2-be82-4c46-befa-e3754cc1c79b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 0
}