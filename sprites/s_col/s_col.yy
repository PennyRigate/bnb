{
    "id": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c4b579e-45db-4d9e-8675-88e9a5dd3cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "f6571546-2c68-4db0-add6-44a64853f7b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c4b579e-45db-4d9e-8675-88e9a5dd3cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade11ee8-38bd-476b-995e-d334abac3287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c4b579e-45db-4d9e-8675-88e9a5dd3cb9",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "78d19cb6-e82b-4e67-9743-98b28bf81773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "36d4ed27-259e-417e-814b-bffcf77f3dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d19cb6-e82b-4e67-9743-98b28bf81773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc95d25-5991-460e-9084-ad5c8b738773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d19cb6-e82b-4e67-9743-98b28bf81773",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "e1c43bd8-da75-44b8-a2c6-0dfb4019d1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "279e14e7-9bee-46a3-8a80-c494bb8f1c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c43bd8-da75-44b8-a2c6-0dfb4019d1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af9df9d4-ddcf-41b4-9aa9-fdd54c7b9f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c43bd8-da75-44b8-a2c6-0dfb4019d1c5",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "076c2b50-0eb3-4faa-bb03-669def7572d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "427d0372-f4ee-4071-9a94-aae1473ae20c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076c2b50-0eb3-4faa-bb03-669def7572d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6d0222-5eae-42b9-96a5-789a5e4be891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076c2b50-0eb3-4faa-bb03-669def7572d2",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "32e0f4e0-d60e-4f59-a104-cee0b0f47228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "f3d24074-1102-4e0d-97b3-d848446dd5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e0f4e0-d60e-4f59-a104-cee0b0f47228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5169ede9-d102-4009-988e-784b8dea8420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e0f4e0-d60e-4f59-a104-cee0b0f47228",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "d7d5f239-956a-46de-988c-f7868d68f831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "e5d2229f-dfe0-4a83-bb38-70e53bd41c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7d5f239-956a-46de-988c-f7868d68f831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e494eb-2681-4b22-b3da-b405064e63dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7d5f239-956a-46de-988c-f7868d68f831",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "f5e90c46-b646-436c-bda8-d885cdc2a166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "9ec4ccc8-762a-4dc0-afda-ce2db555aff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e90c46-b646-436c-bda8-d885cdc2a166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc1cd42-1e24-4773-865e-5c7773dde798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e90c46-b646-436c-bda8-d885cdc2a166",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "ebc11547-150c-4ff4-937a-1afd1cd290bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "7dcde707-0a31-49e6-a0e4-0a4a05fb19d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc11547-150c-4ff4-937a-1afd1cd290bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9beff3-f11a-45e1-9fe0-d15c2de6fb67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc11547-150c-4ff4-937a-1afd1cd290bb",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "1816f1c9-2561-4581-9b36-36e1a0c144f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "6c5fee4f-5555-4053-aa9d-6c984ae8053e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1816f1c9-2561-4581-9b36-36e1a0c144f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d6dde5-82e8-40dd-b264-2fd490c5c9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1816f1c9-2561-4581-9b36-36e1a0c144f1",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "bb6df3f1-2b22-4ce9-b8a4-1c16e0d4a36f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "52b40e9f-e49a-48aa-a2c3-6b919c117d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb6df3f1-2b22-4ce9-b8a4-1c16e0d4a36f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c96d30-cb6d-467b-9818-5f243956dfbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb6df3f1-2b22-4ce9-b8a4-1c16e0d4a36f",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "c35f9372-dcf2-413a-98ce-cd3c68ecc77e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "677a33d8-fa69-4fbf-a87f-e9cb556b5a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c35f9372-dcf2-413a-98ce-cd3c68ecc77e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23890ec7-90dd-4520-86ec-651d4994cf1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c35f9372-dcf2-413a-98ce-cd3c68ecc77e",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "8a2e3ac1-1e13-416b-a473-75ca94b04732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "5c0e83df-b40e-4e3a-8c60-49d6e05d70bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a2e3ac1-1e13-416b-a473-75ca94b04732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b1f87b-ec96-4a6c-a9f4-e81db63b66ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a2e3ac1-1e13-416b-a473-75ca94b04732",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "b9ea0066-2c66-454e-afb4-7a92dc6d944f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "3ff08336-bc92-4a71-b299-003688fadb6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ea0066-2c66-454e-afb4-7a92dc6d944f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd36bd3d-0d01-497b-9eb3-689f4103c044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ea0066-2c66-454e-afb4-7a92dc6d944f",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        },
        {
            "id": "22dfbfa0-74d8-402e-859f-9b794deb8388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "compositeImage": {
                "id": "5d21e2c7-9781-485b-9d02-05351bdf3f2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22dfbfa0-74d8-402e-859f-9b794deb8388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7aaeb8-166f-4227-ad71-549d51ffea64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22dfbfa0-74d8-402e-859f-9b794deb8388",
                    "LayerId": "5572863b-4ed8-4758-82de-e3e98c2b1f17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5572863b-4ed8-4758-82de-e3e98c2b1f17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6f8ab2f-e6fa-41a6-8e3e-a31639ca8cf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}