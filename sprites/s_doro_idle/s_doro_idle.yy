{
    "id": "3038d56f-7098-44b1-8b09-c82649953623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_doro_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95320930-d660-42e1-95bf-6d4ea8c2452f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3038d56f-7098-44b1-8b09-c82649953623",
            "compositeImage": {
                "id": "8da703f5-7866-48ee-808a-ceeca56920eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95320930-d660-42e1-95bf-6d4ea8c2452f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977b8cec-d2bc-4878-b194-14d034493718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95320930-d660-42e1-95bf-6d4ea8c2452f",
                    "LayerId": "e874ae01-5763-40b3-aa07-ff5a7af63035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e874ae01-5763-40b3-aa07-ff5a7af63035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3038d56f-7098-44b1-8b09-c82649953623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}