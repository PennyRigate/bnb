{
    "id": "5cd646cb-425c-4642-8da5-79a8ace50cd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "h_pipe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e116a2e-a0be-4c11-8637-31b2f55ffd79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd646cb-425c-4642-8da5-79a8ace50cd5",
            "compositeImage": {
                "id": "7e002c88-f63a-46c6-87f4-5f09a419867b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e116a2e-a0be-4c11-8637-31b2f55ffd79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6f6930f-49ee-48bd-90f7-5a5944b3aaf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e116a2e-a0be-4c11-8637-31b2f55ffd79",
                    "LayerId": "42a2e301-1465-43a3-8752-efe05fc7863d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "42a2e301-1465-43a3-8752-efe05fc7863d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cd646cb-425c-4642-8da5-79a8ace50cd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}