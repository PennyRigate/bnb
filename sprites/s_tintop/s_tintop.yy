{
    "id": "f1514996-194d-4f72-ba75-9cc1069464a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tintop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282c7648-422d-4f91-87fe-d90e467f8713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1514996-194d-4f72-ba75-9cc1069464a0",
            "compositeImage": {
                "id": "23676d71-5c34-4aa5-b569-5c1fbd5aa47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282c7648-422d-4f91-87fe-d90e467f8713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfe352f-65ed-4cd0-b23c-81c687fb2cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282c7648-422d-4f91-87fe-d90e467f8713",
                    "LayerId": "3309a1b5-b1e7-4690-ac13-151912efb4a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3309a1b5-b1e7-4690-ac13-151912efb4a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1514996-194d-4f72-ba75-9cc1069464a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}