{
    "id": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ladder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b116100a-7e9b-4d4b-ac69-1dc57a353b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
            "compositeImage": {
                "id": "13644640-63eb-4efa-8c58-9f6e164a031f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b116100a-7e9b-4d4b-ac69-1dc57a353b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a2b24ad-f513-4696-a53b-76e31f4460bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b116100a-7e9b-4d4b-ac69-1dc57a353b4a",
                    "LayerId": "09ee8417-7d87-479c-8ac2-694bb2467f54"
                }
            ]
        },
        {
            "id": "aed7df41-e89b-4c04-a5ea-9086bdf1005f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
            "compositeImage": {
                "id": "2672ea7c-fa0a-42b6-b950-81431b18c988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed7df41-e89b-4c04-a5ea-9086bdf1005f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6f0af4-bdc5-409d-83bb-b2d2535476e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed7df41-e89b-4c04-a5ea-9086bdf1005f",
                    "LayerId": "09ee8417-7d87-479c-8ac2-694bb2467f54"
                }
            ]
        },
        {
            "id": "2118ac12-8c73-49ce-81ae-c5e9e47df63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
            "compositeImage": {
                "id": "50b1fc34-ed55-4d0a-a2e0-f0ed23bc0ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2118ac12-8c73-49ce-81ae-c5e9e47df63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2201d7ce-c047-41e5-8042-5f6d76d342bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2118ac12-8c73-49ce-81ae-c5e9e47df63c",
                    "LayerId": "09ee8417-7d87-479c-8ac2-694bb2467f54"
                }
            ]
        },
        {
            "id": "7a815054-2b01-4fed-8543-a866ca84fefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
            "compositeImage": {
                "id": "76350e20-1c54-4222-a9b4-a5b923e19433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a815054-2b01-4fed-8543-a866ca84fefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f5153e-e922-4d4f-abeb-0aeeb8012df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a815054-2b01-4fed-8543-a866ca84fefc",
                    "LayerId": "09ee8417-7d87-479c-8ac2-694bb2467f54"
                }
            ]
        },
        {
            "id": "822dccaf-2bb6-424b-a532-8847d1d705dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
            "compositeImage": {
                "id": "03aa2da4-08ef-4eed-a141-01312739001f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822dccaf-2bb6-424b-a532-8847d1d705dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a88163-1d47-45cb-92a0-6fcde4f92725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822dccaf-2bb6-424b-a532-8847d1d705dc",
                    "LayerId": "09ee8417-7d87-479c-8ac2-694bb2467f54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "09ee8417-7d87-479c-8ac2-694bb2467f54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03531e24-01bf-458f-8c2a-bf0e57aaa75c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}