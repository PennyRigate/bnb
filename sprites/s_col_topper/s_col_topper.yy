{
    "id": "1b3502b1-93b0-4518-818f-deb999908435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_topper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c959329e-1345-44d8-8ca6-c06d095ea4cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b3502b1-93b0-4518-818f-deb999908435",
            "compositeImage": {
                "id": "f792839e-4790-46d0-a65c-833edfa09297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c959329e-1345-44d8-8ca6-c06d095ea4cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3e45f2-ec14-4b5f-bb48-d90863b7bb0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c959329e-1345-44d8-8ca6-c06d095ea4cb",
                    "LayerId": "ac53d028-1390-48bf-83d4-f7709877d812"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ac53d028-1390-48bf-83d4-f7709877d812",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b3502b1-93b0-4518-818f-deb999908435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 24
}