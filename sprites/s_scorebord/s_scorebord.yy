{
    "id": "8293d34f-d1c2-4f66-9ee6-c7a236807fe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_scorebord",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 3,
    "bbox_right": 202,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cadff01-43cc-4d97-b152-645ad1db953a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8293d34f-d1c2-4f66-9ee6-c7a236807fe0",
            "compositeImage": {
                "id": "62f0ef5a-ef21-40d2-bf13-8b7a1f89f29c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cadff01-43cc-4d97-b152-645ad1db953a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a06beb4-c468-4b72-8002-00d559218e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cadff01-43cc-4d97-b152-645ad1db953a",
                    "LayerId": "9ff0b029-dc59-4da1-840e-113400af8b07"
                }
            ]
        },
        {
            "id": "8cdc60f7-cb3d-422c-8c71-e4a45b8637af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8293d34f-d1c2-4f66-9ee6-c7a236807fe0",
            "compositeImage": {
                "id": "ad85b4f3-78ff-460c-a4d4-f41d64799f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdc60f7-cb3d-422c-8c71-e4a45b8637af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78273c76-a0af-4019-ac8f-71902610f4bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdc60f7-cb3d-422c-8c71-e4a45b8637af",
                    "LayerId": "9ff0b029-dc59-4da1-840e-113400af8b07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9ff0b029-dc59-4da1-840e-113400af8b07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8293d34f-d1c2-4f66-9ee6-c7a236807fe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}