{
    "id": "68b5bdd1-c59d-4485-b5c3-34bc82bf7dc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cube",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb5135b2-646e-4da4-9876-f4ce6bc4765a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b5bdd1-c59d-4485-b5c3-34bc82bf7dc7",
            "compositeImage": {
                "id": "79227ec1-ba80-462e-954b-1747a5fbd083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb5135b2-646e-4da4-9876-f4ce6bc4765a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "214ac15a-cc60-40ed-b287-44ee55e53819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb5135b2-646e-4da4-9876-f4ce6bc4765a",
                    "LayerId": "adcd3400-f5d6-4917-9a40-e9c901792c7a"
                }
            ]
        },
        {
            "id": "30761df3-b6aa-47e0-a1a6-7dbea4b1cc41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b5bdd1-c59d-4485-b5c3-34bc82bf7dc7",
            "compositeImage": {
                "id": "6a9d2ad9-6d51-41ea-957d-7f1a7653d202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30761df3-b6aa-47e0-a1a6-7dbea4b1cc41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb05200-49d2-46b1-8db9-1c54d7981f15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30761df3-b6aa-47e0-a1a6-7dbea4b1cc41",
                    "LayerId": "adcd3400-f5d6-4917-9a40-e9c901792c7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "adcd3400-f5d6-4917-9a40-e9c901792c7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68b5bdd1-c59d-4485-b5c3-34bc82bf7dc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 8
}