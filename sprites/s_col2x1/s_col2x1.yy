{
    "id": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col2x1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b470fec-e0df-4de7-ae13-06f19c59fb6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "ecee8ba9-9a05-4fc2-9e8d-f2838e5b109f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b470fec-e0df-4de7-ae13-06f19c59fb6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33dad7fc-6568-478b-8ece-ec6414379155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b470fec-e0df-4de7-ae13-06f19c59fb6b",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "1ae53fc2-11fb-4df5-b116-9b9f9afa7554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "d932d82a-d4ef-400f-8258-61d4c507efd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ae53fc2-11fb-4df5-b116-9b9f9afa7554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54def157-fdc8-45b8-816b-a852d6fb6e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ae53fc2-11fb-4df5-b116-9b9f9afa7554",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "e2c91c74-97f9-4736-b1e6-a139924b39b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "1673c416-2bd9-47dc-b802-118eceb7bc87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2c91c74-97f9-4736-b1e6-a139924b39b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb86a1e3-a895-44ac-8b41-0a23d0a4632e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2c91c74-97f9-4736-b1e6-a139924b39b9",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "22229238-1632-42b3-b126-ab5ecd9659e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "62f5d979-18a1-490b-a388-1404ccda2aff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22229238-1632-42b3-b126-ab5ecd9659e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1689a7-824e-4e8e-9feb-a34fe3d0f671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22229238-1632-42b3-b126-ab5ecd9659e0",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "8c6e711c-3050-4ee0-9a9e-ca69880cd307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "0006eb59-3c95-4bab-816f-363ac4c8e567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6e711c-3050-4ee0-9a9e-ca69880cd307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8aec3aa-e887-4c80-a2c4-e286111f6443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6e711c-3050-4ee0-9a9e-ca69880cd307",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "f0ced17c-88da-43ec-b03c-aa4a673527f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "c4dd77f5-638d-4e54-9d81-43977cb3e35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ced17c-88da-43ec-b03c-aa4a673527f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be872b12-ddd3-4fcf-a37c-e55325992a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ced17c-88da-43ec-b03c-aa4a673527f2",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "3f4d7cef-8459-4e2b-9070-535509cdc23e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "36d8c5a1-0f46-44f9-8ffc-35f12411db80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f4d7cef-8459-4e2b-9070-535509cdc23e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12555dfd-6488-4c56-b974-b9d16bdf29b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f4d7cef-8459-4e2b-9070-535509cdc23e",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        },
        {
            "id": "9934e98d-d182-4424-8810-bcdece8b9728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "compositeImage": {
                "id": "856f0ce5-8c81-49d2-bdbd-0bcaa7cf69ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9934e98d-d182-4424-8810-bcdece8b9728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31318aed-cb67-4993-9267-7addbd527be4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9934e98d-d182-4424-8810-bcdece8b9728",
                    "LayerId": "1a3287fe-7192-4e90-86d5-fe0c28387958"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1a3287fe-7192-4e90-86d5-fe0c28387958",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7745ad5-fad9-43cf-85c7-56b8a60d6278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}