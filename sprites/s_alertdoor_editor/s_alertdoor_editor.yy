{
    "id": "af502e47-ec4a-4413-a3d4-31fa6ee1dee5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_alertdoor_editor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e9d7351-8c00-4bb6-81f8-412c6e636d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af502e47-ec4a-4413-a3d4-31fa6ee1dee5",
            "compositeImage": {
                "id": "820d9d95-72a0-4fa0-a0ad-b662e026245a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9d7351-8c00-4bb6-81f8-412c6e636d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11e8ff75-1322-4bf8-97b9-0399284e57ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9d7351-8c00-4bb6-81f8-412c6e636d1f",
                    "LayerId": "d1c9e2b7-f4a0-4b92-8c68-1cca5ac1a751"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d1c9e2b7-f4a0-4b92-8c68-1cca5ac1a751",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af502e47-ec4a-4413-a3d4-31fa6ee1dee5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}