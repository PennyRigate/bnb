{
    "id": "8b993b73-73b9-4ae4-8dce-671df19ccc32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c4df574-c152-4616-b5fe-9e29140b4506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b993b73-73b9-4ae4-8dce-671df19ccc32",
            "compositeImage": {
                "id": "4e0fd1bb-bdc3-421c-a0e3-65d4f6f733f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c4df574-c152-4616-b5fe-9e29140b4506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7000c43c-09c2-4ab3-8686-ae246e52532b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c4df574-c152-4616-b5fe-9e29140b4506",
                    "LayerId": "fc31619e-f400-40e4-b93b-e131b9206e65"
                }
            ]
        },
        {
            "id": "6901b4ad-6ed0-4ed6-a67f-6b160c155b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b993b73-73b9-4ae4-8dce-671df19ccc32",
            "compositeImage": {
                "id": "164d919c-c6b5-4c91-ab4d-a2361a0a9ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6901b4ad-6ed0-4ed6-a67f-6b160c155b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766ea89d-3825-4c9e-a5e9-c49b0cf9edd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6901b4ad-6ed0-4ed6-a67f-6b160c155b4a",
                    "LayerId": "fc31619e-f400-40e4-b93b-e131b9206e65"
                }
            ]
        },
        {
            "id": "82ac386b-d9c8-4a0a-bef6-b2cae6086dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b993b73-73b9-4ae4-8dce-671df19ccc32",
            "compositeImage": {
                "id": "68a5d8cf-5b32-41be-a0b8-50c61a26a4b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ac386b-d9c8-4a0a-bef6-b2cae6086dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e5dc13f-319c-4112-a21b-fe0cfb213bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ac386b-d9c8-4a0a-bef6-b2cae6086dfb",
                    "LayerId": "fc31619e-f400-40e4-b93b-e131b9206e65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fc31619e-f400-40e4-b93b-e131b9206e65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b993b73-73b9-4ae4-8dce-671df19ccc32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}