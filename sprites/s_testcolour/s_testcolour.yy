{
    "id": "38782739-fca0-4636-abec-cf2b02ab3503",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_testcolour",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb110ac8-8c42-46aa-88a5-48e407898eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38782739-fca0-4636-abec-cf2b02ab3503",
            "compositeImage": {
                "id": "82f1b604-069a-429d-b86f-9daa8d2f75b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb110ac8-8c42-46aa-88a5-48e407898eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960e8e6d-a26f-4194-afe3-0a1f403fc0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb110ac8-8c42-46aa-88a5-48e407898eef",
                    "LayerId": "1bba9da7-868e-4062-8061-4ad3efaa0fcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1bba9da7-868e-4062-8061-4ad3efaa0fcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38782739-fca0-4636-abec-cf2b02ab3503",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}