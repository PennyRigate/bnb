{
    "id": "e8342b10-9396-4168-8047-7cbc5665ff63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_finalegg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6360ff9-903f-4b48-a6fe-bf3873c13a64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8342b10-9396-4168-8047-7cbc5665ff63",
            "compositeImage": {
                "id": "8d2d576f-7e3c-4688-80ed-0c7b3dbc8715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6360ff9-903f-4b48-a6fe-bf3873c13a64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e43d0f4-96cf-4c12-a2ac-0fbe01f74a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6360ff9-903f-4b48-a6fe-bf3873c13a64",
                    "LayerId": "32ddbc14-b8bf-46c9-8575-2e5b90b3cd54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "32ddbc14-b8bf-46c9-8575-2e5b90b3cd54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8342b10-9396-4168-8047-7cbc5665ff63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}