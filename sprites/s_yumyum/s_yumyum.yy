{
    "id": "834a8470-c1ae-492c-848b-f51fc092a33a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_yumyum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9481384f-a9aa-4de6-8149-bf0574de44eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834a8470-c1ae-492c-848b-f51fc092a33a",
            "compositeImage": {
                "id": "92be8dec-bfe6-44c3-b98e-b3c0c9f3cbfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9481384f-a9aa-4de6-8149-bf0574de44eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83e6e55-cae3-4cd3-a36d-2c868c003128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9481384f-a9aa-4de6-8149-bf0574de44eb",
                    "LayerId": "718e1879-1984-423f-a357-786d08ab4806"
                }
            ]
        },
        {
            "id": "6b019659-430b-45f9-bb4d-b3ee3457d217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834a8470-c1ae-492c-848b-f51fc092a33a",
            "compositeImage": {
                "id": "0ca0ac57-e12f-4f4d-8567-ad7ecd3e41d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b019659-430b-45f9-bb4d-b3ee3457d217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b1047bb-4749-498e-9788-d08f356b8bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b019659-430b-45f9-bb4d-b3ee3457d217",
                    "LayerId": "718e1879-1984-423f-a357-786d08ab4806"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "718e1879-1984-423f-a357-786d08ab4806",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "834a8470-c1ae-492c-848b-f51fc092a33a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 2.8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 0
}