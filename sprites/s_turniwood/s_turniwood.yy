{
    "id": "966adee8-b748-4a2c-a4cb-f90477e673d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_turniwood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b97baee-601d-47dd-811e-3d8865421ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
            "compositeImage": {
                "id": "0ed593fd-639a-4fc0-a147-d284233fa678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b97baee-601d-47dd-811e-3d8865421ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208505a2-fc9e-4bc8-975c-a543a3e78000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b97baee-601d-47dd-811e-3d8865421ba1",
                    "LayerId": "caa03945-4170-419a-9753-c48cc2b1bbd6"
                }
            ]
        },
        {
            "id": "8c59e322-2548-40f4-af3a-f9e33622ef0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
            "compositeImage": {
                "id": "55273738-8085-4dcc-aa57-ee5c1c3c406a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c59e322-2548-40f4-af3a-f9e33622ef0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158e73cb-603d-4acd-8b27-8b3714acfcf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c59e322-2548-40f4-af3a-f9e33622ef0e",
                    "LayerId": "caa03945-4170-419a-9753-c48cc2b1bbd6"
                }
            ]
        },
        {
            "id": "7c8d51a4-3817-4f8c-a53a-2bcc9f6e8f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
            "compositeImage": {
                "id": "36ec407a-efb9-44e6-9d8f-a9297190a8fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8d51a4-3817-4f8c-a53a-2bcc9f6e8f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a223e274-da2b-4205-9969-ff15b5081d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8d51a4-3817-4f8c-a53a-2bcc9f6e8f76",
                    "LayerId": "caa03945-4170-419a-9753-c48cc2b1bbd6"
                }
            ]
        },
        {
            "id": "3fa534ea-2b78-4415-9cee-d26bad7b90ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
            "compositeImage": {
                "id": "b9edd536-bc41-4c4a-bce4-531351453788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa534ea-2b78-4415-9cee-d26bad7b90ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f83d6a1-b92d-4bde-9b9e-2bcf993c1540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa534ea-2b78-4415-9cee-d26bad7b90ae",
                    "LayerId": "caa03945-4170-419a-9753-c48cc2b1bbd6"
                }
            ]
        },
        {
            "id": "dd48cdc1-59f3-4fef-815e-1e5b014ead03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
            "compositeImage": {
                "id": "0ff22ae3-b4f7-445e-a72a-25207403f015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd48cdc1-59f3-4fef-815e-1e5b014ead03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1043c56-8e65-40e5-9dcd-ac1beb988012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd48cdc1-59f3-4fef-815e-1e5b014ead03",
                    "LayerId": "caa03945-4170-419a-9753-c48cc2b1bbd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "caa03945-4170-419a-9753-c48cc2b1bbd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "966adee8-b748-4a2c-a4cb-f90477e673d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}