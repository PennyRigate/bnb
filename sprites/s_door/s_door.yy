{
    "id": "e2823d28-2f52-44d8-816d-a972c7b42c7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21df02bc-aff9-483e-a3e4-8292af546567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2823d28-2f52-44d8-816d-a972c7b42c7f",
            "compositeImage": {
                "id": "2a6000d2-8fa4-4b69-84b8-ff73132a1e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21df02bc-aff9-483e-a3e4-8292af546567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8283f949-2a06-4523-ba81-379f8219e1b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21df02bc-aff9-483e-a3e4-8292af546567",
                    "LayerId": "68cd138f-263d-4ddf-aa90-fb60fa4eb1c4"
                }
            ]
        },
        {
            "id": "49ed98e2-a7f9-4ed3-9d49-db0a261e89e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2823d28-2f52-44d8-816d-a972c7b42c7f",
            "compositeImage": {
                "id": "11702698-aed6-4df5-81d6-e355a3eab127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ed98e2-a7f9-4ed3-9d49-db0a261e89e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5259a528-3f88-4ab6-a662-e02bf50f695a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ed98e2-a7f9-4ed3-9d49-db0a261e89e9",
                    "LayerId": "68cd138f-263d-4ddf-aa90-fb60fa4eb1c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "68cd138f-263d-4ddf-aa90-fb60fa4eb1c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2823d28-2f52-44d8-816d-a972c7b42c7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}