{
    "id": "f15ffca7-57bd-4303-bf40-99f6c3f750c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f057a248-dc12-4856-ab6a-3d731574fea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f15ffca7-57bd-4303-bf40-99f6c3f750c8",
            "compositeImage": {
                "id": "4665921b-ba0e-4718-838d-41b9bc558949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f057a248-dc12-4856-ab6a-3d731574fea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b514ba81-df88-4dc3-b662-ab251316debe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f057a248-dc12-4856-ab6a-3d731574fea9",
                    "LayerId": "ca9e051f-c4f7-4745-a648-2f6d86780267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ca9e051f-c4f7-4745-a648-2f6d86780267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f15ffca7-57bd-4303-bf40-99f6c3f750c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}