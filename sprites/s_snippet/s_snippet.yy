{
    "id": "194dfb62-a8d7-43ee-861f-e0f2c9312b2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_snippet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eff2f7a-aad8-410f-bb40-7368ea9542fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "194dfb62-a8d7-43ee-861f-e0f2c9312b2d",
            "compositeImage": {
                "id": "a5c435e7-b85f-4dd1-9ae1-9a4c33219f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eff2f7a-aad8-410f-bb40-7368ea9542fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82d0893-0a45-4b03-88d1-36671c55b954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eff2f7a-aad8-410f-bb40-7368ea9542fd",
                    "LayerId": "69a50384-0fdf-4e78-b1fa-6f34c8457aa8"
                }
            ]
        },
        {
            "id": "970361fe-1b9e-4ea1-b74c-b8c50d1683e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "194dfb62-a8d7-43ee-861f-e0f2c9312b2d",
            "compositeImage": {
                "id": "45efb6cb-a105-431d-8274-8051149dbeed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "970361fe-1b9e-4ea1-b74c-b8c50d1683e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8191e0c-90a2-4341-a7ec-13116c4228d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "970361fe-1b9e-4ea1-b74c-b8c50d1683e5",
                    "LayerId": "69a50384-0fdf-4e78-b1fa-6f34c8457aa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "69a50384-0fdf-4e78-b1fa-6f34c8457aa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "194dfb62-a8d7-43ee-861f-e0f2c9312b2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}