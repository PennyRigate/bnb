{
    "id": "b9972f96-ae54-4560-8185-d7a94181b77b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lives_doro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e916fde4-83b7-4e01-9df5-61870bd98d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "96f25ca2-2121-4b3b-af25-a2970c56063a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e916fde4-83b7-4e01-9df5-61870bd98d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2edede2-2a49-4bc2-be5e-6d2cbafafcee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e916fde4-83b7-4e01-9df5-61870bd98d53",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        },
        {
            "id": "9ad75273-f9bd-4a37-a7ae-c1b9e890e0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "1924d65d-9627-4f57-8655-64b870b9c613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad75273-f9bd-4a37-a7ae-c1b9e890e0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0207443e-cee6-4a47-ae6c-d084500a5478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad75273-f9bd-4a37-a7ae-c1b9e890e0aa",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        },
        {
            "id": "943bb163-a3cd-48ae-a8e8-0d0d5625eef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "11c27632-5ee8-42b5-9f59-7b093c11ab2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943bb163-a3cd-48ae-a8e8-0d0d5625eef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791276fe-d056-4a0f-8e33-2bab6ef9846b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943bb163-a3cd-48ae-a8e8-0d0d5625eef0",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        },
        {
            "id": "0465af37-903e-4eca-87f9-5d4f8e1d2c50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "cde3d9ed-5a3e-4f05-a96c-d2e030b83e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0465af37-903e-4eca-87f9-5d4f8e1d2c50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a3bf22a-d3b4-4ad1-889d-2c50a972ea3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0465af37-903e-4eca-87f9-5d4f8e1d2c50",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        },
        {
            "id": "2ad94c70-0700-40fd-8b71-fc55e9a82e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "3dfdf269-bd3f-48ba-85d5-6c293690fceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad94c70-0700-40fd-8b71-fc55e9a82e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95a55b2-4a1a-4bf0-bcf4-a5602eb5022d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad94c70-0700-40fd-8b71-fc55e9a82e1e",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        },
        {
            "id": "bb5f1a71-bdc2-412a-96e7-2f90d6abd725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "8354cb26-1c0e-47ad-9717-2f3360ee406f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb5f1a71-bdc2-412a-96e7-2f90d6abd725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74fceec1-4cc8-4701-a2f1-6d668e475458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb5f1a71-bdc2-412a-96e7-2f90d6abd725",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        },
        {
            "id": "2bc48f51-cad0-40b3-bab1-3a7f860d1f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "compositeImage": {
                "id": "3725bf52-4e5f-4d5a-8230-e338d2ae3019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc48f51-cad0-40b3-bab1-3a7f860d1f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c642a5c2-cacd-4e8e-8e2c-697d6dce5a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc48f51-cad0-40b3-bab1-3a7f860d1f44",
                    "LayerId": "4501ce40-ad2c-416f-a136-3a089cc33c66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4501ce40-ad2c-416f-a136-3a089cc33c66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9972f96-ae54-4560-8185-d7a94181b77b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 0,
    "yorig": 0
}