{
    "id": "87bf40a2-3dd9-4f9f-9d7c-9400db6727f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1048816c-137e-44a7-ab04-c6c28681c3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87bf40a2-3dd9-4f9f-9d7c-9400db6727f1",
            "compositeImage": {
                "id": "3679fa2e-b3b6-4b97-9c48-b4d76e52b4bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1048816c-137e-44a7-ab04-c6c28681c3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6947ec13-18f0-4fe2-a458-a49130dfec2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1048816c-137e-44a7-ab04-c6c28681c3f1",
                    "LayerId": "1024b8ea-46b6-40c8-9ac9-6eacb912b4c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1024b8ea-46b6-40c8-9ac9-6eacb912b4c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87bf40a2-3dd9-4f9f-9d7c-9400db6727f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}