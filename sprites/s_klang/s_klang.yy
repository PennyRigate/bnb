{
    "id": "477dab43-d99d-4ee6-ba85-e17db969c63f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_klang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc97a2bc-3581-4539-bb14-9b58bb6c4e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477dab43-d99d-4ee6-ba85-e17db969c63f",
            "compositeImage": {
                "id": "d8ca77cd-8836-427b-95b6-3cf0ce659c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc97a2bc-3581-4539-bb14-9b58bb6c4e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad47364c-c40d-4ecf-b5f1-4b6a55e17c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc97a2bc-3581-4539-bb14-9b58bb6c4e27",
                    "LayerId": "87df7381-feae-4d88-ac63-03209e714b29"
                }
            ]
        },
        {
            "id": "ed559b41-0dd3-4e91-917b-5f6357af038b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "477dab43-d99d-4ee6-ba85-e17db969c63f",
            "compositeImage": {
                "id": "1d3da3cd-812a-4e82-94d6-358c6b23ac4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed559b41-0dd3-4e91-917b-5f6357af038b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae670d43-b36c-4491-9541-7cf945acc4cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed559b41-0dd3-4e91-917b-5f6357af038b",
                    "LayerId": "87df7381-feae-4d88-ac63-03209e714b29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "87df7381-feae-4d88-ac63-03209e714b29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "477dab43-d99d-4ee6-ba85-e17db969c63f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}