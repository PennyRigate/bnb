{
    "id": "f66e8e61-d7b1-4f1e-bef5-20921ccb03aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_doro_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9089f13a-a666-42ba-af78-759da646513d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f66e8e61-d7b1-4f1e-bef5-20921ccb03aa",
            "compositeImage": {
                "id": "27f5f223-15a3-4b3d-b20a-35e79c8368cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9089f13a-a666-42ba-af78-759da646513d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37027429-6076-4358-890c-028f39672edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9089f13a-a666-42ba-af78-759da646513d",
                    "LayerId": "7b754465-8503-4b8d-9af0-cfc0d7326c74"
                }
            ]
        },
        {
            "id": "f8924642-282e-4d10-a4b0-c5947330528d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f66e8e61-d7b1-4f1e-bef5-20921ccb03aa",
            "compositeImage": {
                "id": "57044134-adff-4c5d-8638-747e784277af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8924642-282e-4d10-a4b0-c5947330528d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382778f2-a463-4c10-ad6e-8fd6cef13e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8924642-282e-4d10-a4b0-c5947330528d",
                    "LayerId": "7b754465-8503-4b8d-9af0-cfc0d7326c74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b754465-8503-4b8d-9af0-cfc0d7326c74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f66e8e61-d7b1-4f1e-bef5-20921ccb03aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}