{
    "id": "ad548681-7e0f-4496-9861-8c82eca7d3ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_doro_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e883b5e2-405f-4ad5-b366-0fa010ea6961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad548681-7e0f-4496-9861-8c82eca7d3ce",
            "compositeImage": {
                "id": "69d8469d-b17f-44f7-a595-e077de8cbb8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e883b5e2-405f-4ad5-b366-0fa010ea6961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dbe06f5-ec23-48f8-8da3-e3ab13d7de0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e883b5e2-405f-4ad5-b366-0fa010ea6961",
                    "LayerId": "129b8e7a-bf8b-4137-99c5-7150dc805ed8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "129b8e7a-bf8b-4137-99c5-7150dc805ed8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad548681-7e0f-4496-9861-8c82eca7d3ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}