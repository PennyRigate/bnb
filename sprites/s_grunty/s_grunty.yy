{
    "id": "d0608a7b-8675-474d-9049-cbdce3e33ee9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grunty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95be070c-66b1-4e21-a356-474230589f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0608a7b-8675-474d-9049-cbdce3e33ee9",
            "compositeImage": {
                "id": "f564a317-3d05-4ed6-9786-95ded2a7db5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95be070c-66b1-4e21-a356-474230589f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c72603-29d6-4048-aff0-47d15e3ace01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95be070c-66b1-4e21-a356-474230589f57",
                    "LayerId": "e3dd7cfe-d3a2-4f08-b634-5ad801afb360"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e3dd7cfe-d3a2-4f08-b634-5ad801afb360",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0608a7b-8675-474d-9049-cbdce3e33ee9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 16
}