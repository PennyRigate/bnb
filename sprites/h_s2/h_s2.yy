{
    "id": "75a8049d-e30d-45c9-83ec-68c34422b90d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "h_s2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c11f1a6-4c81-4a4d-ad93-68715cda45a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a8049d-e30d-45c9-83ec-68c34422b90d",
            "compositeImage": {
                "id": "c67cbe1c-b0ca-47cb-8585-022fa4159821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c11f1a6-4c81-4a4d-ad93-68715cda45a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b0621a-6072-46e4-aaaa-c74b674e6b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c11f1a6-4c81-4a4d-ad93-68715cda45a2",
                    "LayerId": "8ec685b1-5502-4066-a9c2-21f40ad515a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "8ec685b1-5502-4066-a9c2-21f40ad515a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75a8049d-e30d-45c9-83ec-68c34422b90d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 0,
    "yorig": 0
}