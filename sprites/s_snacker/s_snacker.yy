{
    "id": "3bbc9713-33f2-4d8e-9a4f-a4f7e6fb0136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_snacker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "871e70a9-afa2-4544-a33c-d98968ec20cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bbc9713-33f2-4d8e-9a4f-a4f7e6fb0136",
            "compositeImage": {
                "id": "d114545a-153b-490e-8ea8-c419eb749823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "871e70a9-afa2-4544-a33c-d98968ec20cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a94f331-7368-44b9-80e0-52feb3b1ea61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "871e70a9-afa2-4544-a33c-d98968ec20cb",
                    "LayerId": "fbeb7997-97e3-41a3-8723-bed4de653ae4"
                }
            ]
        },
        {
            "id": "dc9359f8-0a0e-4619-8e18-be24f5b524db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bbc9713-33f2-4d8e-9a4f-a4f7e6fb0136",
            "compositeImage": {
                "id": "d9628835-e141-4368-a7ae-1a6732776641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9359f8-0a0e-4619-8e18-be24f5b524db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5cace5-e6c6-4f46-a80f-4d3f24b9804f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9359f8-0a0e-4619-8e18-be24f5b524db",
                    "LayerId": "fbeb7997-97e3-41a3-8723-bed4de653ae4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fbeb7997-97e3-41a3-8723-bed4de653ae4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bbc9713-33f2-4d8e-9a4f-a4f7e6fb0136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 8
}