{
    "id": "9f1976ed-a1c3-4286-9ea5-8233f504e493",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_launcher",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 6,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7037c51-a7e3-4b9a-acd3-4a93230b4870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f1976ed-a1c3-4286-9ea5-8233f504e493",
            "compositeImage": {
                "id": "377b9162-21f5-4657-88b1-ae2a26497f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7037c51-a7e3-4b9a-acd3-4a93230b4870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf01aba8-b702-447f-82f2-48b35d954094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7037c51-a7e3-4b9a-acd3-4a93230b4870",
                    "LayerId": "a856c925-26f1-48e5-92b0-f57d9ce76d57"
                }
            ]
        },
        {
            "id": "449387a9-97fc-40ef-929f-aa0aada754c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f1976ed-a1c3-4286-9ea5-8233f504e493",
            "compositeImage": {
                "id": "7f58b108-48bb-4dc3-8b52-ee07f2c2130a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449387a9-97fc-40ef-929f-aa0aada754c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd915fa4-b921-485d-acdb-bd64a437cea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449387a9-97fc-40ef-929f-aa0aada754c7",
                    "LayerId": "a856c925-26f1-48e5-92b0-f57d9ce76d57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "a856c925-26f1-48e5-92b0-f57d9ce76d57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f1976ed-a1c3-4286-9ea5-8233f504e493",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}