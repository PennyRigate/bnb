{
    "id": "c42acfeb-c52e-4140-8eeb-97b4d6cd286a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_secam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f53e2d1-619c-4d64-926e-62c7f0781633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c42acfeb-c52e-4140-8eeb-97b4d6cd286a",
            "compositeImage": {
                "id": "37784663-179e-424e-94bc-147bc8198a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f53e2d1-619c-4d64-926e-62c7f0781633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2a487f-bc10-4b1c-843b-033731a421c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f53e2d1-619c-4d64-926e-62c7f0781633",
                    "LayerId": "6f034454-e28c-489e-b970-5bae5eeed6f3"
                }
            ]
        },
        {
            "id": "8f79dc38-373a-4c56-a4a7-38f354cccc36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c42acfeb-c52e-4140-8eeb-97b4d6cd286a",
            "compositeImage": {
                "id": "7a2dc7ba-bca3-4d76-af70-3cee3cbe54cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f79dc38-373a-4c56-a4a7-38f354cccc36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8cd0a31-6624-4a3a-a18c-508bf22f1920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f79dc38-373a-4c56-a4a7-38f354cccc36",
                    "LayerId": "6f034454-e28c-489e-b970-5bae5eeed6f3"
                }
            ]
        },
        {
            "id": "b92d8b68-1ab5-4626-aa0c-a08c36137f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c42acfeb-c52e-4140-8eeb-97b4d6cd286a",
            "compositeImage": {
                "id": "175f6642-f2ee-41f4-9324-84b65e03773e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92d8b68-1ab5-4626-aa0c-a08c36137f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e81920-67e1-498e-9f47-8369cb5dbdf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92d8b68-1ab5-4626-aa0c-a08c36137f27",
                    "LayerId": "6f034454-e28c-489e-b970-5bae5eeed6f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6f034454-e28c-489e-b970-5bae5eeed6f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c42acfeb-c52e-4140-8eeb-97b4d6cd286a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}