{
    "id": "8d53cc04-089f-4d2c-a35b-717221b298d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf9ea691-2c7c-459a-80f3-8dc12dfb9331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "d5fabaf3-0e80-47b5-8600-a43a787d815a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9ea691-2c7c-459a-80f3-8dc12dfb9331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e556f20-28ad-46f7-9709-382422e4f76a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9ea691-2c7c-459a-80f3-8dc12dfb9331",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "832cfc3f-29fe-4315-b252-2a7571dccc4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "05d34af5-aa0f-415e-8756-543e890fc4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832cfc3f-29fe-4315-b252-2a7571dccc4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990b24d5-c019-4923-b5c6-26c8378ce333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832cfc3f-29fe-4315-b252-2a7571dccc4c",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "f721190d-db7a-4d7f-b367-8bcc57043f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "193edec8-c501-4d6a-be3b-471ab3d1c799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f721190d-db7a-4d7f-b367-8bcc57043f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b6bba3c-9e2f-4478-a986-762c7d6d48d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f721190d-db7a-4d7f-b367-8bcc57043f3e",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "d6d537c3-9b3d-4625-9cfd-4d52ad1b156f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "d312870b-b562-4e60-a6e5-a6553752256d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d537c3-9b3d-4625-9cfd-4d52ad1b156f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c869cbee-3dd1-464e-8461-9e244f80974e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d537c3-9b3d-4625-9cfd-4d52ad1b156f",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "083a1f3a-dfec-4e7f-9767-c4dfa72027ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "7c3a59f7-c481-4eb0-a564-28a7e930f3f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083a1f3a-dfec-4e7f-9767-c4dfa72027ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "277432bf-15b6-4157-8108-69a0e59672d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083a1f3a-dfec-4e7f-9767-c4dfa72027ea",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "4812dedd-663b-420a-9d18-d1f2b5290bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "52ec9b33-7263-48ed-9dc4-c12fc08a7891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4812dedd-663b-420a-9d18-d1f2b5290bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8641c6a3-0919-4d5c-a47c-6d4c35ff9451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4812dedd-663b-420a-9d18-d1f2b5290bcd",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "048362a9-f0fc-4e44-a0ff-2037bda57c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "83a911c8-946f-4700-b1db-2f921749e8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048362a9-f0fc-4e44-a0ff-2037bda57c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63b6ebf-aa18-4eed-9f45-32281a2cd2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048362a9-f0fc-4e44-a0ff-2037bda57c14",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        },
        {
            "id": "ec86f352-e853-4986-9d01-1b7c14ec419c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "compositeImage": {
                "id": "142605e1-cb66-4ebe-b43c-ce7afcb90907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec86f352-e853-4986-9d01-1b7c14ec419c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ebef3f-e7ee-48bf-b2da-e23dbf95a6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec86f352-e853-4986-9d01-1b7c14ec419c",
                    "LayerId": "38421578-6eb2-4d14-aa56-697e6abf756e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "38421578-6eb2-4d14-aa56-697e6abf756e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d53cc04-089f-4d2c-a35b-717221b298d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}