{
    "id": "43d550d4-5b38-4904-8850-3f3163ed6925",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_bright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e198dd7f-3f21-4c86-8bb2-bcc5c9e3eb56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "84b01ffc-6136-44c1-bfcf-51cf259f86b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e198dd7f-3f21-4c86-8bb2-bcc5c9e3eb56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4a13325-5d4e-48c6-a77c-38778b9efe5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e198dd7f-3f21-4c86-8bb2-bcc5c9e3eb56",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "49381d95-7b86-4028-a644-ef463ef2b304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "fb66d157-9cba-47bd-90ee-65987c6d78c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49381d95-7b86-4028-a644-ef463ef2b304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2abd9dfe-8a26-486c-889d-8d931c552a61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49381d95-7b86-4028-a644-ef463ef2b304",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "83e9290f-a61b-4ad5-8204-d2af8ea28701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "8619bf3c-93cc-43b2-ba9e-7a42e6626aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e9290f-a61b-4ad5-8204-d2af8ea28701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9668aaad-db36-4c6a-8826-3e8b2e3ebee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e9290f-a61b-4ad5-8204-d2af8ea28701",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "76c237d9-339b-400f-97f4-23b5acd3f7be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "bd427d09-a320-41bc-86a7-489ce6db5961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c237d9-339b-400f-97f4-23b5acd3f7be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5586dc9-cc2d-41cb-9569-de10a1502dc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c237d9-339b-400f-97f4-23b5acd3f7be",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "6816b141-5a2a-4e34-8565-35fbc36fcd4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "157af703-121e-49e7-8eea-af35f190db87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6816b141-5a2a-4e34-8565-35fbc36fcd4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a492acaf-1520-4136-9c94-90a9a44f617f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6816b141-5a2a-4e34-8565-35fbc36fcd4b",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "9eb0328c-0469-492e-8d4f-cd524524f905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "ae415485-c794-42dd-93d1-f212ecc8c1c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb0328c-0469-492e-8d4f-cd524524f905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d351ed-5316-479a-94c4-2bc6e3672972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb0328c-0469-492e-8d4f-cd524524f905",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "038a41cb-cb8f-4a16-b3d3-fe9dd0831a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "f7bdeb10-afd1-451b-ba4f-efe5df8ce845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "038a41cb-cb8f-4a16-b3d3-fe9dd0831a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a381f4d2-0599-4396-96d6-2a70a64fe9da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "038a41cb-cb8f-4a16-b3d3-fe9dd0831a69",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        },
        {
            "id": "53d3d29c-9dbd-48ad-a60e-dd7af31d2f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "compositeImage": {
                "id": "c8327a8b-7c53-4f6e-b9bd-d3d30c7147c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d3d29c-9dbd-48ad-a60e-dd7af31d2f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9848568-8a27-4924-a05f-97fb6b4c28e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d3d29c-9dbd-48ad-a60e-dd7af31d2f76",
                    "LayerId": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ebec9ebb-c1bb-400c-9a68-6cb9242352d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43d550d4-5b38-4904-8850-3f3163ed6925",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}