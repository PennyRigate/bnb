{
    "id": "1527c929-1a69-40d7-b82b-5cdaada06757",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "h_null",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1220d8f5-52b0-40e9-80c9-d66fc51e74fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1527c929-1a69-40d7-b82b-5cdaada06757",
            "compositeImage": {
                "id": "e7019793-8056-4115-b84a-23d22ae6745c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1220d8f5-52b0-40e9-80c9-d66fc51e74fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c80937-bdd3-485b-96f8-3792bf9a5819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1220d8f5-52b0-40e9-80c9-d66fc51e74fc",
                    "LayerId": "9c0ac809-b699-4de6-8a86-cc57e53efa70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c0ac809-b699-4de6-8a86-cc57e53efa70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1527c929-1a69-40d7-b82b-5cdaada06757",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}