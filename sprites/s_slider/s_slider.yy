{
    "id": "c83bef41-3a2a-4776-9115-ccd37d2089e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d755c1df-abf6-4b45-98e6-de4b299e4ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c83bef41-3a2a-4776-9115-ccd37d2089e4",
            "compositeImage": {
                "id": "26a53697-8c36-436e-98b7-2d6554502abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d755c1df-abf6-4b45-98e6-de4b299e4ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "034536de-9ba4-4f97-b136-c49d059dc17e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d755c1df-abf6-4b45-98e6-de4b299e4ae6",
                    "LayerId": "82fe176d-b3c5-4f3c-9475-f49fb8604463"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "82fe176d-b3c5-4f3c-9475-f49fb8604463",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c83bef41-3a2a-4776-9115-ccd37d2089e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}