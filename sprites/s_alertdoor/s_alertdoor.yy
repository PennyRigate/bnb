{
    "id": "35166416-f49f-44bf-8824-aa1cfaa36ee1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_alertdoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d3d5b61-b7a2-4ff7-a9bc-8b904cb44d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35166416-f49f-44bf-8824-aa1cfaa36ee1",
            "compositeImage": {
                "id": "cc625471-da8d-443f-a4da-264730dde309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d3d5b61-b7a2-4ff7-a9bc-8b904cb44d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "226cd8d0-624f-43c2-981a-ec00d6ea1439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d3d5b61-b7a2-4ff7-a9bc-8b904cb44d56",
                    "LayerId": "c8d8dbcc-840d-42ff-9684-2e1e3a297e44"
                }
            ]
        },
        {
            "id": "d30f821c-1673-4e90-a6df-73640b2c44e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35166416-f49f-44bf-8824-aa1cfaa36ee1",
            "compositeImage": {
                "id": "94d197b4-457b-4c92-95dc-849a7da086db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d30f821c-1673-4e90-a6df-73640b2c44e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d01968-7651-4811-a92c-073b79189ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d30f821c-1673-4e90-a6df-73640b2c44e8",
                    "LayerId": "c8d8dbcc-840d-42ff-9684-2e1e3a297e44"
                }
            ]
        },
        {
            "id": "7c0d8826-d3e7-4a46-87c0-ccc6d9afd845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35166416-f49f-44bf-8824-aa1cfaa36ee1",
            "compositeImage": {
                "id": "9d03a143-cfb4-4b5e-9b90-c590543640e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0d8826-d3e7-4a46-87c0-ccc6d9afd845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040d6a26-0849-463f-9f3d-3380d4e7b354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0d8826-d3e7-4a46-87c0-ccc6d9afd845",
                    "LayerId": "c8d8dbcc-840d-42ff-9684-2e1e3a297e44"
                }
            ]
        },
        {
            "id": "f6381a9a-a192-4b2d-9f62-7a23c5ee69b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35166416-f49f-44bf-8824-aa1cfaa36ee1",
            "compositeImage": {
                "id": "7b2cb62c-0dba-4593-96e3-23a3da752918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6381a9a-a192-4b2d-9f62-7a23c5ee69b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a03a339-3bc5-4036-bdc2-d8856ef3368f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6381a9a-a192-4b2d-9f62-7a23c5ee69b0",
                    "LayerId": "c8d8dbcc-840d-42ff-9684-2e1e3a297e44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c8d8dbcc-840d-42ff-9684-2e1e3a297e44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35166416-f49f-44bf-8824-aa1cfaa36ee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}