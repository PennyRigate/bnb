{
    "id": "66b6f543-be24-426e-a82b-c2778595d374",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_climb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db3edca4-a96a-4419-a333-b2a6243443ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66b6f543-be24-426e-a82b-c2778595d374",
            "compositeImage": {
                "id": "2df93ee1-cb29-41eb-866f-27de4e5a20bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3edca4-a96a-4419-a333-b2a6243443ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826920e6-ef55-476d-acff-bf41bf49e1b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3edca4-a96a-4419-a333-b2a6243443ab",
                    "LayerId": "c4675866-3062-4b40-b23f-d627eb505c7b"
                }
            ]
        },
        {
            "id": "3d6864e1-68b9-4e36-b6c7-e75d6e8a5b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66b6f543-be24-426e-a82b-c2778595d374",
            "compositeImage": {
                "id": "8525eacb-7bf5-4b77-b3ef-744f66043838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d6864e1-68b9-4e36-b6c7-e75d6e8a5b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8fba0d-ee4b-4e1c-be1a-a43d0fcc12f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d6864e1-68b9-4e36-b6c7-e75d6e8a5b66",
                    "LayerId": "c4675866-3062-4b40-b23f-d627eb505c7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c4675866-3062-4b40-b23f-d627eb505c7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66b6f543-be24-426e-a82b-c2778595d374",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}