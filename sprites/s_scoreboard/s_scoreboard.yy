{
    "id": "d379b3c1-30af-4b15-b215-02ec6e7d5522",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_scoreboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 202,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffd1f190-abef-4e4c-927f-259bf722a99e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379b3c1-30af-4b15-b215-02ec6e7d5522",
            "compositeImage": {
                "id": "8927312d-7ff9-4186-b7fa-150f78499fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd1f190-abef-4e4c-927f-259bf722a99e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcbc7e73-6f9b-46ee-b4ed-73c85c1acd38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd1f190-abef-4e4c-927f-259bf722a99e",
                    "LayerId": "6e674547-5ebc-4804-a5f5-960e8e6cfa5a"
                }
            ]
        },
        {
            "id": "3adbed0e-8201-4db1-ac00-df8e86a2eb29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379b3c1-30af-4b15-b215-02ec6e7d5522",
            "compositeImage": {
                "id": "888081f0-d596-48cd-b2eb-cce535a4f253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3adbed0e-8201-4db1-ac00-df8e86a2eb29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ab4870-8704-4fe5-8610-10fef6bf2991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3adbed0e-8201-4db1-ac00-df8e86a2eb29",
                    "LayerId": "6e674547-5ebc-4804-a5f5-960e8e6cfa5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6e674547-5ebc-4804-a5f5-960e8e6cfa5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d379b3c1-30af-4b15-b215-02ec6e7d5522",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}