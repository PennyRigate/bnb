{
    "id": "02fdf093-f093-4995-ab78-c08fbbd71351",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6acfbe29-f8d9-4e10-8006-be17b726ce5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "feb2be32-0744-4c66-b052-0ca37df8dbf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6acfbe29-f8d9-4e10-8006-be17b726ce5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52969971-9ed3-4f37-886d-4943b45f841d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6acfbe29-f8d9-4e10-8006-be17b726ce5f",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        },
        {
            "id": "ad015c9b-9730-44fc-9b3b-a44c32e4b2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "dd3635b7-88c8-4da0-8626-c9d418191dd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad015c9b-9730-44fc-9b3b-a44c32e4b2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2074372f-a97d-4c87-af27-f63ec3735e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad015c9b-9730-44fc-9b3b-a44c32e4b2d0",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        },
        {
            "id": "c698a3e6-63bc-40ae-b415-17385cd776ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "9fadc1af-fd28-468b-9677-5af8ecd62036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c698a3e6-63bc-40ae-b415-17385cd776ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "308a5643-16e4-4f87-828c-77b99eb3aa7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c698a3e6-63bc-40ae-b415-17385cd776ac",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        },
        {
            "id": "e738f1d4-1b9b-41d2-a3c3-711cfdc2965a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "d6b5daed-7fa1-4494-98fb-09554ec2844e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e738f1d4-1b9b-41d2-a3c3-711cfdc2965a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7565f7-b474-4587-9500-05f611dd81f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e738f1d4-1b9b-41d2-a3c3-711cfdc2965a",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        },
        {
            "id": "88b18f87-4a34-49a6-b47f-074aaad8b4dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "9332ddbc-3e5a-42ad-9686-a3bf4e6ca248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b18f87-4a34-49a6-b47f-074aaad8b4dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ba9f29-29d8-4f83-8908-ebfdb4bfdfb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b18f87-4a34-49a6-b47f-074aaad8b4dc",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        },
        {
            "id": "1dc61c90-6e2d-4d25-b5df-4d9995fac479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "79bf0e46-049c-407e-b16a-2939f95771d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc61c90-6e2d-4d25-b5df-4d9995fac479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f70e2f-cbbe-422f-8453-1722692e78e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc61c90-6e2d-4d25-b5df-4d9995fac479",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        },
        {
            "id": "db66e65c-48a9-472f-a6b0-7e4dd7be02c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "compositeImage": {
                "id": "13c34323-c15d-4495-a5c9-6638527fb7b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db66e65c-48a9-472f-a6b0-7e4dd7be02c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "321b495b-e4bd-494f-a329-27c331030aa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db66e65c-48a9-472f-a6b0-7e4dd7be02c9",
                    "LayerId": "f33032ce-f23a-45c2-98e1-789f1164c5f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f33032ce-f23a-45c2-98e1-789f1164c5f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02fdf093-f093-4995-ab78-c08fbbd71351",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}