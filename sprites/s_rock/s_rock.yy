{
    "id": "fde97bea-7907-4d78-a4df-0286c9675baf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bf8b89f-208a-4368-9ad6-b1f9d1884bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde97bea-7907-4d78-a4df-0286c9675baf",
            "compositeImage": {
                "id": "1b3d5b8f-c430-4ce2-b27e-dc132709f38f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf8b89f-208a-4368-9ad6-b1f9d1884bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5376accd-6144-4a55-8e36-5a5904315b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf8b89f-208a-4368-9ad6-b1f9d1884bcc",
                    "LayerId": "58fd1e75-864b-4577-bbb1-c08744005455"
                }
            ]
        },
        {
            "id": "4345a930-3ebf-4163-b45a-d547f839c367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde97bea-7907-4d78-a4df-0286c9675baf",
            "compositeImage": {
                "id": "d56171b5-1860-4ad2-a265-127150048b40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4345a930-3ebf-4163-b45a-d547f839c367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb59a7e6-7df2-462b-8bb0-d3f49dcd8f17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4345a930-3ebf-4163-b45a-d547f839c367",
                    "LayerId": "58fd1e75-864b-4577-bbb1-c08744005455"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "58fd1e75-864b-4577-bbb1-c08744005455",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fde97bea-7907-4d78-a4df-0286c9675baf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}