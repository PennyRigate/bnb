{
    "id": "41cdbe2a-099c-40b7-8cf3-a60696a05b6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_egg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78f036b4-2445-4424-be9c-6798b720724b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41cdbe2a-099c-40b7-8cf3-a60696a05b6e",
            "compositeImage": {
                "id": "597e7e49-d4f5-43c5-a86d-c9211d4b7abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f036b4-2445-4424-be9c-6798b720724b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ce43ec-bf2c-4602-a091-4572e55b6500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f036b4-2445-4424-be9c-6798b720724b",
                    "LayerId": "57fb92ae-b353-434d-bbe0-bc7435dfad40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "57fb92ae-b353-434d-bbe0-bc7435dfad40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41cdbe2a-099c-40b7-8cf3-a60696a05b6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 0
}