{
    "id": "b3391422-c97e-4e2d-b016-2be59f2b2921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_testbac",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e601dd5c-e0f2-46f9-a15d-95dd6d5b4f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3391422-c97e-4e2d-b016-2be59f2b2921",
            "compositeImage": {
                "id": "5e128952-12da-41c8-90b3-6e4609efe976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e601dd5c-e0f2-46f9-a15d-95dd6d5b4f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6ae8c1f-91d1-42d8-aba3-3e9d3b23c9a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e601dd5c-e0f2-46f9-a15d-95dd6d5b4f1d",
                    "LayerId": "6ce2df97-8576-489d-b0c0-5c3a0aa3ed19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "6ce2df97-8576-489d-b0c0-5c3a0aa3ed19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3391422-c97e-4e2d-b016-2be59f2b2921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 0,
    "yorig": 0
}