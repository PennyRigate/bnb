{
    "id": "3be1791f-636f-4f33-b2fd-543aab323c59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_meg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ceed1bb-7c2a-462a-b200-8ef028987eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be1791f-636f-4f33-b2fd-543aab323c59",
            "compositeImage": {
                "id": "b7464048-3a6f-4b56-841d-98e4451656e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ceed1bb-7c2a-462a-b200-8ef028987eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09faef44-6484-4ebe-9cbd-6a0eece989fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ceed1bb-7c2a-462a-b200-8ef028987eba",
                    "LayerId": "13d310ee-d91a-4971-a25c-ee4a2a00aa62"
                }
            ]
        },
        {
            "id": "881f0c64-bfa2-4045-bf3d-ccfa847e7dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be1791f-636f-4f33-b2fd-543aab323c59",
            "compositeImage": {
                "id": "29d93e32-e56d-443e-abd2-42f1c2c1936d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "881f0c64-bfa2-4045-bf3d-ccfa847e7dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf17d9f-9c98-4178-8b36-b345c22446fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881f0c64-bfa2-4045-bf3d-ccfa847e7dfa",
                    "LayerId": "13d310ee-d91a-4971-a25c-ee4a2a00aa62"
                }
            ]
        },
        {
            "id": "9d1a2116-0c50-40db-8351-3c4e64611f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be1791f-636f-4f33-b2fd-543aab323c59",
            "compositeImage": {
                "id": "79a529ad-b509-4e59-a408-5f09aef817d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d1a2116-0c50-40db-8351-3c4e64611f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa1631b-ec4a-4a4f-b70f-eb4c5f7f1bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d1a2116-0c50-40db-8351-3c4e64611f8e",
                    "LayerId": "13d310ee-d91a-4971-a25c-ee4a2a00aa62"
                }
            ]
        },
        {
            "id": "340985e3-ae77-433c-bc41-f4340ea53091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be1791f-636f-4f33-b2fd-543aab323c59",
            "compositeImage": {
                "id": "ad4488da-8064-4465-928d-39193888a674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "340985e3-ae77-433c-bc41-f4340ea53091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19aafdd7-bf0b-4ff1-b1f6-d2dfb0d08f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "340985e3-ae77-433c-bc41-f4340ea53091",
                    "LayerId": "13d310ee-d91a-4971-a25c-ee4a2a00aa62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13d310ee-d91a-4971-a25c-ee4a2a00aa62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3be1791f-636f-4f33-b2fd-543aab323c59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}