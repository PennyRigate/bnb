{
    "id": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bottles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df276630-6cc0-4a67-98b6-ab2c1023cf9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "24995bac-6113-4c93-b3d4-ab119b10e539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df276630-6cc0-4a67-98b6-ab2c1023cf9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "022654d8-a4c0-4746-8c94-14f9a7e1bbd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df276630-6cc0-4a67-98b6-ab2c1023cf9c",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        },
        {
            "id": "62a27dd2-46ef-4ee8-8a1e-5e7060e8d05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "2334bb13-08de-4f6e-a4bd-543fd00c6a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a27dd2-46ef-4ee8-8a1e-5e7060e8d05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7e1ea7-e0c3-46f4-abba-cccffd514f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a27dd2-46ef-4ee8-8a1e-5e7060e8d05d",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        },
        {
            "id": "0711bbe2-44c6-4433-a2a8-091b7f7c9fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "e49f9ae4-b854-4f85-a375-c4c440020ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0711bbe2-44c6-4433-a2a8-091b7f7c9fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f624d9-7842-4c3b-9c6b-f25ee19a1fc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0711bbe2-44c6-4433-a2a8-091b7f7c9fb6",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        },
        {
            "id": "6c50e929-ee4a-4f9b-bf7b-86e7952c367e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "fef82484-acad-41e1-ba29-dae600b10265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c50e929-ee4a-4f9b-bf7b-86e7952c367e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b1f44b-fdbb-4387-903e-416c39bb8b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c50e929-ee4a-4f9b-bf7b-86e7952c367e",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        },
        {
            "id": "53ab821d-12c3-4c29-bd0d-2c810d694b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "6ad45a6c-5531-4cdc-8c40-18b89c88553f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ab821d-12c3-4c29-bd0d-2c810d694b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f000a513-0166-4297-a1d5-be979cb9c85e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ab821d-12c3-4c29-bd0d-2c810d694b88",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        },
        {
            "id": "6dee6611-5325-45fb-884b-78f102c53b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "d8da7dc2-9dd3-44e8-ad9e-a93b57f50010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dee6611-5325-45fb-884b-78f102c53b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e8fd3e1-c39c-46d0-b255-0e6c9a1d93f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dee6611-5325-45fb-884b-78f102c53b31",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        },
        {
            "id": "67aa5a52-22e1-4a2a-a6ed-41846330dad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "compositeImage": {
                "id": "bad464f8-0017-4b5d-a92c-f1d7e4d8c4ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67aa5a52-22e1-4a2a-a6ed-41846330dad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d8960f0-7af2-41fd-90a8-8a026308ed67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67aa5a52-22e1-4a2a-a6ed-41846330dad0",
                    "LayerId": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "ea6e3209-4752-4b2f-9ec2-513bf9fa4862",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b84cdd44-9152-4d8c-a9bb-f5d6360e98d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}