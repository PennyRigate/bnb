{
    "id": "a56b056d-bc17-4b1e-a7d4-b5479e456425",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_notedoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebbc0199-c86d-4f2a-bdf5-6844261d5f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a56b056d-bc17-4b1e-a7d4-b5479e456425",
            "compositeImage": {
                "id": "25714650-648d-40a8-9243-615466aff4cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbc0199-c86d-4f2a-bdf5-6844261d5f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "372f1bf8-a557-4a9b-ad0f-5a55342e3a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbc0199-c86d-4f2a-bdf5-6844261d5f55",
                    "LayerId": "fa63343f-5f88-49d8-8088-026762f4895f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fa63343f-5f88-49d8-8088-026762f4895f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a56b056d-bc17-4b1e-a7d4-b5479e456425",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}