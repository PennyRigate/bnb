{
    "id": "517ea468-7efe-460c-8007-526c7f32a3bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_snacker_jig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "025ca185-fa5b-4a17-9918-be74c42fd03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "517ea468-7efe-460c-8007-526c7f32a3bb",
            "compositeImage": {
                "id": "27e8a78a-12c1-45d3-b08d-d73910c13d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025ca185-fa5b-4a17-9918-be74c42fd03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361921fa-bc1e-4a6d-b2ed-af3f2a4206c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025ca185-fa5b-4a17-9918-be74c42fd03a",
                    "LayerId": "ff257793-91eb-4049-a771-16a3939adf12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff257793-91eb-4049-a771-16a3939adf12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "517ea468-7efe-460c-8007-526c7f32a3bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 32
}