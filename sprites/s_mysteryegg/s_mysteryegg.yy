{
    "id": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mysteryegg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "096e3360-f814-4503-a9dd-7f91260f0527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
            "compositeImage": {
                "id": "5335e1c8-0093-4c09-b463-fc9ee3efabe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "096e3360-f814-4503-a9dd-7f91260f0527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "579d26c5-c0c5-444b-bed8-116017a4c52e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "096e3360-f814-4503-a9dd-7f91260f0527",
                    "LayerId": "e2df2c7b-caa6-4390-8177-d8c48d2faafb"
                }
            ]
        },
        {
            "id": "761d8aeb-e287-4f0d-a75b-0dcfefb518d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
            "compositeImage": {
                "id": "339602bf-5a70-4455-8d50-986eef2ade4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "761d8aeb-e287-4f0d-a75b-0dcfefb518d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06a6087-4c50-4baa-9540-f3e963425c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "761d8aeb-e287-4f0d-a75b-0dcfefb518d6",
                    "LayerId": "e2df2c7b-caa6-4390-8177-d8c48d2faafb"
                }
            ]
        },
        {
            "id": "d9f706e1-5c18-4b49-87d1-98bf8c7fec6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
            "compositeImage": {
                "id": "2ac37eca-bf6b-4919-a522-a75ac2d591c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f706e1-5c18-4b49-87d1-98bf8c7fec6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f1d57b-01e3-4ebb-a865-0d228b9a9708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f706e1-5c18-4b49-87d1-98bf8c7fec6a",
                    "LayerId": "e2df2c7b-caa6-4390-8177-d8c48d2faafb"
                }
            ]
        },
        {
            "id": "e2fb82a2-1e0a-45af-8698-b00ab68fc1d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
            "compositeImage": {
                "id": "fc231383-a537-44e6-b00a-e960ab015c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2fb82a2-1e0a-45af-8698-b00ab68fc1d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a38484-0dd8-4e67-b37c-3684e3af8fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2fb82a2-1e0a-45af-8698-b00ab68fc1d1",
                    "LayerId": "e2df2c7b-caa6-4390-8177-d8c48d2faafb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e2df2c7b-caa6-4390-8177-d8c48d2faafb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a8a15e3-5d8a-4e2d-8608-da26ecf7ac12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}