{
    "id": "f7bbf549-1007-4d54-957d-b6e85f17fcf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_jiggy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5706684-e986-4329-9dd1-72473f5b6daa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bbf549-1007-4d54-957d-b6e85f17fcf3",
            "compositeImage": {
                "id": "3fdfeaf7-f8c4-4bf3-b2e4-f0617d66dd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5706684-e986-4329-9dd1-72473f5b6daa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f4528f-3325-4f85-8540-a6994b721a0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5706684-e986-4329-9dd1-72473f5b6daa",
                    "LayerId": "3af4a519-6b37-41d8-bc69-296759215cee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3af4a519-6b37-41d8-bc69-296759215cee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7bbf549-1007-4d54-957d-b6e85f17fcf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}