{
    "id": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d80f4e3-e2ea-4f7f-bfd1-b03cf9f72f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "cba64792-edbc-49e9-b488-13f5d9835417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d80f4e3-e2ea-4f7f-bfd1-b03cf9f72f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97a2054-785f-4a18-b0e0-836c63a6b65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d80f4e3-e2ea-4f7f-bfd1-b03cf9f72f6b",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "c92dd1ad-cdab-4c13-bc89-7e8c2b97f40a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "d9a8fa5c-bcd8-447f-87e0-252d969bbf8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92dd1ad-cdab-4c13-bc89-7e8c2b97f40a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c727fd2-8ea4-4cd8-8cde-c2c89d90bf5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92dd1ad-cdab-4c13-bc89-7e8c2b97f40a",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "927802b3-4f9b-4e21-a5b7-fa1f4729955a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "cb455ed5-1b47-4dab-a2d2-5730d8ce4ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927802b3-4f9b-4e21-a5b7-fa1f4729955a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e6e4e39-86f3-4b66-b2d6-d0c69acc264c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927802b3-4f9b-4e21-a5b7-fa1f4729955a",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "da9e9965-2021-4811-9620-8cf1d133c5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "bdecefd2-7dc0-42ba-9d69-1f3f1cfce63b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9e9965-2021-4811-9620-8cf1d133c5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71325c7f-fdd5-4ca7-ac01-4c1efc4e465b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9e9965-2021-4811-9620-8cf1d133c5df",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "a79679de-2a64-412d-9b5b-f7323a60f8a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "528b7519-18a2-4971-bcea-ca9d20f6a14e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79679de-2a64-412d-9b5b-f7323a60f8a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccecce93-41f3-48fa-aeeb-8f23efa3f106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79679de-2a64-412d-9b5b-f7323a60f8a9",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "df7106f1-7018-463a-94bc-b790670bbc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "b7e96990-a596-4031-838d-b4b834e51cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df7106f1-7018-463a-94bc-b790670bbc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35915a97-d899-4bee-812c-57f065d1dc91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df7106f1-7018-463a-94bc-b790670bbc20",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "a32ddb81-f96a-46e0-843b-7d2a8e8fa2fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "06e69aa7-b31e-4c88-b0f2-d4e786aa8734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32ddb81-f96a-46e0-843b-7d2a8e8fa2fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2cbc14-1873-4adc-bf2e-d2455ee3f491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32ddb81-f96a-46e0-843b-7d2a8e8fa2fa",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "6029fb6e-1709-4c8f-a15f-9657d47ac18d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "decea726-34af-4cbf-89bd-f8f91a149f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6029fb6e-1709-4c8f-a15f-9657d47ac18d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373dc645-aaa2-49e6-bdd7-bd37740275f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6029fb6e-1709-4c8f-a15f-9657d47ac18d",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "cfc1f178-6967-4193-b1b4-336dccbc17cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "0947412d-b68d-4b01-853f-3582a4e61380",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfc1f178-6967-4193-b1b4-336dccbc17cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39282d84-ecdb-4ca2-99b5-d59ab5708adf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfc1f178-6967-4193-b1b4-336dccbc17cb",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "1b66277e-930e-4b8c-9761-e96002d0a9f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "670883d2-1033-4124-b636-e2b8512cc845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b66277e-930e-4b8c-9761-e96002d0a9f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd2e4c0-84f4-489c-95d0-cdb9f37792f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b66277e-930e-4b8c-9761-e96002d0a9f1",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "ed672419-7ccb-40e0-873e-050c6b0c958b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "5a9d1009-0c9e-435d-bb2f-a122ac752a22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed672419-7ccb-40e0-873e-050c6b0c958b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1861c75a-8a5b-4b6a-aae3-13bd95a140a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed672419-7ccb-40e0-873e-050c6b0c958b",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "44b3e0a9-fe4c-48f3-a59a-6e0a529b6fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "b54f716b-60ab-4e49-9b3a-418833dcbb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b3e0a9-fe4c-48f3-a59a-6e0a529b6fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1485b8f7-f989-4f2f-a8e6-31d91f28ef3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b3e0a9-fe4c-48f3-a59a-6e0a529b6fb6",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "5edb45c6-7d32-4055-8f40-4f69fa64bd4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "81565ef9-3ce4-4d49-acd4-c35ec93fe75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5edb45c6-7d32-4055-8f40-4f69fa64bd4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab76b9f-bae8-4766-a616-9c1da3034cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5edb45c6-7d32-4055-8f40-4f69fa64bd4c",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "fe52be17-20c9-413e-a89e-1de435906b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "703415d4-8709-4ff2-9b51-435d2bcf6b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe52be17-20c9-413e-a89e-1de435906b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf46b5a1-1bd1-4bb4-9856-8c8991ffd1a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe52be17-20c9-413e-a89e-1de435906b6d",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "81ac1b65-7085-45f8-9857-618a00c41376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "a73be5c7-a09b-4f32-b379-04aee9b2fa82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ac1b65-7085-45f8-9857-618a00c41376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "625a1764-b120-437b-b0df-a8455970a2e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ac1b65-7085-45f8-9857-618a00c41376",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        },
        {
            "id": "50534455-bdae-42c8-93fd-ec5648adc4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "compositeImage": {
                "id": "b4718a74-6b1d-462e-bfa3-f420df3e9734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50534455-bdae-42c8-93fd-ec5648adc4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56bb9139-0573-4f51-a713-43ea765cf077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50534455-bdae-42c8-93fd-ec5648adc4ee",
                    "LayerId": "e34d3605-a58a-4b64-a2dd-6ae4919c281c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e34d3605-a58a-4b64-a2dd-6ae4919c281c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd9a7baa-b0f8-4d8f-9138-7422c8552f26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}