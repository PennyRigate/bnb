{
    "id": "9862193e-749e-4088-8ecc-def3d752df35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_testpattern",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fc3a202-de24-479e-b31c-2b3ec6030279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9862193e-749e-4088-8ecc-def3d752df35",
            "compositeImage": {
                "id": "ef27e3cc-5147-4629-bbe4-c5475df0328f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc3a202-de24-479e-b31c-2b3ec6030279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29030378-7d40-418e-8ebf-ba8670c0f0c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc3a202-de24-479e-b31c-2b3ec6030279",
                    "LayerId": "33586de5-1fd0-43ad-a889-542cfb11855b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "33586de5-1fd0-43ad-a889-542cfb11855b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9862193e-749e-4088-8ecc-def3d752df35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}