{
    "id": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_jinjos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 216,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6165ecf5-a2bd-4aea-ad2d-c92383fefe7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
            "compositeImage": {
                "id": "6969ebc9-08ae-45bd-aa9b-ea3e7c8db74b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6165ecf5-a2bd-4aea-ad2d-c92383fefe7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e9039c3-84a5-4d9f-92ca-26d6956b6526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6165ecf5-a2bd-4aea-ad2d-c92383fefe7e",
                    "LayerId": "39f8a568-9ee7-4de1-8b7c-e229f7753335"
                }
            ]
        },
        {
            "id": "017bca68-f4d9-49da-b2f3-7954ecb7d99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
            "compositeImage": {
                "id": "beecee7b-486e-49b0-b864-513aaf393bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "017bca68-f4d9-49da-b2f3-7954ecb7d99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d2a917-b77d-4cf4-8c76-41d9f9cfcbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "017bca68-f4d9-49da-b2f3-7954ecb7d99b",
                    "LayerId": "39f8a568-9ee7-4de1-8b7c-e229f7753335"
                }
            ]
        },
        {
            "id": "5708c772-b96f-46fa-bdf8-3cd574a3da0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
            "compositeImage": {
                "id": "f48c73fc-f201-4e4a-9b92-eb291ada0fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5708c772-b96f-46fa-bdf8-3cd574a3da0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3af7bc-ce33-414e-834f-dd2b3e4fcc21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5708c772-b96f-46fa-bdf8-3cd574a3da0d",
                    "LayerId": "39f8a568-9ee7-4de1-8b7c-e229f7753335"
                }
            ]
        },
        {
            "id": "1a27b6b1-dc4d-4600-a584-7af6b9967939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
            "compositeImage": {
                "id": "b2ac5c37-3a9e-4b00-ad8f-7beb2f78793b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a27b6b1-dc4d-4600-a584-7af6b9967939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c629740-39bd-43e2-8388-80f2645e1fb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a27b6b1-dc4d-4600-a584-7af6b9967939",
                    "LayerId": "39f8a568-9ee7-4de1-8b7c-e229f7753335"
                }
            ]
        },
        {
            "id": "c965183a-933a-4898-bdb6-4b7a35675e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
            "compositeImage": {
                "id": "a5b0854a-9227-4f74-b7c8-9b5ddaf557eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c965183a-933a-4898-bdb6-4b7a35675e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd505cb-2bc2-4640-b487-e7731f113b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c965183a-933a-4898-bdb6-4b7a35675e0b",
                    "LayerId": "39f8a568-9ee7-4de1-8b7c-e229f7753335"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "39f8a568-9ee7-4de1-8b7c-e229f7753335",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b74eac6d-eda7-4c65-bdd4-e6521e71c0a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}