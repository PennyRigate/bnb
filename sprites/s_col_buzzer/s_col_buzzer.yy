{
    "id": "6ffea6aa-55c5-4c05-a9c2-8b3097b8fe9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_col_buzzer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 8,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9ac4ff8-842f-47ae-bc48-ad613debb841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ffea6aa-55c5-4c05-a9c2-8b3097b8fe9f",
            "compositeImage": {
                "id": "5ec197ff-8026-4289-b0f0-284a2b4ab0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ac4ff8-842f-47ae-bc48-ad613debb841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a2c76b8-a776-4499-bcd2-29e03384bf12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ac4ff8-842f-47ae-bc48-ad613debb841",
                    "LayerId": "4567156f-4b44-45df-970e-bbd0b951d396"
                }
            ]
        },
        {
            "id": "866831ff-5ded-4037-9345-6689f3c63cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ffea6aa-55c5-4c05-a9c2-8b3097b8fe9f",
            "compositeImage": {
                "id": "91a6a6f7-81af-48ae-a9a8-33ed76fae1be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866831ff-5ded-4037-9345-6689f3c63cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0a5603-4955-4f4b-b9fb-862ba44dd55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866831ff-5ded-4037-9345-6689f3c63cbe",
                    "LayerId": "4567156f-4b44-45df-970e-bbd0b951d396"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4567156f-4b44-45df-970e-bbd0b951d396",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ffea6aa-55c5-4c05-a9c2-8b3097b8fe9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 8
}