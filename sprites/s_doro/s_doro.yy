{
    "id": "d2ac0282-dfb4-48d1-b815-d1b6d415d4b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_doro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24cbf4a0-1ee1-421d-bcce-520a9d35b84a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ac0282-dfb4-48d1-b815-d1b6d415d4b8",
            "compositeImage": {
                "id": "c3c89154-2775-4c63-8c8c-1cf419490c9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cbf4a0-1ee1-421d-bcce-520a9d35b84a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f349d8ab-1dfb-47e8-bc5c-e1d1c6535512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cbf4a0-1ee1-421d-bcce-520a9d35b84a",
                    "LayerId": "bc38fd44-93e5-46ff-a504-7a40bf352982"
                }
            ]
        },
        {
            "id": "b1b0e0f1-c4e3-47a3-9c19-487717e55606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ac0282-dfb4-48d1-b815-d1b6d415d4b8",
            "compositeImage": {
                "id": "aec21d5e-085b-46c0-b49e-7ee9003117fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b0e0f1-c4e3-47a3-9c19-487717e55606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43c2892e-6b07-4b2b-9443-8097555b4ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b0e0f1-c4e3-47a3-9c19-487717e55606",
                    "LayerId": "bc38fd44-93e5-46ff-a504-7a40bf352982"
                }
            ]
        },
        {
            "id": "8b0ba9ce-33af-4b18-8e3e-7597cd99f4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ac0282-dfb4-48d1-b815-d1b6d415d4b8",
            "compositeImage": {
                "id": "acc1c526-22b4-4e37-9cda-6ad1a1cb7106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0ba9ce-33af-4b18-8e3e-7597cd99f4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812b2c4e-6b7d-4700-a10e-14041ee3067d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0ba9ce-33af-4b18-8e3e-7597cd99f4ad",
                    "LayerId": "bc38fd44-93e5-46ff-a504-7a40bf352982"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bc38fd44-93e5-46ff-a504-7a40bf352982",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2ac0282-dfb4-48d1-b815-d1b6d415d4b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}