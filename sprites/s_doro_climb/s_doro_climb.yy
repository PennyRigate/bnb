{
    "id": "213b487c-e424-47f1-a270-ba9fdc29bf52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_doro_climb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f6ead2a-514f-4f75-9a08-0255af5ad5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213b487c-e424-47f1-a270-ba9fdc29bf52",
            "compositeImage": {
                "id": "34081d48-4a27-4f3a-b83f-197af0f62069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f6ead2a-514f-4f75-9a08-0255af5ad5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d473a96-deb2-4854-91a8-0625874a37b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f6ead2a-514f-4f75-9a08-0255af5ad5bd",
                    "LayerId": "33855255-8711-4df9-8d9e-082a61bfcbf8"
                }
            ]
        },
        {
            "id": "1632cf5e-8a35-40a4-984b-ecf784218e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213b487c-e424-47f1-a270-ba9fdc29bf52",
            "compositeImage": {
                "id": "df32c55b-78d6-4749-b15a-680ee8ec0835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1632cf5e-8a35-40a4-984b-ecf784218e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "641d2633-79b1-4933-8699-f7776e549e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1632cf5e-8a35-40a4-984b-ecf784218e70",
                    "LayerId": "33855255-8711-4df9-8d9e-082a61bfcbf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "33855255-8711-4df9-8d9e-082a61bfcbf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "213b487c-e424-47f1-a270-ba9fdc29bf52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}