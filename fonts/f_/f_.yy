{
    "id": "6ea953f8-9a81-4578-b257-8458c274ea8a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ZX Spectrum 7",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "148748f4-1d17-4978-a3db-f672218656e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "eba01f8f-2732-4a93-b9a9-16b7918e5b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 1,
                "x": 217,
                "y": 25
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e08096cf-8d61-4360-a36e-62f8b9e20ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 211,
                "y": 25
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "71ce936c-b98b-4e5a-b3e7-8460719c7c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 203,
                "y": 25
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3c132979-2aba-4a8e-b659-c6c5cfcf1caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 196,
                "y": 25
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bb6b0cfd-9f6b-4515-9e71-18238a5115d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 188,
                "y": 25
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "17e48b6f-c5dc-41ca-8e71-0f74988ea3a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 180,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a4038e47-f8d5-4ab1-9aa7-504ad9faa80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 176,
                "y": 25
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e59d08b2-e60f-4f97-b7be-e4390f2f04c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 2,
                "x": 172,
                "y": 25
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3685b76a-6acd-4339-828a-a0087a1089b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 168,
                "y": 25
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c6369f5b-b248-4391-8b32-811728ab22ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1eedfc25-a879-4350-9179-291cc3cd6b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 161,
                "y": 25
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f40e91d8-20e9-4471-9f00-885125a9f82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 149,
                "y": 25
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "747d708a-cd2e-4228-8328-e76b9f1c8179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 142,
                "y": 25
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "93b4c4f1-6135-4a22-a37b-a1834771e334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 25
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bef61bbf-5153-4b82-9b14-1976b0dfba4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 131,
                "y": 25
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "609d6432-edb0-48f7-abfe-e2f4678f464f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 123,
                "y": 25
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "27717bca-350c-42a9-99b2-2c3c430c1e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 116,
                "y": 25
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9ef51157-7c3a-43c2-8950-7deeb86940c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 25
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c74fdd12-4fde-4fab-bb58-04205b5f8784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 100,
                "y": 25
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "26f58b81-5636-4dfc-9b32-2eca07f65ebe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 25
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "79dbeca0-2c72-4d94-bda4-147167a19dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 153,
                "y": 25
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "36540392-d3a4-4fa8-9991-f1396afa3f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 227,
                "y": 25
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4e33f2a1-0afa-404e-b021-4db7dbd23743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 235,
                "y": 25
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8ed3b098-6b50-42d4-889f-eb5f1a0afdd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 243,
                "y": 25
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9a2a50c5-16b5-4cac-a138-f35de1e4bec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "fa848906-5e60-484f-8680-9ef867bea7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 1,
                "x": 142,
                "y": 48
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "276a4c0f-533b-46d8-91be-bc6762177375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 48
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "61e71346-2b3b-4fd3-afa2-7b75257d9eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 133,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8cc3f38c-a318-4e66-83db-54e455f748be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 126,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3adc5dd9-b7f1-429e-a437-20f51c19da32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 121,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b6224280-324d-409b-9544-63ddaa3204d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 113,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d4a680a8-de16-445f-8e1e-e5a2ec10a048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 105,
                "y": 48
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f2ec7854-69f6-4014-8824-c235bf68cd95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "19252119-ab4a-4916-81ad-ca72901da07d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 89,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fbcdd839-de32-46ef-9d8b-e6ca3dfe77dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 81,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "62878244-dc7e-4f53-80a8-54d525db7200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 73,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c6ed106d-eafd-4da7-9ac3-3f5320cb922f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 65,
                "y": 48
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3376960c-0c32-4f5d-a9e3-a865571809bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 57,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "03f2872a-fe47-4662-adb8-2bd682e8324a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 49,
                "y": 48
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "23ca4ad1-392c-401b-9fcb-0f7f0e4fc4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 41,
                "y": 48
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "22ab7185-3b06-45b2-aa5b-d490473eb29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 34,
                "y": 48
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ec5c3f3d-e7e6-4ecb-8e57-a77bf61daad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5f49038c-938e-4eb0-87a8-8de4e095bde2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "95f11678-c968-4703-83a5-a32a5d5b08c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "23a9bbe1-a0e8-4ad5-a1b2-f124b25a8b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d6b57647-11a6-4692-a48d-1dc280937605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 84,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2896a393-d6e3-413c-aa7c-ee7d6856f56f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 76,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dcbb5686-72ad-44b4-8a99-9e38f18b09ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0c239c5d-1b97-4057-a12e-5848c33442c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "19cde228-96a0-49f8-82b0-d90b05dd4e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dfaf0b41-3f38-49c7-ae69-08e393859943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f0d14908-a0c8-4a05-ad8a-70fcb9d7a11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c1f6054c-196a-44ce-a072-15ee135824ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8ff3ab46-1231-4f7f-8267-75259803e9bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0a2c7dd3-b0a9-4b4a-b581-00f0e4e4756d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9b6a132b-51f3-4fc4-abc1-b646636e88bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "af25ea64-1242-4130-aa5c-46aa0dc0eadd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4211c76c-11f2-420d-b361-9069b12bca57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a55568ba-6511-498c-8b00-886bb658854f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 3,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9572551c-7f03-4683-a22e-34a2c26570ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "623ba44a-f572-47aa-a0e7-eef814102c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 3,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "881533f0-db47-408f-8d40-5e61ee3d1831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fea434cc-a9c7-4681-b6f7-551e8fdc94c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0b475d22-9480-4e2c-864e-0003395ece35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e7a23a12-4518-4c8f-b174-62d54d99e9e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bb0986b0-bb2f-43e2-baa8-c988cc1b1ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b21f206d-37d3-4259-ae10-a0ee17759344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "05d75545-0a4b-459c-96d4-b4163de02923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fc485a03-4256-452d-8baa-911ca915e99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ca195fc6-235f-46b7-bd9b-6bb4b1fb6a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6739dbc3-8e29-4d8f-afb3-d8827396520b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "33cbe702-e322-4282-a826-6ff12ed641a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6e6d266f-c7d4-46ef-a196-49b2c136a522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7a1522ec-2d90-4fb4-a4bd-a8adbe518048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 55,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "775aeb3c-6a14-478a-bddd-e0761008ad12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 49,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f773a7d4-5851-4c66-bbd9-a611b7e695a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 44,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "27339e02-b1cd-412f-b3af-151bfed996d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 37,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "38bfc65f-9420-4f5c-b8cd-7a32a64751f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 30,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6a45c0d7-b7a7-4580-8dc7-fd20b9b3364e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 23,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "183113d4-0fa5-4508-b2f7-6d5ca55f3a4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 16,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a005e27a-6ba1-465d-99df-9df3fa23dcb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 8,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c24c797a-436f-45a3-a18e-643a8edf7268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c061f1a7-3d83-4402-90c0-965175e7822f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 61,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "91bfedb8-85fe-4642-b93b-ffca6c861300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8155e920-a5f7-4ca3-a7c4-76859349e58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1a884a9e-5185-4e45-ab48-3c5e7a027aa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d7c1feb9-b399-449d-b9bb-b8b4229fcea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "47dc7d55-f675-4f5e-8216-ef488c3e6c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "eee5feb5-69c2-4aae-afed-4967a8c91dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "00d8041c-f0e8-4772-aa77-3e855515cd69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e3f405dd-ea02-44fc-babe-388dcbd2401e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "22e4b610-3df1-4b93-aae1-e4953ede27b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 1,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a64331f5-5f6c-44a4-b300-f2578ffe873c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d649f179-5977-436f-bb63-120b645bd643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 153,
                "y": 48
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c3085c5f-2363-4f2e-92fa-34ccdb22129f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 159,
                "y": 48
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}