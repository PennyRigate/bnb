{
    "id": "6ea953f8-9a81-4578-b257-8458c274ea8a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ZX Spectrum 7",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "baf40231-2d49-41ba-9bb4-38395dcd9df4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b1a179b9-971b-430f-b018-fe50e5f46abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 1,
                "x": 217,
                "y": 25
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "42337a21-6d72-4b83-a54b-86f7e0c569f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 211,
                "y": 25
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "103d8333-ecfd-4927-bbcb-b588b79ca725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 203,
                "y": 25
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9dfc48a0-1375-4bf3-a101-b8b493627703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 196,
                "y": 25
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6a7df4eb-ac2f-40e8-b9c7-50664b825092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 188,
                "y": 25
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b94842ce-2664-4653-900e-d6d3d7e07f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 180,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "aa5a73f4-344c-43d6-afc8-bd2fee67d376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 176,
                "y": 25
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7c7608a5-b65a-400e-b132-541498223b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 2,
                "x": 172,
                "y": 25
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c9c1c8f6-847a-4f87-84e3-bf02689461fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 168,
                "y": 25
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d673f4aa-c6c6-4098-a7cb-d55afce67f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c411fc7d-8163-4869-9aae-8ec6ab11618f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 161,
                "y": 25
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "42fd6ae4-f267-404d-aa54-63942b7906c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 149,
                "y": 25
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9e213a5d-0d06-445a-b618-b6f5171bc7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 142,
                "y": 25
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e34518f2-50ca-47c2-a97a-355db5b29193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 25
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4314793e-2398-477a-9c36-d308fb576826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 131,
                "y": 25
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b4d58526-b7bb-46d5-a540-f87220f4cecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 123,
                "y": 25
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "433a40fa-1b34-4999-a931-7dd32c747c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 116,
                "y": 25
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c20989c4-7c62-4cd6-a97b-a53ddb389aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 25
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "49b69165-7960-41c4-8596-21f0adcf84a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 100,
                "y": 25
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6ebb1bca-33ef-4a5a-a4fe-b22ab28ee14c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 25
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dd9fe0fd-456c-45c2-94b4-a5b3d7992024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 153,
                "y": 25
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8546e158-c38e-4242-9b73-7de263f20b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 227,
                "y": 25
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "31481285-e9d1-4caf-9d4c-878f165538a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 235,
                "y": 25
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e09eced4-1248-4e0d-a824-36f344f0c049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 243,
                "y": 25
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "16a108de-99b7-4cf8-8811-b1cb75eac40c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cc013a2c-e1c3-42f8-a7dc-5a785f393223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 1,
                "x": 142,
                "y": 48
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "91996b0a-d1de-4321-8ae7-7f4bbda86f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 48
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8494a7d7-078b-41f9-899b-3c316d11fe72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 133,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "009bc097-4bbe-43bd-81a3-54994e0cc6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 126,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "36f99d8b-672f-4bee-86ab-505d801c744c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 121,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e65da05b-1b05-403b-bca5-1e014d11f6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 113,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "632fbcbe-8779-4b3e-800a-c5c893aeaf7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 105,
                "y": 48
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9b2e0ca5-b540-48be-9782-e82e0a6af80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8d42d4cf-7152-469c-9c3a-54d7eb14f420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 89,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "eca8beab-3297-4fda-9096-b63e4a58e449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 81,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5439d4e4-b3c2-4615-a68a-02186cc2d3f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 73,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "624eb26a-39b6-4d16-b76f-15e03d98ca13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 65,
                "y": 48
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e49df61e-5753-4861-a50c-a063836d1186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 57,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2e406241-f48c-4b54-a4c9-07ce6ed9a1da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 49,
                "y": 48
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f627013a-5ff4-4dff-8303-fc5a827bf7c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 41,
                "y": 48
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c0bd7984-99d5-4a20-9d4e-f18e17199d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 34,
                "y": 48
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e9c64f88-2162-42df-adea-b98d9cde056d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9b80f95c-e9d3-4639-8a68-5d36657fb339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e0b53bdb-0864-4f14-b035-232e3ccbf8c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a82d3940-5b07-4d1e-a769-7c2a6531fabb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "79d24576-babb-4c77-872d-a5f41f15836e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 84,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5bcfcc0b-68ad-4895-93c9-b73645b5fd6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 76,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5ad95a8d-9fd3-4ea6-a032-90ce994f748a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "532bd383-7382-4c0c-8acc-cff86ec8a95a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d4c918d5-85cb-4c07-840a-c3c8579da81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "25be77e8-6feb-486e-b44e-183f11d77414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f70adf6b-3343-48ab-a01c-8a8a4378f5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dd08b37e-0eb0-4a30-a94e-21e4b813c5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "41ec2df1-58c3-4a86-81e9-3059f07b4f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0d8a5681-6352-4bc1-a27f-5bcb6be455cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a0f4b236-8170-496a-a7cf-d840fc9d0f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0cbbd126-3df0-42a8-9819-f7110eae9d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "601eb22c-0737-41cd-8535-ced917137977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0de21ab2-ac8e-4d57-aafc-858f2bb93092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 3,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7adcfbad-1ce5-4458-8eef-d08b799d2ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a988e9a3-ad70-4e5d-b77e-df28665887de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 3,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "137973bd-965d-4e9f-b594-35ffb3b993d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9da25364-4fbe-439f-80af-e75aa5577b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9afd9896-50e2-4894-bdf2-bf5b578b3b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e9799e06-4116-4ba7-a977-5979ac9c2f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f6882de7-f5f6-4140-992d-0606d4b2f5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "17cd05fc-8b64-4b0a-b5e3-68bad49edcfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fef1ec74-bcb9-4ac5-b2a9-0febb010bfab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cac2a6c2-ad4a-4886-8dc5-4ce1a7e8cc70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7d83b7a4-b36e-4a38-bb28-fdd69e249654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3da78b8e-7386-4e1e-a87f-be0ed7025246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "21992377-4b32-41f9-96da-e033752d6f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ede9e6ed-dcb3-42ee-aab0-f5a2c04a59ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bb816468-bbd4-4215-a659-b30886f0ef18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 55,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8e5b80ad-859e-4002-8f4b-050a48309659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 49,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "55968a89-ae91-4a96-a964-ea4fd0a06bb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 44,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b9bbad6f-698c-40aa-a971-1d780c4ed1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 37,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e0eee318-a6bf-4c90-a7ab-0ac03e33ba67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 30,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "cfbab9d6-5896-4548-ac02-3866ad802fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 23,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d06dd385-e4c1-4d8c-9e95-fc082ef47952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 16,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "84d0d0be-fe91-4785-82df-2e49494e69b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 8,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fd31c2af-c453-4fea-b87f-347d93513f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0852aa90-25f5-4fd8-a36d-9d8507823a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 61,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6d5d3270-689b-4fc3-8eee-465c26dc9bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4e51dd35-5160-4c8f-aa87-afff4c77669c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "05b19240-b53d-467c-9840-a7b7c5b2c5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "12d984e8-bf97-48ce-b42d-927a24156413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6f4a4b5d-575d-46dd-845f-fdaf04c14118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "07fe4b9b-3721-4122-8fe7-72d7e97d0f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cd047c3b-c4d8-414f-9229-4b477161ee2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6f2791ef-2672-4b71-abc1-39d3652e57d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3ae03d25-5f07-4415-b778-7643e81a93c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 1,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "11e5e2bf-762a-4d8c-bbf2-dd35ca0178d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "051a7b1f-0d8b-4641-be39-24ecd6131d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 153,
                "y": 48
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "bb88491f-1d24-4723-99a2-6edea2784f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 159,
                "y": 48
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}