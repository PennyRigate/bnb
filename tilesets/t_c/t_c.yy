{
    "id": "1ce63e25-7a8c-4e17-b169-ea669ab400f7",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_c",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 5,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "f15ffca7-57bd-4303-bf40-99f6c3f750c8",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 24,
    "tileheight": 8,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 8,
    "tilexoff": 0,
    "tileyoff": 0
}